<?php

class UsuarioController extends GxController {

public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
	return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
			'actions'=>array('index','view'),
			'users'=>array('*'),
		),
		array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('*'),
			'users'=>array('@'),
		),
		array('allow', 
			'actions'=>array('delete'),
			'users'=>Usuario::model()->userPermissao('1'),
		),
		array('allow', 
			'actions'=>array('admin','create','update','alterstatus'),
			'users'=>Usuario::model()->userPermissao('1','2'),
		),
		array('deny',  // deny all users
			'users'=>array('*'),
		),
	);
}




	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Usuario'),
		));
	}

	public function actionCreate() {
		$model = new Usuario;

		if (isset($_POST['Usuario'])) {
			$model->setAttributes($_POST['Usuario']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					Yii::app()->user->setFlash("success", $model->label()." salvo com sucesso!");

				$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Usuario');


		if (isset($_POST['Usuario'])) {
			$model->setAttributes($_POST['Usuario']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Usuario')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$this->actionAdmin() ;
	}

	public function actionAdmin() {
		$model = new Usuario('search');
		$model->unsetAttributes();

		if (isset($_GET['Usuario']))
			$model->setAttributes($_GET['Usuario']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAlterStatus() {

		if (isset($_POST['id'])) {
			$model = $this->loadModel($_POST['id'], 'Usuario');

			if($model->status == 1){
				$model->status=0;
			}else{
				$model->status=1;
			}

			if ($model->update()) {
					die('ok');	
			}else{
				die('error');
			}
		}	
	}


}