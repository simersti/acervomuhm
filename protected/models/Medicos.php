<?php

/**
 * This is the model class for table "medicos".
 *
 * The followings are the available columns in table 'medicos':
 * @property string $crm
 * @property string $nome
 * @property string $apelido
 * @property string $rg
 * @property string $cpf
 * @property string $inscricao_municipal
 * @property string $datanascimento
 * @property string $sexo
 * @property string $cartao_numero
 * @property string $cartao_nome
 * @property integer $cartao_gerado
 * @property integer $codtipoassociado
 * @property integer $formapgto
 * @property integer $contribuicaoanual
 * @property string $dataassociacao
 * @property string $residente
 * @property string $dataformatura
 * @property string $dddcelular
 * @property string $celular
 * @property string $dddcelular2
 * @property string $celular2
 * @property string $dddcelular3
 * @property string $celular3
 * @property string $email
 * @property string $emailsecretaria
 * @property string $emailsimers
 * @property string $gerenterelac
 * @property string $datatermino
 * @property string $datanovotermino
 * @property string $datajubilamento
 * @property string $datareassociacao
 * @property string $datafimresidente
 * @property string $ghc
 * @property string $unimed
 * @property string $unicred
 * @property string $municipario
 * @property string $sindical
 * @property integer $socio_amerers
 * @property string $ano_residencia
 * @property integer $espec_residencia
 * @property integer $local_residencia
 * @property integer $assoc_residencia
 * @property string $dataassoc_amerers
 * @property string $datadesassoc_amerers
 * @property string $motivo
 * @property string $motivo_obs
 * @property string $motivo_usuario
 * @property integer $ref_hospital
 * @property string $senha
 * @property string $senhaunisimers
 * @property integer $revisado
 * @property string $revisor
 * @property string $data_revisao
 * @property string $data_alteracao
 * @property string $usuario
 * @property string $id
 * @property integer $recebe_email
 * @property integer $recebe_torpedos
 * @property string $capitalinicial
 * @property integer $cartao_enviado
 * @property integer $status
 * @property string $data_geracao_cartao
 * @property string $data_envio_cartao
 * @property string $office2_password
 */
class Medicos extends MultiActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'medicos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('apelido, inscricao_municipal, cartao_numero, cartao_nome, emailsecretaria, datanovotermino, ano_residencia, dataassoc_amerers, datadesassoc_amerers, senhaunisimers, recebe_torpedos, capitalinicial', 'required'),
			array('cartao_gerado, codtipoassociado, formapgto, contribuicaoanual, socio_amerers, espec_residencia, local_residencia, assoc_residencia, ref_hospital, revisado, recebe_email, recebe_torpedos, cartao_enviado, status', 'numerical', 'integerOnly'=>true),
			array('crm, ano_residencia', 'length', 'max'=>5),
			array('nome, email, emailsimers', 'length', 'max'=>100),
			array('apelido', 'length', 'max'=>150),
			array('rg, capitalinicial', 'length', 'max'=>10),
			array('cpf, celular, celular2, celular3', 'length', 'max'=>15),
			array('inscricao_municipal, senhaunisimers, office2_password', 'length', 'max'=>30),
			array('sexo, residente, ghc, unimed, unicred, municipario, sindical', 'length', 'max'=>1),
			array('cartao_numero', 'length', 'max'=>16),
			array('cartao_nome', 'length', 'max'=>40),
			array('dddcelular, dddcelular2, dddcelular3', 'length', 'max'=>2),
			array('emailsecretaria, motivo, motivo_obs, revisor', 'length', 'max'=>255),
			array('gerenterelac', 'length', 'max'=>25),
			array('motivo_usuario, usuario', 'length', 'max'=>50),
			array('senha', 'length', 'max'=>6),
			array('datanascimento, dataassociacao, dataformatura, datatermino, datajubilamento, datareassociacao, datafimresidente, data_revisao, data_alteracao, data_geracao_cartao, data_envio_cartao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('crm, nome, apelido, rg, cpf, inscricao_municipal, datanascimento, sexo, cartao_numero, cartao_nome, cartao_gerado, codtipoassociado, formapgto, contribuicaoanual, dataassociacao, residente, dataformatura, dddcelular, celular, dddcelular2, celular2, dddcelular3, celular3, email, emailsecretaria, emailsimers, gerenterelac, datatermino, datanovotermino, datajubilamento, datareassociacao, datafimresidente, ghc, unimed, unicred, municipario, sindical, socio_amerers, ano_residencia, espec_residencia, local_residencia, assoc_residencia, dataassoc_amerers, datadesassoc_amerers, motivo, motivo_obs, motivo_usuario, ref_hospital, senha, senhaunisimers, revisado, revisor, data_revisao, data_alteracao, usuario, id, recebe_email, recebe_torpedos, capitalinicial, cartao_enviado, status, data_geracao_cartao, data_envio_cartao, office2_password', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'crm' => 'Crm',
			'nome' => 'Nome',
			'apelido' => 'Apelido',
			'rg' => 'Rg',
			'cpf' => 'Cpf',
			'inscricao_municipal' => 'Inscricao Municipal',
			'datanascimento' => 'Datanascimento',
			'sexo' => 'Sexo',
			'cartao_numero' => 'Cartao Numero',
			'cartao_nome' => 'Cartao Nome',
			'cartao_gerado' => 'Cartao Gerado',
			'codtipoassociado' => 'Codtipoassociado',
			'formapgto' => 'Formapgto',
			'contribuicaoanual' => 'Contribuicaoanual',
			'dataassociacao' => 'Dataassociacao',
			'residente' => 'Residente',
			'dataformatura' => 'Dataformatura',
			'dddcelular' => 'Dddcelular',
			'celular' => 'Celular',
			'dddcelular2' => 'Dddcelular2',
			'celular2' => 'Celular2',
			'dddcelular3' => 'Dddcelular3',
			'celular3' => 'Celular3',
			'email' => 'Email',
			'emailsecretaria' => 'Emailsecretaria',
			'emailsimers' => 'Emailsimers',
			'gerenterelac' => 'Gerenterelac',
			'datatermino' => 'Datatermino',
			'datanovotermino' => 'Datanovotermino',
			'datajubilamento' => 'Datajubilamento',
			'datareassociacao' => 'Datareassociacao',
			'datafimresidente' => 'Datafimresidente',
			'ghc' => 'Ghc',
			'unimed' => 'Unimed',
			'unicred' => 'Unicred',
			'municipario' => 'Municipario',
			'sindical' => 'Sindical',
			'socio_amerers' => 'Socio Amerers',
			'ano_residencia' => 'Ano Residencia',
			'espec_residencia' => 'Espec Residencia',
			'local_residencia' => 'Local Residencia',
			'assoc_residencia' => 'Assoc Residencia',
			'dataassoc_amerers' => 'Dataassoc Amerers',
			'datadesassoc_amerers' => 'Datadesassoc Amerers',
			'motivo' => 'Motivo',
			'motivo_obs' => 'Motivo Obs',
			'motivo_usuario' => 'Motivo Usuario',
			'ref_hospital' => 'Ref Hospital',
			'senha' => 'Senha',
			'senhaunisimers' => 'Senhaunisimers',
			'revisado' => 'Revisado',
			'revisor' => 'Revisor',
			'data_revisao' => 'Data Revisao',
			'data_alteracao' => 'Data Alteracao',
			'usuario' => 'Usuario',
			'id' => 'ID',
			'recebe_email' => 'Recebe Email',
			'recebe_torpedos' => 'Recebe Torpedos',
			'capitalinicial' => 'Capitalinicial',
			'cartao_enviado' => 'Cartao Enviado',
			'status' => 'Status',
			'data_geracao_cartao' => 'Data Geracao Cartao',
			'data_envio_cartao' => 'Data Envio Cartao',
			'office2_password' => 'Office2 Password',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('crm',$this->crm,false);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('apelido',$this->apelido,true);
		$criteria->compare('rg',$this->rg,true);
		$criteria->compare('cpf',$this->cpf,true);
		$criteria->compare('inscricao_municipal',$this->inscricao_municipal,true);
		$criteria->compare('datanascimento',$this->datanascimento,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('cartao_numero',$this->cartao_numero,true);
		$criteria->compare('cartao_nome',$this->cartao_nome,true);
		$criteria->compare('cartao_gerado',$this->cartao_gerado);
		$criteria->compare('codtipoassociado',$this->codtipoassociado);
		$criteria->compare('formapgto',$this->formapgto);
		$criteria->compare('contribuicaoanual',$this->contribuicaoanual);
		$criteria->compare('dataassociacao',$this->dataassociacao,true);
		$criteria->compare('residente',$this->residente,true);
		$criteria->compare('dataformatura',$this->dataformatura,true);
		$criteria->compare('dddcelular',$this->dddcelular,true);
		$criteria->compare('celular',$this->celular,true);
		$criteria->compare('dddcelular2',$this->dddcelular2,true);
		$criteria->compare('celular2',$this->celular2,true);
		$criteria->compare('dddcelular3',$this->dddcelular3,true);
		$criteria->compare('celular3',$this->celular3,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('emailsecretaria',$this->emailsecretaria,true);
		$criteria->compare('emailsimers',$this->emailsimers,true);
		$criteria->compare('gerenterelac',$this->gerenterelac,true);
		$criteria->compare('datatermino',$this->datatermino,true);
		$criteria->compare('datanovotermino',$this->datanovotermino,true);
		$criteria->compare('datajubilamento',$this->datajubilamento,true);
		$criteria->compare('datareassociacao',$this->datareassociacao,true);
		$criteria->compare('datafimresidente',$this->datafimresidente,true);
		$criteria->compare('ghc',$this->ghc,true);
		$criteria->compare('unimed',$this->unimed,true);
		$criteria->compare('unicred',$this->unicred,true);
		$criteria->compare('municipario',$this->municipario,true);
		$criteria->compare('sindical',$this->sindical,true);
		$criteria->compare('socio_amerers',$this->socio_amerers);
		$criteria->compare('ano_residencia',$this->ano_residencia,true);
		$criteria->compare('espec_residencia',$this->espec_residencia);
		$criteria->compare('local_residencia',$this->local_residencia);
		$criteria->compare('assoc_residencia',$this->assoc_residencia);
		$criteria->compare('dataassoc_amerers',$this->dataassoc_amerers,true);
		$criteria->compare('datadesassoc_amerers',$this->datadesassoc_amerers,true);
		$criteria->compare('motivo',$this->motivo,true);
		$criteria->compare('motivo_obs',$this->motivo_obs,true);
		$criteria->compare('motivo_usuario',$this->motivo_usuario,true);
		$criteria->compare('ref_hospital',$this->ref_hospital);
		$criteria->compare('senha',$this->senha,true);
		$criteria->compare('senhaunisimers',$this->senhaunisimers,true);
		$criteria->compare('revisado',$this->revisado);
		$criteria->compare('revisor',$this->revisor,true);
		$criteria->compare('data_revisao',$this->data_revisao,true);
		$criteria->compare('data_alteracao',$this->data_alteracao,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('recebe_email',$this->recebe_email);
		$criteria->compare('recebe_torpedos',$this->recebe_torpedos);
		$criteria->compare('capitalinicial',$this->capitalinicial,true);
		$criteria->compare('cartao_enviado',$this->cartao_enviado);
		$criteria->compare('status',$this->status);
		$criteria->compare('data_geracao_cartao',$this->data_geracao_cartao,true);
		$criteria->compare('data_envio_cartao',$this->data_envio_cartao,true);
		$criteria->compare('office2_password',$this->office2_password,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->seconddb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Medicos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getCampo($crm,$campo = '*') {
		if(!empty($crm)){
		  	$command=Yii::app()->seconddb->createCommand();
		    $command->select($campo);
		    $command->from('medicos');
		    $command->where('crm ='.$crm);
		    $return = $command->queryScalar();
		    if($return == '' || $return == null){

		    	return 'Indefido';

		    }else{
		    	return $return;
		    }
    	}else{
    		return false;
    	}

	}


}
