<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

	<nav class="navbar navbar-default navbar-fixed-top navbar-inverse">
	  <div class="container-fluid">

	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
    <a id="menu-toggle"  href="menu" class="navbar-brand glyphicon glyphicon-indent-right " data-toggle="tooltip" data-placement="bottom" data-original-title="Mostrar/Ocultar Menu" style="color:#fff;">
    </a>

            <a class="navbar-brand" href="<?php echo Yii::app()->baseUrl; ?>">
	            <img alt="Brand" src="images/logo.png">
	        </a>
		    <a class="navbar-brand" style="color:#fff;font-size: 20px;" href="<?php echo Yii::app()->baseUrl; ?>"><?php echo CHtml::encode(Yii::app()->name); ?></a>
		    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		    </button>
	     	<a class="navbar-brand" href="#"></a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    <ul class="nav navbar-nav navbar-right" style="margin-right:10px;">
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" ><span class="glyphicon glyphicon glyphicon-user" aria-hidden="true"></span>  <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="<?php echo Yii::app()->baseUrl; ?>/?r=site/logout"><span class="glyphicon glyphicon-off" aria-hidden="true"></span> Sair</a></li>
<!-- 	            <li class="divider"></li>
	            <li></li> -->
	          </ul>
	        </li>
	    </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="clear" style="height:70px;width:100%;"></div>

<!-- ================================= -->


	<div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">

            <nav id="spy" style="margin-bottom:50px;">
            	<?php 
            		require_once '_menulateral.php';
            	?>
            </nav>

        </div>

        <!-- Page content -->
        <div id="page-content-wrapper">

            <div class="content-header">

                    <?php echo $this->title_action; ?>

                    <?php //echo $this->pageTitle; ?>
            </div>

            <div class="page-content inset" data-spy="scroll" data-target="#spy">
                <div class="row">
                    <?php
                        foreach(Yii::app()->user->getFlashes() as $key => $message) {
							echo '<div class="alert alert-' . $key . ' alert-dismissible" role="alert">
							  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							  ' . $message . '
							</div>';
					    }
					?>

    				<?php echo $content; ?>

                </div>
            </div>
        </div>
    </div>

<!-- ================================= -->



<div id="footer" style="position: static; width: 100%; bottom: 0px;">
    <div id="footer-content">
            © SIMERS - Sindicato Médico do Rio Grande do Sul - 2015  - Todos os direitos reservados.
    </div>
    
    <div id="footer-user-display-name">
        Olá, <?=Yii::app()->user->nome?>!
    </div>
</div>
	<div class="clear" style="height:50px;width:100%;"></div>

<?php $this->endContent(); ?>