<?php

class RelatorioController extends GxController {


// public function filters()
// {
// return array(
// 'accessControl', // perform access control for CRUD operations
// );
// }

// /**
// * Specifies the access control rules.
// * This method is used by the 'accessControl' filter.
// * @return array access control rules
// */
// public function accessRules()
// {
// 	return array(
// 		array('allow',  // allow all users to perform 'index' and 'view' actions
// 			'actions'=>array(''),
// 			'users'=>array('*'),
// 		),
// 		array('allow', // allow authenticated user to perform 'create' and 'update' actions
// 			'actions'=>array('*'),
// 			'users'=>array('@'),
// 		),
// 		// array('allow', 
// 		// 	'actions'=>array(''),
// 		// 	'users'=>Usuarios::model()->userPermissao('2'),
// 		// ),

// 		array('allow', 
// 			'actions'=>array(
// 				'index',
// 				'Result',
// 				'ResultCentroCusto',
// 				'ResultExcel',
// 			),
// 			'users'=>Usuarios::model()->userPermissao('1'),
// 		),
// 		array('deny',  // deny all users
// 			'users'=>array('*'),
// 		),
// 	);
// }


	public function actionIndex() {

		$Categoria 		= Categoria::model()->findAll('1 ORDER BY id');
		$SubCategoria 	= SubCategoria::model()->findAll('1 ORDER BY id');
		$Estado 		= Estado::model()->findAll('1 ORDER BY id');
		$Cidade 		= Cidade::model()->findAll('1 ORDER BY id');

		$this->render('index', array(
			'Categoria'		=>$Categoria,
			'SubCategoria'	=>$SubCategoria,
			'Estado'		=>$Estado,
			'Cidade'		=>$Cidade,
		));
	}

	public function actionResult(){
		if(!empty($_POST)){
			$html = $this->renderPartial(
				'_result', 
				array(
					'result' => Relatorio::resultList($_POST),
					'busca'=>json_encode($_POST),
					'form'=>md5(time()),
				),true
			);
			$return["busca"] = json_encode($_POST);
			$return["html"] = $html;
	  		echo json_encode($return);

			die();
		}else{
			die('erro');
		}


	}

	public function actionResultExcel(){
		$busca = (array) json_decode($_POST['value']);
		if(!empty($_POST)){
			$this->renderPartial(
				'excel', 
				array(
					'result' => Relatorio::resultList($busca),
				)
			);
		}else{
			$this->redirect(array('index'));
		}
	}

}