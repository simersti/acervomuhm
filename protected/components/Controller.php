<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController{
	
	// -------------------------------------------------------------------------------------------------------------
	
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */


	public $layout='//layouts/column1';


	//altera menu lateral para permissão de usuário

	protected function alterLayout(){
		if(!isset(Yii::app()->user->permissao)){
			$this->layout='//layouts/column1';
		}else if(Yii::app()->user->permissao){
			$this->layout='//layouts/column_admin';
		}
		//else if(Yii::app()->user->permissao == 3){
		// 	$this->layout='//layouts/column4';
		// }else{
		// 	$this->layout='//layouts/column3';
		// }
	}

	// -------------------------------------------------------------------------------------------------------------
	
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();

	/**
	 * @var título da página
	 */
	public $title_action = null;

	/**
	 * @var array menu lateral
	 */

	// public $menu_lateral = array();
	public function menuLateral(){

		return 	array(
			/*array(
				'label'=>'Listagens',
				'link'=>false,
				'icon'=>'list',
				'permissao'=>array(1,2),
				'items'=>array(
					array('label'=>'Relações Institucionais', 'url'=>'?r=usuarios','icon'=>false),
					// array('label'=>'Funcionários', 'url'=>'#','icon'=>false),
				),
			),*/
			array(
				'label'=>'Arquivístico',
				'link'=>false,
				'icon'=>'tags',
				'permissao'=>array(1,2),
				'items'=>array(
					array('label'=>'Arquivístico', 'url'=>'?r=arquivistico','icon'=>false, 'target' => ''),
					array('label'=>'Tipo Série', 'url'=>'?r=tipoSerie','icon'=>false, 'target' => ''),
					array('label'=>'Classificação', 'url'=>'?r=classificacaoArquivistico','icon'=>false, 'target' => ''),
					array('label'=>'Movimentação', 'url'=>'?r=movimentacaoArquivistico','icon'=>false, 'target' => ''),
					array('label'=>'Relatórios', 'url'=>'?r=arquivistico/relatorios','icon'=>false, 'target' => ''),
					array('label'=>'Relatórios de Movimentações Ativas', 'url'=>'?r=arquivistico/movimentacaoAtiva','icon'=>false, 'target' => '_blank'),
					array('label'=>'Relatórios de Movimentações Inativas', 'url'=>'?r=arquivistico/movimentacaoInativa','icon'=>false, 'target' => '_blank'),
				)
			),
			array(
				'label'=>'Bibliográfico',
				'link'=>false,
				'icon'=>'tags',
				'permissao'=>array(1,2),
				'items'=>array(
					array('label'=>'Bibliográfico', 'url'=>'?r=biblioteca','icon'=>false, 'target' => ''),					
					array('label'=>'Editora', 'url'=>'?r=editora','icon'=>false, 'target' => ''),
					array('label'=>'Especialidade Médica', 'url'=>'?r=especialidadeMedica','icon'=>false, 'target' => ''),
					array('label'=>'Tipologia', 'url'=>'?r=tipologia','icon'=>false, 'target' => ''),
					array('label'=>'Movimentação', 'url'=>'?r=movimentacaoBiblioteca','icon'=>false, 'target' => ''),	
					array('label'=>'Relatórios', 'url'=>'?r=biblioteca/relatorios','icon'=>false, 'target' => ''),
					array('label'=>'Relatórios de Movimentações Ativas', 'url'=>'?r=biblioteca/movimentacaoAtiva','icon'=>false, 'target' => '_blank'),
					array('label'=>'Relatórios de Movimentações Inativas', 'url'=>'?r=biblioteca/movimentacaoInativa','icon'=>false, 'target' => '_blank'),
				)
			),
			array(
				'label'=>'Tridimensional',
				'link'=>false,
				'icon'=>'tags',
				'permissao'=>array(1,2),
				'items'=>array(
					array('label'=>'Tridimensional', 'url'=>'?r=inventario','icon'=>false, 'target' => ''),
					array('label'=>'Movimentação', 'url'=>'?r=movimentacao','icon'=>false, 'target' => ''),	
					array('label'=>'Classificação', 'url'=>'?r=classificacao','icon'=>false, 'target' => ''),
					array('label'=>'Coleção', 'url'=>'?r=colecao','icon'=>false, 'target' => ''),
					array('label'=>'Material', 'url'=>'?r=material','icon'=>false, 'target' => ''),
					array('label'=>'Material Acondicionado', 'url'=>'?r=acondicionado','icon'=>false, 'target' => ''),					
					array('label'=>'Sub Coleção', 'url'=>'?r=subColecao','icon'=>false, 'target' => ''),	
					array('label'=>'Tipologia', 'url'=>'?r=tipoInventario','icon'=>false, 'target' => ''),	
					array('label'=>'Relatórios', 'url'=>'?r=inventario/relatorios','icon'=>false, 'target' => ''),	
					array('label'=>'Relatórios de Movimentações Ativas', 'url'=>'?r=inventario/movimentacaoAtiva','icon'=>false, 'target' => '_blank'),
					array('label'=>'Relatórios de Movimentações Inativas', 'url'=>'?r=inventario/movimentacaoInativa','icon'=>false, 'target' => '_blank'),
				)
			),
			array(
				'label'=>'Cadastros Complementares',
				'link'=>false,
				'icon'=>'asterisk',
				'permissao'=>array(1,2),
				'items'=>array(		
					array('label'=>'Acumulado Por', 'url'=>'?r=acumuladoPor','icon'=>false, 'target' => ''),
					array('label'=>'Doador', 'url'=>'?r=doador','icon'=>false, 'target' => ''),										
					array('label'=>'Estado de Conservação', 'url'=>'?r=conservacao','icon'=>false, 'target' => ''),
					array('label'=>'Modo de Aquisição', 'url'=>'?r=modoAquisicao','icon'=>false, 'target' => ''),
					array('label'=>'Motivo de Inatividade', 'url'=>'?r=motivoInativo','icon'=>false, 'target' => ''),										
					array('label'=>'Tipo de Movimentação', 'url'=>'?r=tipoMovimentacao','icon'=>false, 'target' => ''),
					
				),
			),			
			array(
				'label'=>'Usuários',
				'link'=>false,
				'icon'=>'user',
				'permissao'=>array(1,2),
				'items'=>array(
					array('label'=>'Usuário', 'url'=>'?r=usuario','icon'=>false, 'target' => ''),
					array('label'=>'Grupo', 'url'=>'?r=grupo','icon'=>false, 'target' => ''),					
				)
			),			
		);
	}






	// -------------------------------------------------------------------------------------------------------------
	
	/**
	 * @var Atributo que ira receber o array de notificações do sistema
	 */
	public $notification=null;
	
	// -------------------------------------------------------------------------------------------------------------
	
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	// -------------------------------------------------------------------------------------------------------------
	
	public function init(){
	
		// captura as notificações do sistema
		// $this->notification = Yii::app()->Notification->getNotificacoes();
		$this->alterLayout();
		parent::init();
	}
	
	// -------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método responsável por setar a paginçãoo do TbGridView
	 *
	 * @author Rodrigo Conceicao de Araujo
	 * @access private
	 * @return void
	 **/
	protected function setPagination(){
		if (Yii::app()->getRequest()->getIsAjaxRequest()){
			if(isset($_GET['pageSize']) && !empty($_GET['pageSize'])){
				Yii::app()->params['defaultPageSize'] = $_GET['pageSize'];
			}
		}
	}
	
	// -------------------------------------------------------------------------------------------------------------
}