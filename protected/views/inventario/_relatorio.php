<h1><?php echo 'Relatório ' . GxHtml::encode($model->label()); ?></h1>

<div class="form well">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'inventario-form',
	'htmlOptions'=>array('target'=>'_blank'),
	'enableAjaxValidation' => false,
));
?>
		<?php echo $form->errorSummary($model); ?>

		<div class="row">
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'doador_id'); ?>
				<?php echo $form->dropDownList($model, 'doador_id', GxHtml::listDataEx(Doador::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'doador_id'); ?>								
			</div>
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'modo_aquisicao_id'); ?>
				<?php echo $form->dropDownList($model, 'modo_aquisicao_id', GxHtml::listDataEx(ModoAquisicao::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'modo_aquisicao_id'); ?>								
			</div>	
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'acumulado_por_id'); ?>
				<?php echo $form->dropDownList($model, 'acumulado_por_id', GxHtml::listDataEx(AcumuladoPor::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'acumulado_por_id'); ?>								
			</div>					
		</div><!-- row -->

		<div class="row">
			<div class="col-md-3">			
				<?php echo $form->labelEx($model,'tipo_inventario_id'); ?>
				<?php echo $form->dropDownList($model, 'tipo_inventario_id', GxHtml::listDataEx(TipoInventario::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'tipo_inventario_id'); ?>								
			</div>	
			<div class="col-md-3">			
				<?php echo $form->labelEx($model,'classificacao_id'); ?>
				<?php echo $form->dropDownList($model, 'classificacao_id', GxHtml::listDataEx(Classificacao::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'classificacao_id'); ?>								
			</div>	
			<div class="col-md-3">			
				<?php echo $form->labelEx($model,'material_id'); ?>
				<?php echo $form->dropDownList($model, 'material_id', GxHtml::listDataEx(Material::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'material_id'); ?>								
			</div>
			<div class="col-md-3">			
				<?php echo $form->labelEx($model,'acondicionado_id'); ?>
				<?php echo $form->dropDownList($model, 'acondicionado_id', GxHtml::listDataEx(Acondicionado::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'acondicionado_id'); ?>								
			</div>								
		</div><!-- row -->

		<div class="row">
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'colecao_id'); ?>
				<?php echo $form->dropDownList($model, 'colecao_id', GxHtml::listDataEx(Colecao::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'colecao_id'); ?>								
			</div>

			<div class="col-md-4">					
				<?php echo $form->labelEx($model,'sub_colecao_id'); ?>
				<?php echo CHtml::activeDropDownList($model,'sub_colecao_id', CHtml::listData(SubColecao::model()->findAll(), 'id', 'nome'), array('empty' => 'Sem Sub Coleção','class'=>'form-control')); ?>
				<?php echo $form->error($model,'sub_colecao_id'); ?>								
			</div>			
			<div class="col-md-4">
			  <?php  echo $form->dropDownListGroup($model,'ativo',array(
						'widgetOptions' => array(
							'data' => array('2'=>'Ambos','1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('ativo'),
							),
						),
					)); ?>
			</div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-6">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'size'=>'large',
					'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
					'label'=> 'Gerar Relatório',
				)); ?>
			</div>
		</div><!-- row -->
<?php

$this->endWidget();
?>
</div><!-- form -->