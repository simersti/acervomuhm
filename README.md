Projeto Modelo
============
Projeto modelo para desenvolvimento com framework Yii.

### Banco de dados
Existem um arquivo de banco de dados modelo na pasta db/dump-base.sql

### Configuração do sistema
O arquivo de configuração do sistema se encontra na pasta protected/confing/main.php

