<?php

class AcondicionadoController extends GxController {


	public function actionCreate() {
		$model = new Acondicionado;


		if (isset($_POST['Acondicionado'])) {
			$model->setAttributes($_POST['Acondicionado']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Acondicionado');


		if (isset($_POST['Acondicionado'])) {
			$model->setAttributes($_POST['Acondicionado']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Acondicionado');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Acondicionado('search');
		$model->unsetAttributes();

		if (isset($_GET['Acondicionado']))
			$model->setAttributes($_GET['Acondicionado']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}