<div class="form well">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'movimentacao-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Os campos com <span class="required">*</span> são obrigatórios.
	</p>

	<?php 
		echo $form->errorSummary($model);		
	 ?>

		<div class="row">
			<div class="col-md-2">				
				<?php echo $form->labelEx($model,'tipo_movimentacao_id'); ?>
				<?php if($model->isNewRecord): ?>					
			    	<?php echo $form->dropDownList($model, 'tipo_movimentacao_id', GxHtml::listDataEx(TipoMovimentacao::model()->findAllByAttributes(array('ativo' => 1, 'excluido' => 0), array('order' => 'nome ASC'))),array('class'=>'form-control')); ?>			    	
				<?php else: ?>
					<?php echo $form->dropDownList($model, 'tipo_movimentacao_id', GxHtml::listDataEx(TipoMovimentacao::model()->findAllByAttributes(array('excluido' => 0), array('order' => 'nome ASC'))),array('class'=>'form-control','disabled'=> true)); ?>
				<?php endif; ?>				
				<?php echo $form->error($model,'tipo_movimentacao_id'); ?>
			</div>
			<?php if($model->isNewRecord): ?>
				<div class="col-md-2"><?php echo $form->textFieldGroup($model, 'codigo', array('maxlength' => 50)); ?></div>
			<?php else: ?>				
				<div class="col-md-2"><?php echo $form->textFieldGroup($model, 'codigo', array('maxlength' => 50, 
																							   'widgetOptions' => array(
																							   'htmlOptions' => array('disabled' => true))
																							   )); ?></div>	
			<?php endif; ?>				
		</div><!-- row -->

		<div class="row">
			<?php if($model->isNewRecord): ?>
			   <div class="col-md-4"><?php echo $form->textFieldGroup($model, 'instituicao', array('maxlength' => 100)); ?></div>
			<?php else: ?>
				<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'instituicao', array('maxlength' => 100,
																									'widgetOptions' => array(
																							  		'htmlOptions' => array('disabled' => true))
																							   		)); ?></div>
			<?php endif; ?>

			<?php if($model->isNewRecord): ?>
			  <div class="col-md-4"><?php echo $form->textFieldGroup($model, 'motivo', array('maxlength' => 255)); ?></div>
			<?php else: ?>
			   <div class="col-md-4"><?php echo $form->textFieldGroup($model, 'motivo', array('maxlength' => 255,
																							  'widgetOptions' => array(
																							  'htmlOptions' => array('disabled' => true))
			   																				  )); ?></div>
			<?php endif; ?>			
		</div><!-- row -->


		<div class="row">

				<div class="col-sm-4">
				<?php if($model->isNewRecord): ?>
					<?php
						echo $form->datePickerGroup($model, 'data_saida',
			                array(
			                    'widgetOptions' => array(
			                        'options' => array(
			                            'language'                 => 'pt-BR',
			                            'format'                 => 'dd/mm/yyyy',
			                            'showDropdowns'         => false,
			                            'autoclose'                => false,
			                            'todayHighlight'         => false,
			                        ),
			                        // 'htmlOptions'=>array('placeholder'=>'Período',),
			                    ),
			                )
			            );
					?>
				<?php else: ?>
					<?php
						echo $form->datePickerGroup($model, 'data_saida',
			                array(
			                    'widgetOptions' => array(
			                        'options' => array(
			                            'language'                 => 'pt-BR',
			                            'format'                 => 'dd/mm/yyyy',
			                            'showDropdowns'         => false,
			                            'autoclose'                => false,
			                            'todayHighlight'         => false,			                            
			                        ),
			                         'htmlOptions'=>array('disabled' => true,),
			                    ),
			                )
			            );
					?>
				<?php endif; ?>		
				</div>


				<div class="col-sm-4">
				<?php if($model->isNewRecord): ?>
					<?php
						echo $form->datePickerGroup($model, 'periodo_inicio',
			                array(
			                    'widgetOptions' => array(
			                        'options' => array(
			                            'language'                 => 'pt-BR',
			                            'format'                 => 'dd/mm/yyyy',
			                            'showDropdowns'         => false,
			                            'autoclose'                => false,
			                            'todayHighlight'         => false,
			                        ),
			                        // 'htmlOptions'=>array('placeholder'=>'Período',),
			                    ),
			                )
			            );
					?>
				<?php else: ?>
					<?php
						echo $form->datePickerGroup($model, 'periodo_inicio',
			                array(
			                    'widgetOptions' => array(
			                        'options' => array(
			                            'language'                 => 'pt-BR',
			                            'format'                 => 'dd/mm/yyyy',
			                            'showDropdowns'         => false,
			                            'autoclose'                => false,
			                            'todayHighlight'         => false,			                            
			                        ),
			                         'htmlOptions'=>array('disabled' => true,),
			                    ),
			                )
			            );
					?>
				<?php endif; ?>
				</div>

				<div class="col-sm-4">
				<?php if($model->isNewRecord): ?>
					<?php
						echo $form->datePickerGroup($model, 'periodo_fim',
			                array(
			                    'widgetOptions' => array(
			                        'options' => array(
			                            'language'                 => 'pt-BR',
			                            'format'                 => 'dd/mm/yyyy',
			                            'showDropdowns'         => false,
			                            'autoclose'                => false,
			                            'todayHighlight'         => false,
			                        ),
			                        // 'htmlOptions'=>array('placeholder'=>'Período',),
			                    ),
			                )
			            );
					?>
				<?php else: ?>
					<?php
						echo $form->datePickerGroup($model, 'periodo_fim',
			                array(
			                    'widgetOptions' => array(
			                        'options' => array(
			                            'language'                 => 'pt-BR',
			                            'format'                 => 'dd/mm/yyyy',
			                            'showDropdowns'         => false,
			                            'autoclose'                => false,
			                            'todayHighlight'         => false,			                            
			                        ),
			                         'htmlOptions'=>array('disabled' => true,),
			                    ),
			                )
			            );
					?>
				<?php endif; ?>
				</div>

				<div class="col-sm-4">

				<?php if($model->isNewRecord): ?>
					<?php
						echo $form->datePickerGroup($model, 'data_retorno',
			                array(
			                    'widgetOptions' => array(
			                        'options' => array(
			                            'language'                 => 'pt-BR',
			                            'format'                 => 'dd/mm/yyyy',
			                            'showDropdowns'         => false,
			                            'autoclose'                => false,
			                            'todayHighlight'         => false,
			                        ),
			                         'htmlOptions'=>array('disabled' => true,),
			                    ),
			                )
			            );
					?>
				<?php else: ?>
					<?php
						echo $form->datePickerGroup($model, 'data_retorno',
			                array(
			                    'widgetOptions' => array(
			                        'options' => array(
			                            'language'                 => 'pt-BR',
			                            'format'                 => 'dd/mm/yyyy',
			                            'showDropdowns'         => false,
			                            'autoclose'                => false,
			                            'todayHighlight'         => false,			                            
			                        ),
			                        //// 'htmlOptions'=>array('disabled' => true,),
			                    ),
			                )
			            );
					?>
				<?php endif; ?>
				</div>
		</div>

		<div class="row">
			<?php if($model->isNewRecord): ?>
			    <div class="col-md-4"><?php echo $form->textFieldGroup($model, 'localizacao', array('maxlength' => 255)); ?></div>
			<?php else: ?>
			   <div class="col-md-4"><?php echo $form->textFieldGroup($model, 'localizacao', array('maxlength' => 255,
																							  'widgetOptions' => array(
																							  'htmlOptions' => array('disabled' => true))
			   																				  )); ?></div>

			<?php endif; ?>

			<?php if($model->isNewRecord): ?>
			   <div class="col-md-4"><?php echo $form->textFieldGroup($model, 'responsavel', array('maxlength' => 255)); ?></div>
			<?php else: ?>
			   <div class="col-md-4"><?php echo $form->textFieldGroup($model, 'responsavel', array('maxlength' => 255,
																							  'widgetOptions' => array(
																							  'htmlOptions' => array('disabled' => true))
			   																				  )); ?></div>
			<?php endif; ?>

		</div><!-- row -->

		<div class="row">

			<?php if($model->isNewRecord): ?>
			   <div class="col-md-4"><?php echo $form->textFieldGroup($model, 'solicitante', array('maxlength' => 100)); ?></div>
			<?php else: ?>
			   <div class="col-md-4"><?php echo $form->textFieldGroup($model, 'solicitante', array('maxlength' => 100,
																							  'widgetOptions' => array(
																							  'htmlOptions' => array('disabled' => true))
			   																				  )); ?></div>
			<?php endif; ?>


			<?php if($model->isNewRecord): ?>
			   <div class="col-md-3"><?php echo $form->textFieldGroup($model, 'rg_solicitante', array('maxlength' => 100)); ?></div>
			<?php else: ?>
			   <div class="col-md-3"><?php echo $form->textFieldGroup($model, 'rg_solicitante', array('maxlength' => 100,
																							  'widgetOptions' => array(
																							  'htmlOptions' => array('disabled' => true))
			   																				  )); ?></div>
			<?php endif; ?>			
			
			<div class="col-md-1">
			<?php if($model->isNewRecord): ?>
			  <?php  echo $form->dropDownListGroup($model,'ativo',array(
						'widgetOptions' => array(
							'data' => array('1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('ativo'),
							),							
						),
					)); ?>
			<?php else: ?>
			  <?php  echo $form->dropDownListGroup($model,'ativo',array(
						'widgetOptions' => array(
							'data' => array('1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('ativo'),
							),
							'htmlOptions' => array('disabled' => true)
						),
					)); ?>
			<?php endif; ?>						
			</div>
		</div><!-- row -->		

		<div class="row">
			<div class="col-md-6">
			<?php echo $form->html5EditorGroup(
				$model,
				'observacao',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>
		</div>		

		<?php if($model->isNewRecord): ?>
		<div class="row">
		  <div class="col-md-4">		
			<?php echo $form->select2Group($inventario,
					'id',
					array(
						'label' => 'Localizar Inventário',
						'widgetOptions' => array(
							'data' => GxHtml::listDataEx(Inventario::model()->findAllAttributes(null, true)),
							'options' => array(								
								'placeholder' => 'Selecione o inventário',
								'tokenSeparators' => array(',', ' '),
							),
							'htmlOptions' => array(
								'multiple' 	=> 'multiple',		            	
								'data-toggle' 	=> 'modal',
								'data-target' 	=> '#box-movimentacao',
								'ajax' => array(
		                            	'type'		=> 'POST',
		                            	'dataType'  => 'json',
		                                'url'		=> Yii::app()->createUrl('/movimentacao/createItens'),
		                                'title'		=> '<i class="fa fa-cubes"></i> Novo grupo',
		                                'success'	=> 'function(data) { 	                                	
						                	$(".box-movimentacao-table" ).remove();
						                	$("#box-movimentacao").show();
						                	$("#box-movimentacao").prepend(data["html"]);  
		                                }',
		                                'error'	=> 'function(){
		                                	bootbox.alert("Não foi possível executar esta ação.");
		                            	}'
		                            )
							)
						),
					)
				);
			?>
		   </div>
		</div>
		<?php endif; ?>
		
<?php if($model->isNewRecord): ?>
<h4 style="margin-left: 14px;"><?php echo 'Itens da ' . ' ' . GxHtml::encode($model->label()); ?></h4>
<?php endif; ?>

<div id="box-movimentacao" style="display:block;">

	<hr>	
	<div class="row">
		<div class="col-md-6">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'size'=>'large',
				'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
				'label'=>$model->isNewRecord ? 'Cadastrar' : 'Receber Itens',
			)); ?>
		</div>
	</div><!-- row -->	
</div>		

<?php
$this->endWidget();
?>
</div><!-- form -->