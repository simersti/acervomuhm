<?php

class MotivoInativoController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'MotivoInativo'),
		));
	}

	public function actionCreate() {
		$model = new MotivoInativo;


		if (isset($_POST['MotivoInativo'])) {
			$model->setAttributes($_POST['MotivoInativo']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'MotivoInativo');


		if (isset($_POST['MotivoInativo'])) {
			$model->setAttributes($_POST['MotivoInativo']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'MotivoInativo');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}


	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new MotivoInativo('search');
		$model->unsetAttributes();

		if (isset($_GET['MotivoInativo']))
			$model->setAttributes($_GET['MotivoInativo']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}