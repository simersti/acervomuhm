<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);

$this->title_action = GxHtml::encode($model->label(2));

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('especialidade-medica-grid', {
		data: $(this).serialize()
	});
	return false;
});

");

$this->widget(
    'booster.widgets.TbButtonGroup',
    array(
        'buttons' => array(
            array('buttonType'=>'link' ,'label' => 'Cadastrar Nova '.$model->label(), 'url'=>array('create'), 'context'=>'primary'),
        ),
    )
);

?>


<?php $this->widget('booster.widgets.TbExtendedGridView', array(
	'id' => 'especialidade-medica-grid',
	'type' => 'striped bordered condensed hover',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		///'id',
		'descricao',
		array(
			'name'=>'ativo',
			'value'=>'UtilModel::statusAtual($data->ativo)',
			'filter'=>array('1'=>'Ativo','0'=>'Inativo'),
			'htmlOptions'=>array('style'=>'width:10%;')

		),		
		///'excluido',
		array(
			'header' 		=> Yii::t('ses', 'Ações'),
			'class'			=> 'booster.widgets.TbButtonColumn',
			'template'		=> '{update} | {delete}',
			'buttons'=>array(
                'update'=>array(
                    // 'icon'=>'icon-edit',
                    'label'=>'Editar',
                ),
                'delete'=>array(
                    // 'icon'=>'delete',
                    'label'=>'delete',
                ),

    		),
			'headerHtmlOptions'	=> array('class'=>'col-sm-1 text-center'),
			'htmlOptions'	=> array('style' => 'vertical-align: middle; width:20%;', 'class' => 'text-center'),
		),
	),
)); ?>