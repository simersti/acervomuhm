<?php

Yii::import('application.models._base.BaseAcervo');

class Acervo extends BaseAcervo
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function geraCodigo(){

			/*cria o um novo numero de inventario apartir do numero anterior para manter a sequencia ou padrão*/
			$criteria = new CDbCriteria;
			$criteria->limit = 1;
			$criteria->order = 'codigo DESC';

		    $acervo = Acervo::Model()->find($criteria);

		    $numero = preg_replace("/[^0-9]/","", $acervo->codigo);
		    $numero = (int)$numero + 1;
		    
			$retorno = 'MUHM';
			$retorno.= (string)($numero);

			return $retorno;
	}
}