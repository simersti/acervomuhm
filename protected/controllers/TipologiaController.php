<?php

class TipologiaController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Tipologia'),
		));
	}

	public function actionCreate() {
		$model = new Tipologia;


		if (isset($_POST['Tipologia'])) {
			$model->setAttributes($_POST['Tipologia']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Tipologia');


		if (isset($_POST['Tipologia'])) {
			$model->setAttributes($_POST['Tipologia']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Tipologia');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Tipologia('search');
		$model->unsetAttributes();

		if (isset($_GET['Tipologia']))
			$model->setAttributes($_GET['Tipologia']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}