<div class="wide form">

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<div class="col-md-2">
			<?php echo $form->textFieldGroup($model, 'id'); ?>
		</div>
		<div class="col-md-4">

			<?php echo $form->dropDownListGroup($model,'grupo_id',array('widgetOptions' => array('data' => GxHtml::listDataEx(Grupo::model()->findAllAttributes(null, true), 'id', 'grupo'),'htmlOptions'=>array('prompt'=>'Selecione...')))); ?>
		</div>


		<div class="col-md-4">
			<?php echo $form->textFieldGroup($model, 'nome', array('maxlength' => 255)); ?>
		</div>
		<div class="col-md-2">
		  <?php  echo $form->dropDownListGroup($model,'status',array(
					'widgetOptions' => array(
						'data' => array('1'=>'Ativo','0'=>'Inativo'),
						'options' => array(

							'placeholder' => $model->getAttributeLabel('status'),
						),
					),
				)); ?>
		</div>
	</div><!-- row -->

		<div class="row">
			<div class="col-md-6">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'size'=>'large',
					'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
					'label'=>'Buscar',
				)); ?>
			</div>
		</div><!-- row -->

<?php $this->endWidget(); ?>

</div><!-- search-form -->
