<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->title_action = 'Visualizando ' . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model));


?>

<?php 


$this->widget(
    'booster.widgets.TbButtonGroup',
    array(
        'context' => 'primary',
        // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons' => array(
            array('label' => 'Ações', 'url' => '#'),
            array(
                'items' => array(
					array('label' => 'Editar '.$model->label(), 'url'=>array('update', 'id' => $model->id), 'context'=>'primary'),
					array('label'=>'Excluir ' . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Deseja realmente excluir este item?')),
					array('label' => 'Cadastrar novo', 'url'=>array('create'), 'context'=>'primary'),
					array('label' => 'Grade de '.$model->label(2), 'url'=>array('admin'), 'context'=>'primary'),
                )
            ),
        ),
    )
);

?>



<div class="well" style="background-color:#fff; margin-top:20px;">
	<?php $this->widget('zii.widgets.CDetailView', array(
		'data' => $model,
		'attributes' => array(
			'id',

			'nome',
			array(
				'name' => 'idGrupo',
				'type' => 'raw',
				'value' => $model->idGrupo !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idGrupo)), array('grupo/view', 'id' => GxActiveRecord::extractPkValue($model->idGrupo, true))) : null,
			),
			array(
				'name' => 'status',
				'type' => 'raw',
				'value'=> UtilModel::statusAtual($model->status),
			),

		),
	)); ?>
</div>
