<?php

class TipoInventarioController extends GxController {


	public function actionCreate() {
		$model = new TipoInventario;


		if (isset($_POST['TipoInventario'])) {
			$model->setAttributes($_POST['TipoInventario']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'TipoInventario');


		if (isset($_POST['TipoInventario'])) {
			$model->setAttributes($_POST['TipoInventario']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'TipoInventario');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new TipoInventario('search');
		$model->unsetAttributes();

		if (isset($_GET['TipoInventario']))
			$model->setAttributes($_GET['TipoInventario']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}