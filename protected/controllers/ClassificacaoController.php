<?php

class ClassificacaoController extends GxController {

	public function actionCreate() {
		$model = new Classificacao;


		if (isset($_POST['Classificacao'])) {
			$model->setAttributes($_POST['Classificacao']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Classificacao');


		if (isset($_POST['Classificacao'])) {
			$model->setAttributes($_POST['Classificacao']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Classificacao');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Classificacao('search');
		$model->unsetAttributes();

		if (isset($_GET['Classificacao']))
			$model->setAttributes($_GET['Classificacao']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}