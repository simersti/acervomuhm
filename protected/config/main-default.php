<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Muhm WEB',

	'sourceLanguage'=>'pt_br',
	'language'=>'pt_br', 
 	'timeZone' => 'America/Sao_Paulo',

	'defaultController'=>'site/login',
	
	// preloading 'log' component
	'preload'=>array('log', 'booster'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.validators.*',
		'ext.giix.components.*',
 		'ext.LdapUserIdentity.*',
		'ext.highcharts.*',
		'ext.pdffactory.*',
		'application.pdf.docs.*',
		'ext.PHPExcel.*',
		'ext.tcpdf.*',
		'ext.PHPMailer-master.*',
	),
 
	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'muhmweb',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		
			'generatorPaths' => array(
				'ext.giix.generators', 
			    'ext.bootstrap.gii',
			    'ext.mpgii',
       		),
		),
		
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
				
		'LdapUserIdentity'	=>array(
            'class'			=> 'ext.ldap.components.LdapUserIdentity',
            //'host'			=> '192.168.0.10',
	    	'host'			=> '192.168.0.201',
			'port'			=> '389',
			//'userDomain'	=> '@simers.local',
			'userDomain'	=> '@simers-rs.local',
			//'dn'		=> 'dc=simers,dc=local',
			'dn'		=> 'dc=simers-rs,dc=local',
   		),
   		
		'booster' => array(
	     	'class' 			=> 'ext.booster.components.Booster',
	     	'responsiveCss' 	=> true,
       		'coreCss'			=> true,
			'bootstrapCss'		=> true, 
            'yiiCss'			=> true,
            'enableJS'			=> true,   
            'enableNotifierJS' 	=> true,
            'enableBootboxJS'	=> true,
            'maskedInput'		=> true,
            // 'fontAwesomeCss'	=> true,
      	),
      	

      	'Notification' => array(
      		'class'		=> 'Notification',
     		'modelMap' 	=> array(
					array(	
						'model' 	=> 'Acao', 
						'method' 	=> 'getAcoesExpirar', 
						'view' 		=> 'notificacao'
					),
      		)
      	),
      	'sendemail' => array(
      		'class'		=> 'Sendemail',
     		'smtp' 	=> array(
					'Host' 		=> 'smtp.mandrillapp.com', 
					'Username' 	=> 'evertonjuru@gmail.com', 
					'Password' 	=> 'WJdAIi-UJCpo17gFRGv64g',
					'Port' 		=> 587,
      		)
      	),
      	
      	'ePdf' => array(
      		'class'		=> 'ext.yii-pdf.EYiiPdf',
      		'params'	=> array(
      				'mpdf'		=> array(
      					'librarySourcePath' => 'application.vendors.mpdf.*',
      					'constants'         => array(
      							'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
      					),
      					'class'=>'mpdf',
      				),
      				'HTML2PDF' 	=> array(
	      				'librarySourcePath' => 'application.vendors.html2pdf.*',
      					'classFile'         => 'html2pdf.class.php', // For adding to
      				),
      		),
      	),
      	
 		 'pdfFactory'=>array(
            'class'=>'ext.pdffactory.EPdfFactory',
 
            'tcpdfPath'=>'application.extensions.tcpdf', //=default: the path to the tcpdf library
            //'fpdiPath'=>'ext.pdffactory.vendors.fpdi', //=default: the path to the fpdi library
 
            //the cache duration
            'cacheHours'=>0, //-1 = cache disabled, 0 = never expires, hours if >0
 
             //The alias path to the directory, where the pdf files should be created
            'pdfPath'=>'application.runtime.pdf',
 
            //The alias path to the *.pdf template files
            //'templatesPath'=>'application.pdf.templates', //= default
 
            //the params for the constructor of the TCPDF class  
            // see: http://www.tcpdf.org/doc/code/classTCPDF.html 
            'tcpdfOptions'=>array(
                  /* default values
                    'format'=>'A4',
                    'orientation'=>'P', //=Portrait or 'L' = landscape
                    'unit'=>'mm', //measure unit: mm, cm, inch, or point
                    'unicode'=>true,
                    'encoding'=>'UTF-8',
                    'diskcache'=>false,
                    'pdfa'=>false,
                   */
            )
         ),
      	
      	
		// uncomment the following to enable URLs in path-format
		
		/*'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName' => false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),*/
		
      	
		// uncomment the following to use a MySQL database

		/*'dbxxx'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=novomuhmweb', //host + nome da base
			'emulatePrepare' => true,
			'username' => 'root', //usuário
			'password' => '', //senha
			'charset' => 'utf8',
			'enableProfiling' => true,
		),*/

		// database settings are configured in database.php
	/*	'db'=>array(
	        'class'				=> 'CDbConnection',
			'connectionString'	=> 'mysql:host=192.168.0.8;dbname=novomuhmweb',
			'emulatePrepare' 	=> true,
			'username' 			=> 'root',
			'password' 			=> '4ZQu43fg3Hn79U!@#',
			'charset' 			=> 'utf8',
			'enableProfiling' 	=> true,
		),*/


		'db'=>array(
			'connectionString'	=> 'mysql:host=192.168.0.104;dbname=novomuhmweb',
			'emulatePrepare' 	=> true,
			'username' 			=> 'root',
			'password' 			=> 'cvBtUeS1m3r5*918!',
			'charset' 			=> 'utf8',
			'enableProfiling' 	=> true,
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'trace, info, error, warning',
				),
				// uncomment the following to show log messages on web pages
				
				//array('class'=>'CWebLogRoute',),
				
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'		=>'luciano.bobsin@simers.org.br',
		/**
		 * DEFINICOES DOS UPLOADS
		 **/
		'fileUploadConfig'	=>
		array(
			'JS' => array(
				'type'	=> 'gif|jpg|jpeg|png',
				'size'	=> 1000000, #1MB [https://github.com/blueimp/jQuery-File-Upload/wiki/Options]
			),
			'PHP' => array(
				'typeImage'	=> 'gif,jpg,jpeg,png',
				///'typeDoc'	=> 'docx,xls,xlsx,pdf,odt,ods',
				'type'		=> 'gif,jpg,jpeg,png',
				'size'	=> '1048576', #1048576 bytes = 1Mb
			),
		),
		'maxfileupload'	=> 40,
		'pathAnexo'		=> 'images/imagens/',
		'pathBiblioteca'=> 'images/biblioteca/',
		'pathArquivistico'=> 'images/arquivistico/',
		'pathInventario'=> 'images/tridimensional/',
	),
);
