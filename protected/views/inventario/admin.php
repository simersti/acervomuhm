<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);

$this->title_action = GxHtml::encode($model->label(2));

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('inventario-grid', {
		data: $(this).serialize()
	});
	return false;
});

");

$this->widget(
    'booster.widgets.TbButtonGroup',
    array(
        'buttons' => array(
            array(
                'buttonType'    => 'link',
                'label'         => 'Cadastrar Novo '.$model->label(),
                'url'           => array('create'),
                'context'       => 'primary'
            ),
        ),
    )
);

?>




<?php $this->widget('booster.widgets.TbExtendedGridView', array(
	'id' => 'inventario-grid',
	'type' => 'striped bordered condensed hover',
	'dataProvider' => $model->search(),
	'filter' => $model,
    'pager' => array(
        'class' => 'booster.widgets.TbPager', // **use extended CLinkPager class**
        'cssFile' => false, //prevent Yii autoloading css
        'header' => false, // hide 'go to page' header
        'firstPageLabel' => '&lt;&lt;', // change pager button labels
        'prevPageLabel' => '&lt;',
        'nextPageLabel' => '&gt;',
        'lastPageLabel' => '&gt;&gt;',
        'displayFirstAndLast' => true,
    ),
	'columns' => array(	
		'numero_inventario',
		'numero_anterior',		
		'nome',
		array(
				'name'      => 'colecao_id',
				'value'     => 'GxHtml::valueEx($data->colecao)',				
				'filter'    => GxHtml::listDataEx(Colecao::model()->findAllAttributes(null, true)),
		),
		array(
				'name'      => 'sub_colecao_id',
				'value'     => 'GxHtml::valueEx($data->subColecao)',				
				'filter'    => GxHtml::listDataEx(SubColecao::model()->findAllAttributes(null, true)),
		),				
		array(
			'name'           =>'ativo',
			'value'          =>'UtilModel::statusAtual($data->ativo)',
			'filter'         => array('1'=>'Ativo','0'=>'Inativo'),
			'htmlOptions'    => array('style'=>'width:5%;')

		),				
		array(
			'header' 		=> Yii::t('ses', 'Ações'),
			'class'			=> 'booster.widgets.TbButtonColumn',
			'template'		=> '{desdobramento_sim}{desdobramento_nao} | {imagens_sim}{imagens_nao} | {update} | {view} | {publicar}{despublicar} | {delete}|{clonar}',
			'buttons'=>array(
                'desdobramento_sim'=>array(
                    'url'       => 'Yii::app()->createUrl("desdobramento/", array("id"=>$data->id))',
                    'icon'      => 'glyphicon glyphicon-menu-hamburger',
                    'options'   => array('class'=>'view','style'=>'color:green;'),
                    'label'     => 'Desdobramento',
                    'visible'   => '$data->desdobramentos!=null?1:0'
                ),			
                'desdobramento_nao'=>array(
                    'url'       => 'Yii::app()->createUrl("desdobramento/", array("id"=>$data->id))',
                    'icon'      => 'glyphicon glyphicon-menu-hamburger',
                    'options'   => array('class'=>'view','style'=>'color:red;'),
                    'label'     => 'Desdobramento',
                    'visible'   => '$data->desdobramentos==null?1:0'
                ),			                
                'imagens_sim'=>array(
                    'url'       => 'Yii::app()->createUrl("inventario/imagens/", array("id"=>$data->id))',
                    'icon'      => 'glyphicon glyphicon-picture',
                    'options'   => array('class'=>'view','style'=>'color:green;'),
                    'label'     => 'Contém Imagens Cadastradas',
                    'visible'   => '$data->inventarioImagem!=null?1:0'
                ),
                'imagens_nao'=>array(
                    'url'       => 'Yii::app()->createUrl("inventario/imagens/", array("id"=>$data->id))',
                    'icon'      => 'glyphicon glyphicon-picture',
                    'options'   => array('class'=>'view','style'=>'color:red;'),
                    'label'     => 'Não Contém Imagens Cadastradas',
                    'visible'   => '$data->inventarioImagem==null?1:0'
                ),                			                
                'update'=>array(                    
                    'label'=>'Editar',
                ),                                          
                'view'=>array(                    
                    'label'=>'Visualizar',
                ),
                'publicar'=>array(
                    'url'       => '$data->id',
                    'icon'      => 'glyphicon glyphicon-globe',
	                'options'   => array('style'=>'color:red;'),
                    'click'     => "function( e ){e.preventDefault();  publicarCatalogo( $( this ).attr( 'href' ) ,'inventario', 'inventario-grid'); }",
                    'label'     => 'Não Publicado no Site',
                    'visible'   => '$data->catalogo==0?1:0'
                ),
                'despublicar'=>array(
                    'url'       => '$data->id',
                    'icon'      => 'glyphicon glyphicon-globe',
	                'options'   => array('style'=>'color:green;'),
                    'click'     => "function( e ){e.preventDefault();  publicarCatalogo( $( this ).attr( 'href' ) ,'inventario', 'inventario-grid' ); }",
                    'label'     => 'Publicado no Site',
                    'visible'   => '$data->catalogo==1?1:0'
                ),                
                'delete'=>array(                    
                    'label'=>'delete',
                ),
                'clonar'=>array(
                    'url'=> 'Yii::app()->createUrl("inventario/clonar", array("id"=>$data->id))',
                    'icon'=>'glyphicon glyphicon-copy',
                    'options'=>array('class'=>'view'),
                    'label'=>'Clonar',
                ),                  
    		),
			'headerHtmlOptions'	=> array(
                'class'=>'col-sm-1 text-center'
            ),
			'htmlOptions'	=> array(
                'style' => 'vertical-align: middle; width:30%;',
                'class' => 'text-center'
            ),
		),
	),
)); ?>
