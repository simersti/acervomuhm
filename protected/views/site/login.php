<?php 

	 $form = $this->beginWidget('booster.widgets.TbActiveForm',
		array(	'id'					=> 'login-form',
			'enableClientValidation'	=> true,
			'clientOptions'				=> array('validateOnSubmit'=>true,),
			'htmlOptions' 				=> array(),
		)
	); 
						
	echo $form->errorSummary($model);

?>
<div class="form-login form-signin">
<center><img src="images/logo-simers.jpg"></center>
	<div class="alert alert-block alert-danger" id="login-form_es_" style="display:none">
		<p>Por favor, corrija os seguintes erros:</p>
	</div>
	<div class="form-group">
		<div class="col-sm-12">
		<?php 
			echo $form->textFieldGroup($model, 'username',	
				array(
					'maxlength'		=> 70,
				),
				array(	
					'labelOptions' 	=> array('label' => false),
					'errorOptions' 	=> array('label'=> false),
				)
			);
		?>
		</div>
		<div class="col-sm-12">
		<?php 
			echo $form->passwordFieldGroup($model, 'password',	
				array(	'maxlength'		=> 40,
				),
				array(	'labelOptions' 	=> array('label' => false),
						'errorOptions' 	=> array('style'=> 'display: none;'),
				)
			); 
		?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-12">
		<?php 
			$this->widget('booster.widgets.TbButton',
				array(
					'label'			=> 'Acessar',
					'context' 		=> 'danger',
					'icon'			=> 'glyphicon glyphicon-ok',
					// 'size' 			=> 'small',
					'buttonType'	=> 'submit',
					'htmlOptions'	=> array()
				)
			);
							
			$this->endWidget();
		?>
		</div>
	</div>
</div>
