<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);

$this->title_action = GxHtml::encode($model->label(2));

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('movimentacao-biblioteca-grid', {
		data: $(this).serialize()
	});
	return false;
});

");

$this->widget(
    'booster.widgets.TbButtonGroup',
    array(
        'buttons' => array(
            array('buttonType'=>'link' ,'label' => 'Cadastrar Nova '.$model->label(), 'url'=>array('create'), 'context'=>'primary'),
        ),
    )
);

?>

<?php $this->widget('booster.widgets.TbExtendedGridView', array(
	'id' => 'movimentacao-biblioteca-grid',
	'type' => 'striped bordered condensed hover',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'codigo',
		'instituicao',
		'motivo',
		'data_saida',
		array(
				'name'=>'tipo_movimentacao_id',
				'value'=>'GxHtml::valueEx($data->tipoMovimentacao)',				
				'filter'=>GxHtml::listDataEx(TipoMovimentacao::model()->findAllAttributes(null, true)),
				),	
		array(
			'name'=>'ativo',
			'value'=>'UtilModel::statusAtual($data->ativo)',
			'filter'=>array('1'=>'Ativo','0'=>'Inativo'),
			'htmlOptions'=>array('style'=>'width:10%;')

		),			
		array(
			'header' 		=> Yii::t('ses', 'Ações'),
			'class'			=> 'booster.widgets.TbButtonColumn',
			'template'		=> '{receber} | {delete}',
			'buttons'=>array(
                'receber'=>array(
                    'url'=> 'Yii::app()->createUrl("movimentacaoBiblioteca/receberItens", array("id"=>$data->id))',
                    'icon'=>'glyphicon glyphicon-retweet',
                    'options'=>array('class'=>'view'),
                    'label'=>'Receber Itens',
                ),
                'delete'=>array(                    
                    'label'=>'delete',
                ),

    		),
			'headerHtmlOptions'	=> array('class'=>'col-sm-1 text-center'),
			'htmlOptions'	=> array('style' => 'vertical-align: middle; width:20%;', 'class' => 'text-center'),
		),
	),
)); ?>