<?php
/* @var $this MedicosController */
/* @var $model Medicos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'crm'); ?>
		<?php echo $form->textField($model,'crm',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nome'); ?>
		<?php echo $form->textField($model,'nome',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apelido'); ?>
		<?php echo $form->textField($model,'apelido',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rg'); ?>
		<?php echo $form->textField($model,'rg',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cpf'); ?>
		<?php echo $form->textField($model,'cpf',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'inscricao_municipal'); ?>
		<?php echo $form->textField($model,'inscricao_municipal',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datanascimento'); ?>
		<?php echo $form->textField($model,'datanascimento'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sexo'); ?>
		<?php echo $form->textField($model,'sexo',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cartao_numero'); ?>
		<?php echo $form->textField($model,'cartao_numero',array('size'=>16,'maxlength'=>16)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cartao_nome'); ?>
		<?php echo $form->textField($model,'cartao_nome',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cartao_gerado'); ?>
		<?php echo $form->textField($model,'cartao_gerado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'codtipoassociado'); ?>
		<?php echo $form->textField($model,'codtipoassociado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'formapgto'); ?>
		<?php echo $form->textField($model,'formapgto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contribuicaoanual'); ?>
		<?php echo $form->textField($model,'contribuicaoanual'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dataassociacao'); ?>
		<?php echo $form->textField($model,'dataassociacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'residente'); ?>
		<?php echo $form->textField($model,'residente',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dataformatura'); ?>
		<?php echo $form->textField($model,'dataformatura'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dddcelular'); ?>
		<?php echo $form->textField($model,'dddcelular',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'celular'); ?>
		<?php echo $form->textField($model,'celular',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dddcelular2'); ?>
		<?php echo $form->textField($model,'dddcelular2',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'celular2'); ?>
		<?php echo $form->textField($model,'celular2',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dddcelular3'); ?>
		<?php echo $form->textField($model,'dddcelular3',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'celular3'); ?>
		<?php echo $form->textField($model,'celular3',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emailsecretaria'); ?>
		<?php echo $form->textField($model,'emailsecretaria',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emailsimers'); ?>
		<?php echo $form->textField($model,'emailsimers',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gerenterelac'); ?>
		<?php echo $form->textField($model,'gerenterelac',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datatermino'); ?>
		<?php echo $form->textField($model,'datatermino'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datanovotermino'); ?>
		<?php echo $form->textField($model,'datanovotermino'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datajubilamento'); ?>
		<?php echo $form->textField($model,'datajubilamento'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datareassociacao'); ?>
		<?php echo $form->textField($model,'datareassociacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datafimresidente'); ?>
		<?php echo $form->textField($model,'datafimresidente'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ghc'); ?>
		<?php echo $form->textField($model,'ghc',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unimed'); ?>
		<?php echo $form->textField($model,'unimed',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unicred'); ?>
		<?php echo $form->textField($model,'unicred',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'municipario'); ?>
		<?php echo $form->textField($model,'municipario',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sindical'); ?>
		<?php echo $form->textField($model,'sindical',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'socio_amerers'); ?>
		<?php echo $form->textField($model,'socio_amerers'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ano_residencia'); ?>
		<?php echo $form->textField($model,'ano_residencia',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'espec_residencia'); ?>
		<?php echo $form->textField($model,'espec_residencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'local_residencia'); ?>
		<?php echo $form->textField($model,'local_residencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'assoc_residencia'); ?>
		<?php echo $form->textField($model,'assoc_residencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dataassoc_amerers'); ?>
		<?php echo $form->textField($model,'dataassoc_amerers'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datadesassoc_amerers'); ?>
		<?php echo $form->textField($model,'datadesassoc_amerers'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'motivo'); ?>
		<?php echo $form->textField($model,'motivo',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'motivo_obs'); ?>
		<?php echo $form->textField($model,'motivo_obs',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'motivo_usuario'); ?>
		<?php echo $form->textField($model,'motivo_usuario',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ref_hospital'); ?>
		<?php echo $form->textField($model,'ref_hospital'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'senha'); ?>
		<?php echo $form->textField($model,'senha',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'senhaunisimers'); ?>
		<?php echo $form->textField($model,'senhaunisimers',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'revisado'); ?>
		<?php echo $form->textField($model,'revisado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'revisor'); ?>
		<?php echo $form->textField($model,'revisor',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_revisao'); ?>
		<?php echo $form->textField($model,'data_revisao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_alteracao'); ?>
		<?php echo $form->textField($model,'data_alteracao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario'); ?>
		<?php echo $form->textField($model,'usuario',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'recebe_email'); ?>
		<?php echo $form->textField($model,'recebe_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'recebe_torpedos'); ?>
		<?php echo $form->textField($model,'recebe_torpedos'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'capitalinicial'); ?>
		<?php echo $form->textField($model,'capitalinicial',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cartao_enviado'); ?>
		<?php echo $form->textField($model,'cartao_enviado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_geracao_cartao'); ?>
		<?php echo $form->textField($model,'data_geracao_cartao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_envio_cartao'); ?>
		<?php echo $form->textField($model,'data_envio_cartao'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->