<?php

class DesdobramentoController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Desdobramento'),
		));
	}

	public function actionCreate() {
		$model = new Desdobramento;

		if (isset($_POST['Desdobramento'])) {
			$model->setAttributes($_POST['Desdobramento']);
			$model->inventario_id = $_GET['id'];

			/*cria o numero apartir do numero do inventario + o total de desdobramentos + 1 */
			$count = Desdobramento::Model()->count("inventario_id=:inventario_id", array("inventario_id" => $_GET['id']));
			$inventario = Inventario::Model()->find("id=:id", array("id" => $_GET['id']));
			$count ++;	
			$model->numero = $inventario->numero_inventario.'.'.$count;		
			

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('&id='.$_GET['id']));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Desdobramento');


		if (isset($_POST['Desdobramento'])) {
			$model->setAttributes($_POST['Desdobramento']);

			if ($model->save()) {
				$inventario_id = $model->inventario_id;
				$model->setInventario($inventario_id);
				$this->redirect(array('&id='.$model->getInventario()));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Desdobramento');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('&id='.$model->inventario_id));
		}
	}	

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Desdobramento('search');
		$model->unsetAttributes();

		if(isset($_GET['id'])){	
			$model->setInventario($_GET['id']);			
		}

		$inventario_id = $model->getInventario();

		$model2=$this->loadModel($inventario_id, 'Inventario');		

		$this->render('admin', array(
			'model' => $model,
			'model2' => $model2,
		));
	}

}