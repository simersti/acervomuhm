<div class="form well">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'acondicionado-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Os campos com <span class="required">*</span> são obrigatórios.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'nome', array('maxlength' => 255)); ?></div>
		</div><!-- row -->
		<div class="row">
			<div class="col-md-6">				
				<?php echo $form->label($model,'ativo'); ?>
				<?php echo $form->error($model,'ativo'); ?>
				<?php echo $form->checkBox($model, 'ativo'); ?>
			</div>
		</div><!-- row -->		
		<div class="row">
			<div class="col-md-6">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'size'=>'large',
					'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
					'label'=>$model->isNewRecord ? 'Cadastrar' : 'Salvar',
				)); ?>
			</div>
		</div><!-- row -->
<?php

$this->endWidget();
?>
</div><!-- form -->