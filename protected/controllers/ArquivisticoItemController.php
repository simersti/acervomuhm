<?php

class ArquivisticoItemController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ArquivisticoItem'),
		));
	}

	public function actionCreate() {
		$model = new ArquivisticoItem;


		if (isset($_POST['ArquivisticoItem'])) {
			$model->setAttributes($_POST['ArquivisticoItem']);
			$model->arquivistico_id = $_GET['id'];

			/*cria o numero apartir do numero do arquivistico + o total de itens + 1 */
			$count = ArquivisticoItem::Model()->count("arquivistico_id=:arquivistico_id", array("arquivistico_id" => $_GET['id']));
			$arquivistico = Arquivistico::Model()->find("id=:id", array("id" => $_GET['id']));
			$count ++;	
			$model->codigo = $arquivistico->codigo.'.'.$count;		


			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('&id='.$_GET['id']));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ArquivisticoItem');


		if (isset($_POST['ArquivisticoItem'])) {
			$model->setAttributes($_POST['ArquivisticoItem']);

			if ($model->save()) {
				$arquivistico_id = $model->arquivistico_id;
				$model->setArquivistico($arquivistico_id);
				$this->redirect(array('&id='.$model->getArquivistico()));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'ArquivisticoItem');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('&id='.$model->arquivistico_id));
		}
	}	

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new ArquivisticoItem('search');
		$model->unsetAttributes();

		if(isset($_GET['id'])){	
			$model->setArquivistico($_GET['id']);			
		}

		$arquivistico_id = $model->getArquivistico();

		$model2=$this->loadModel($arquivistico_id, 'Arquivistico');		

		$this->render('admin', array(
			'model' => $model,
			'model2' => $model2,
		));
	}	
}