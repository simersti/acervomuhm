	<div class="row box-movimentacao-table">

		<div class="col-md-12">
			<?php if(!empty($grid)){ ?>

			<table class="table  table-hover table-bordered table-striped" style="background:#fff;">
			  
			  <thead>
			    <tr class="active">
			      <?php if(!$is_recebimento_itens){ ?>
			      <th style="width:20px;">
			        Incluir
			      </th>
			      <?php } ?>
			      <th style="width:300px;">
			        Código Arquivístico
			      </th>
			      <th>
			        Descrição Arquivístico
			      </th>			      
			      <th>
			        Código Item
			      </th>			      
			      <th>
			        Descrição Item
			      </th>			      			      			      
			    </tr>
		  	  </thead>

			  <tbody>
			  	<?php foreach ($grid as $ref => $grid_) { ?>
			    <tr class="danger">
			      <?php if(!$is_recebimento_itens){ ?>
			      <td style="text-align: center;">
					<?php
						echo CHtml::checkBox(
							'itens['.$ref.']['.$grid_['arquivistico_id'].'][incluir_item]',
							1,	
							array(
								'value'=>$grid_['id'],
							)
						); 
					?>	
			      </td>
			      <?php } ?>
			      <td>
			        <?php echo $grid_['codigo_arquivistico']; ?>
			      </td>			      
			      <td>
			        <?php echo $grid_['descricao_arquivistico']; ?>
			      </td>			      
			      <td>
			        <?php echo $grid_['codigo_arquivistico_item']; ?>
			      </td>			      
			      <td>
			        <?php echo $grid_['descricao_arquivistico_item']; ?>
			      </td>			      
			      
			    </tr>
			    <?php } ?>
		  	  </tbody>

		  	</table>

			<?php }else{ ?>

				<h3>Não existem itens cadastrados para esta movimentação!</h3>

			<?php } ?>
		</div>
	</div>