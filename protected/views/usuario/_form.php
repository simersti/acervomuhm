<div class="form well">


<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'usuario-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Os campos com <span class="required">*</span> são obrigatórios.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'nome', array('maxlength' => 255)); ?></div>
			<div class="col-md-2"><?php echo $form->textFieldGroup($model, 'login', array('maxlength' => 255)); ?></div>
			<div class="col-md-2">
				<?php echo $form->select2Group($model, 
					'grupo_id',
					array(
						'label' => 'Grupo',
						'widgetOptions' => array(
							'data' => GxHtml::listDataEx(Grupo::model()->findAll('ativo = 1 and excluido = 0'), 'id', 'nome'),
							'options' => array(
								'placeholder' => $model->getAttributeLabel('grupo'),
							),
						),
					)
				);
				?>
			</div>
			<div class="col-md-2">
			  <?php  echo $form->dropDownListGroup($model,'permissao',array(
						'widgetOptions' => array(
							'data' => array(''=>'Selecione','1'=>'Administrador Master','2'=>'Administrador','3'=>'Usuário'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('status'),
							),
						),
					)); ?>
			</div>
			<div class="col-md-1">
			  <?php  echo $form->dropDownListGroup($model,'status',array(
						'widgetOptions' => array(
							'data' => array('1'=>'Ativo','0'=>'Inativo'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('status'),
							),
						),
					)); ?>
			</div>
		</div><!-- row -->
		<div class="row">
			<div class="col-md-6">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'size'=>'large',
					'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
					'label'=>$model->isNewRecord ? 'Cadastrar' : 'Salvar',
				)); ?>
			</div>
		</div><!-- row -->
<?php

$this->endWidget();
?>
</div><!-- form -->