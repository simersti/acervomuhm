<?php

class MovimentacaoArquivisticoItensController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'MovimentacaoArquivisticoItens'),
		));
	}

	public function actionCreate() {
		$model = new MovimentacaoArquivisticoItens;


		if (isset($_POST['MovimentacaoArquivisticoItens'])) {
			$model->setAttributes($_POST['MovimentacaoArquivisticoItens']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'MovimentacaoArquivisticoItens');


		if (isset($_POST['MovimentacaoArquivisticoItens'])) {
			$model->setAttributes($_POST['MovimentacaoArquivisticoItens']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'MovimentacaoArquivisticoItens')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('MovimentacaoArquivisticoItens');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new MovimentacaoArquivisticoItens('search');
		$model->unsetAttributes();

		if (isset($_GET['MovimentacaoArquivisticoItens']))
			$model->setAttributes($_GET['MovimentacaoArquivisticoItens']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}