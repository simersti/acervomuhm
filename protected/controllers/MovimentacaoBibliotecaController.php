<?php

class MovimentacaoBibliotecaController extends GxController {

	public function actionCreate() {
		$model = new MovimentacaoBiblioteca;

		$biblioteca = new Biblioteca('search');
		
		if (isset($_POST['MovimentacaoBiblioteca'])) {
			$model->setAttributes($_POST['MovimentacaoBiblioteca']);

			/*seta o id do usuario para o usuario que está fazendo a transação*/
			$model->usuario_id = Yii::app()->user->id;

			if ($model->save()) {
				
				foreach ($_POST['itens'] as $key => $item) {

					/*Salva os itens no movimentação itens*/
					foreach ($item as $key => $value) {

						$model2 = new MovimentacaoBibliotecaItens;						

						$model2->biblioteca_id = $item['incluir_item'];
						$model2->movimentacao_biblioteca_id = $model->id;
						$model2->ativo= 1;

						$model2->save();					
					}

				}				 

				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model, 'biblioteca' => $biblioteca));
	}

	/*
	*  Método responsavel por receber os itens que estão em movimentação
	*
	*/

	public function actionReceberItens($id) {
		$model = $this->loadModel($id, 'MovimentacaoBiblioteca');

		$biblioteca = new Biblioteca('search');
		

		if (isset($_POST['MovimentacaoBiblioteca'])) {		

			$model->setAttributes($_POST['MovimentacaoBiblioteca']);


			$model->observacao = $_POST['MovimentacaoBiblioteca']['observacao'];
			$model->ativo = 0;

			if ($model->save()) {

				$itens = MovimentacaoBibliotecaItens::model()->findAllByAttributes(array('movimentacao_biblioteca_id'=>$id));			
			
				foreach ($itens as $key => $item) {
					$item->ativo = 0;				
					$item->update();
				}

				$this->redirect(array('admin'));
				
			}

		}else{
			/*busca todos os itens que compoe está movimentação*/
		    $itens = Yii::app()->db->createCommand("select * from movimentacao_biblioteca_itens 
		    										where movimentacao_biblioteca_id =".$id)->queryAll();

			$grid = array();
			$i = 0;
			foreach ($itens as $chave => $item) {						
				$grid[$i]['id']= $item['biblioteca_id'];				

				$biblioteca_item = Biblioteca::model()->findByPk($item['biblioteca_id']);

				$grid[$i]['numero']=$biblioteca_item->numero;
				$grid[$i]['titulo']=$biblioteca_item->titulo;

				$i++;											
			}			
		}		

		$this->render('receber_itens', array(
				'model' => $model,
				'biblioteca' => $biblioteca,
				'grid' => $grid,				
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'MovimentacaoBiblioteca');
		$model->excluido = 1;
		$model->ativo = 0;

		if ($model->save()) {

			$itens = MovimentacaoBibliotecaItens::model()->findAllByAttributes(array('movimentacao_biblioteca_id'=>$id));
		
			
			foreach ($itens as $key => $item) {
				$item->ativo = 0;				
				$item->update();
			}
			

			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new MovimentacaoBiblioteca('search');
		$model->unsetAttributes();

		if (isset($_GET['MovimentacaoBiblioteca']))
			$model->setAttributes($_GET['MovimentacaoBiblioteca']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionCreateItens() {
		$model = new MovimentacaoBiblioteca;		
		
		if(isset($_POST['Biblioteca'])){			

			$grid = array();
			$i = 0;
			foreach ($_POST['Biblioteca']['id'] as $key => $value) {
					$itens = Yii::app()->db->createCommand("select id, numero, titulo from 
															biblioteca
															where id =:biblioteca_id and ativo = 1 and excluido = 0")
											->bindValue(":biblioteca_id",(int)$value)											
											->queryAll();

					foreach ($itens as $chave => $item) {

						$biblioteca = $item['id'];							

						$itens = MovimentacaoBibliotecaItens::model()->findAllByAttributes(array('biblioteca_id'=>$biblioteca,
																					   'ativo'=>1));						

						if(!$itens){

							$grid[$i]['id']= $item['id'];							
							$grid[$i]['numero']=$item['numero'];
							$grid[$i]['titulo']=$item['titulo'];

							$i++;
						}									
					}					
			}			
		}else{
			$grid = array();
		}

		/*variavel de controle, para mostrar os checks, pois deve criar os itens na movimentação*/
		$is_recebimento_itens = false;
		$html =  $this->renderPartial(
			'_box_movimentacao', 			
			array(				
				'is_recebimento_itens'=> $is_recebimento_itens,				
				'grid' => $grid,
			),true
		);

		$return["html"] = $html;

		echo json_encode($return);
		die();		
	}	
}