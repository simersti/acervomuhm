<?php
/* @var $this MedicosController */
/* @var $model Medicos */

$this->breadcrumbs=array(
	'Medicoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Medicos', 'url'=>array('index')),
	array('label'=>'Manage Medicos', 'url'=>array('admin')),
);
?>

<h1>Create Medicos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>