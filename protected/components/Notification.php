<?php

/**
 * Classe responsável pelas notificações dis sistema
 * @author Rodrigo Conceição de Araujo
 *
 */
class Notification extends CApplicationComponent{
	
	// -------------------------------------------------------------------------------------------------------------
		/**
	 * Constantes que definie o caminho absoluto da pasta de views
	 * @var String
	 */
	const VIEWS_PATH = 'protected/views/'; 
	
	// -------------------------------------------------------------------------------------------------------------
	
	/**
	 * Atributo que ira receber as otificações do sistema
	 * @var Array
	 */
	private $notification=array();

	
	/**
	 * Atributo que ira receber a model e o método de notificação a ser executado
	 * @var Array
	 */
	public $modelMap=array();
	
	// -------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método de inicialização
	 * @access public  
	 */
	public function init(){
		
		// executa método responsável por capturar as notificações
		$this->run();
	}
	
	// -------------------------------------------------------------------------------------------------------------

	/**
	 * Método responsável por executar a captura da notificações de acordo com o model map
	 *
	 * @author Rodrigo Conceicao de Araujo
	 * @access private
	 * @return void
	 * 
	 * @obs: 
	 * 		-	o nome das views de notificação devem ser padrão: notificacao
	 * 		-	os parametros que são passados para a view são model: instancia da model / data: dados de retorno da execução do método da model
	 * 
	 */
	private function run(){

		// percorre as models definidas na configuração
		foreach ($this->modelMap as $map){
			
			// seta a notificação
			$this->notification[$map['model']] = $this->getRender($map);
		}
	}
	
	// -------------------------------------------------------------------------------------------------------------

	/**
	 * Método responsável por renderizar a view de acordo com parametros e retornar o HTML gerado
	 * 
	 * @author Rodrigo Conceicao de Araujo
	 * @access private
	 * @param Array $map - array com os dados da view a ser renderizado
	 * @return Render HTML
	 */
	private function getRender($map){

		// inicializa variave que ira reber o resultado do buffer
		$response = null;
		
		// instancia a model
		$model = new $map['model']();
			
		// captura os dados da notificação
		$data = $model->$map['method']();

		// verifica se retornou resultado do banco de dados
		if($data){
			
			// inicia processo que ira impedir de apresentar o "HTML" para usuário
			ob_start();
				
			// cria string que ira representar o caminho da view a ser renderizada
			$pathView = self::VIEWS_PATH . strtolower($map['model']) . DIRECTORY_SEPARATOR .$map['view']. '.php';
				
			// renderiza a pagina de ntificações dos avisos
			CConsoleCommand::renderFile($pathView, compact('model', 'data'));
				
			// captura o buffer resultante da renderização da view e armazena
			$response = ob_get_clean();
				
			// destroi a instancia da model
			unset($model);
			
			// retorna o "HTML" renderizado
			return trim($response);
		
		}else{ 
			return NULL;
		}
	}
	
	// -------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método responsável por capturar as notificações de acordo com o model mapa definido
	 * 
	 * @author Rodrigo Conceicao de Araujo
	 * @access public
	 * @return Array com as notificações
	 */
	public function getNotificacoes(){
		
		// retorna as notificações
		return $this->notification;
	}
	
	// -------------------------------------------------------------------------------------------------------------
	
}