<?php

class ModoAquisicaoController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'ModoAquisicao'),
		));
	}

	public function actionCreate() {
		$model = new ModoAquisicao;


		if (isset($_POST['ModoAquisicao'])) {
			$model->setAttributes($_POST['ModoAquisicao']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ModoAquisicao');


		if (isset($_POST['ModoAquisicao'])) {
			$model->setAttributes($_POST['ModoAquisicao']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'ModoAquisicao');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new ModoAquisicao('search');
		$model->unsetAttributes();

		if (isset($_GET['ModoAquisicao']))
			$model->setAttributes($_GET['ModoAquisicao']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}