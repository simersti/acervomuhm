DROP DATABASE atas;
CREATE DATABASE atas;
USE atas;

CREATE TABLE status_ata (
	status_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	status_ata VARCHAR(80) NOT NULL,
	PRIMARY KEY(status_id)
);
INSERT INTO status_ata (status_ata) VALUES ('Aberto');
INSERT INTO status_ata (status_ata) VALUES ('Fechado');


CREATE TABLE tipo_reuniao(
	tipo_reuniao_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	tipo_reuniao VARCHAR(80) NOT NULL,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(tipo_reuniao_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;
INSERT INTO tipo_reuniao (tipo_reuniao) VALUES ('Assembléia');
INSERT INTO tipo_reuniao (tipo_reuniao) VALUES ('Interior');
INSERT INTO tipo_reuniao (tipo_reuniao) VALUES ('Metropolitana');


CREATE TABLE relator(
	relator_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	relator VARCHAR(100) NOT NULL,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(relator_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;
INSERT INTO relator (relator) VALUES ('Amália Ceola');
INSERT INTO relator (relator) VALUES ('Andréia');
INSERT INTO relator (relator) VALUES ('Camila Jonco');
INSERT INTO relator (relator) VALUES ('Mariana Aguiar');
INSERT INTO relator (relator) VALUES ('Mirella');


CREATE TABLE local_ata(
	local_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	local_ata VARCHAR(45) NOT NULL,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(local_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;
INSERT INTO local_ata (local_ata) VALUES ('Auditório SIMERS');
INSERT INTO local_ata (local_ata) VALUES ('Sala da Vice-Presidência');


CREATE TABLE ata(
	ata_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	tipo_reuniao_id INT(11) UNSIGNED NOT NULL,
	relator_id INT(11) UNSIGNED NOT NULL,
	local_id INT(11) UNSIGNED NOT NULL,
	status_id INT(11) UNSIGNED NOT NULL,
	data_reuniao DATETIME NOT NULL,
	participante VARCHAR(200) NOT NULL,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	origin_id int(11) DEFAULT NULL COMMENT 'ID da acao que originou esta no momento da realização de um clone',
	delete_flag int(1) DEFAULT 0 COMMENT 'Se igual a 1, registro excluido, não deve ser apresentado para o usuário',
	PRIMARY KEY(ata_id),
	FOREIGN KEY(tipo_reuniao_id) REFERENCES tipo_reuniao (tipo_reuniao_id),
	FOREIGN KEY(relator_id) REFERENCES relator (relator_id),
	FOREIGN KEY(local_id) REFERENCES local_ata (local_id),
	FOREIGN KEY(status_id) REFERENCES status_ata (status_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;

CREATE TABLE ata_anexo(
	ata_anexo_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	ata_id INT(11) UNSIGNED NOT NULL,
	nome_arquivo VARCHAR(255) NOT NULL,
	file VARCHAR(255) NOT NULL,
	directory VARCHAR(255) NOT NULL,
	mimetype VARCHAR(255) NOT NULL,
	filesize INT(11) NOT NULL,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	data_alteracao TIMESTAMP DEFAULT 0,
	PRIMARY KEY (ata_anexo_id),
	FOREIGN KEY (ata_id) REFERENCES ata (ata_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;



CREATE TABLE responsavel(
	responsavel_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	responsavel VARCHAR(100) NOT NULL,
	STATUS INT(1) DEFAULT 1,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(responsavel_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;
INSERT INTO responsavel (responsavel, STATUS) VALUES ('Vitor Dossa', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Odakhan Picanço', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Denise ', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Tchamacho', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Michele Braun', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Argollo Mendes', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Afonso Araújo', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Fabiana Machado', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('André Gonzáles', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Edilene Appelt', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Diego Baisch', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('João Adams', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Rosane', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Maria Rita de Assis Brasil', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Edson Machado', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Toni Belinaso', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Jorge Eltz', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Clarissa Bassin', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Ana Maria Martins', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Leandro Melo', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Beatriz Valle', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Adriana Rojas', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Cristiana', 1);
INSERT INTO responsavel (responsavel, STATUS) VALUES('Dra. Marise', 1);



CREATE TABLE cidade(
	cidade_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	cidade VARCHAR(45) NOT NULL,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (cidade_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;
INSERT INTO cidade (cidade) VALUES ('Alecrim');
INSERT INTO cidade (cidade) VALUES('Alegrete');
INSERT INTO cidade (cidade) VALUES('Alto Uruguai');
INSERT INTO cidade (cidade) VALUES('Alvorada');
INSERT INTO cidade (cidade) VALUES('Araricá');
INSERT INTO cidade (cidade) VALUES('Bagé');
INSERT INTO cidade (cidade) VALUES('Barracão');
INSERT INTO cidade (cidade) VALUES('Barros Cassal');
INSERT INTO cidade (cidade) VALUES('Boqueirão do Leão');
INSERT INTO cidade (cidade) VALUES('Caçapava do Sul');
INSERT INTO cidade (cidade) VALUES('Cachoeira do Sul');
INSERT INTO cidade (cidade) VALUES('Cachoeirinha');
INSERT INTO cidade (cidade) VALUES('Cachoeirinha e Canoas');
INSERT INTO cidade (cidade) VALUES('Canguçu');
INSERT INTO cidade (cidade) VALUES('Charqueadas');
INSERT INTO cidade (cidade) VALUES('Camaquã');
INSERT INTO cidade (cidade) VALUES('Candiota');
INSERT INTO cidade (cidade) VALUES('Canoas');
INSERT INTO cidade (cidade) VALUES('Carazinho');
INSERT INTO cidade (cidade) VALUES('Cruz Alta');
INSERT INTO cidade (cidade) VALUES('Dom Pedrito');
INSERT INTO cidade (cidade) VALUES('Encruzilhada do Sul');
INSERT INTO cidade (cidade) VALUES('Erechim');
INSERT INTO cidade (cidade) VALUES('Estado');
INSERT INTO cidade (cidade) VALUES('Esteio');
INSERT INTO cidade (cidade) VALUES('Estrela');
INSERT INTO cidade (cidade) VALUES('Estrela Velha');
INSERT INTO cidade (cidade) VALUES('Farroupilha');
INSERT INTO cidade (cidade) VALUES('Frederico Westphalen');
INSERT INTO cidade (cidade) VALUES('Gravataí');
INSERT INTO cidade (cidade) VALUES('Horizontina');
INSERT INTO cidade (cidade) VALUES('Ibirubá');
INSERT INTO cidade (cidade) VALUES('Ijuí');
INSERT INTO cidade (cidade) VALUES('Iraí');
INSERT INTO cidade (cidade) VALUES('Itaqui');
INSERT INTO cidade (cidade) VALUES('Jaguarão');
INSERT INTO cidade (cidade) VALUES('Lagoão');
INSERT INTO cidade (cidade) VALUES('Lagoa Vermelha');
INSERT INTO cidade (cidade) VALUES('Litoral Gaúcho');
INSERT INTO cidade (cidade) VALUES('Mais Médicos');
INSERT INTO cidade (cidade) VALUES('Maquiné');
INSERT INTO cidade (cidade) VALUES('Marques de Souza');
INSERT INTO cidade (cidade) VALUES('Nova Bassano');
INSERT INTO cidade (cidade) VALUES('Osório');
INSERT INTO cidade (cidade) VALUES('Palmeira das Missões');
INSERT INTO cidade (cidade) VALUES('Passo Fundo');
INSERT INTO cidade (cidade) VALUES('Pedras Altas');
INSERT INTO cidade (cidade) VALUES('Pelotas');
INSERT INTO cidade (cidade) VALUES('Piratini');
INSERT INTO cidade (cidade) VALUES('Porto Alegre');
INSERT INTO cidade (cidade) VALUES('Rio Grande');
INSERT INTO cidade (cidade) VALUES('Rio Pardo');
INSERT INTO cidade (cidade) VALUES('Rolante');
INSERT INTO cidade (cidade) VALUES('Rosário do Sul');
INSERT INTO cidade (cidade) VALUES('Santa Cruz');
INSERT INTO cidade (cidade) VALUES('Santa Cruz do Sul');
INSERT INTO cidade (cidade) VALUES('Santa Maria');
INSERT INTO cidade (cidade) VALUES('Santa Rosa');
INSERT INTO cidade (cidade) VALUES('Santo Ãngelo');
INSERT INTO cidade (cidade) VALUES('Santa Vitória do Palmar ');
INSERT INTO cidade (cidade) VALUES('Santana da Boa Vista');
INSERT INTO cidade (cidade) VALUES('Santana do Livramento');
INSERT INTO cidade (cidade) VALUES('Santiago');
INSERT INTO cidade (cidade) VALUES('Santo Cristo');
INSERT INTO cidade (cidade) VALUES('São Borja');
INSERT INTO cidade (cidade) VALUES('São Gabriel');
INSERT INTO cidade (cidade) VALUES('São José do Norte');
INSERT INTO cidade (cidade) VALUES('São Leopoldo');
INSERT INTO cidade (cidade) VALUES('São Lourenço do Sul ');
INSERT INTO cidade (cidade) VALUES('São Miguel das Missões');
INSERT INTO cidade (cidade) VALUES('São Sepé');
INSERT INTO cidade (cidade) VALUES('Severiano de Almeida');
INSERT INTO cidade (cidade) VALUES('Soledade');
INSERT INTO cidade (cidade) VALUES('Taquari');
INSERT INTO cidade (cidade) VALUES('Tenente Portela');
INSERT INTO cidade (cidade) VALUES('Tramandaí');
INSERT INTO cidade (cidade) VALUES('Três de Maio');
INSERT INTO cidade (cidade) VALUES('Três Passos');
INSERT INTO cidade (cidade) VALUES('Triunfo');
INSERT INTO cidade (cidade) VALUES('Turuçu');
INSERT INTO cidade (cidade) VALUES('Uruguaiana');
INSERT INTO cidade (cidade) VALUES('Vacaria');
INSERT INTO cidade (cidade) VALUES('Vale Verde');
INSERT INTO cidade (cidade) VALUES('Viamão');
INSERT INTO cidade (cidade) VALUES('Xangri-Lá');


CREATE TABLE classificacao_acao(
	classificacao_acao_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	classificacao VARCHAR(50) NOT NULL,
	STATUS INT(1) DEFAULT 1,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(classificacao_acao_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Acordo Coletivo de Trabalho',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Amrigs',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Aplicação EC29',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Ação parlamentar',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Ações MPE',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Ações MPT',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Brigada Militar',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('CAPS (todos os tipos)',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Casas de Parto',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('CBHPM',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('CFC Detran',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Conselho Estadual de Saúde',1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Conselho Municipal da Saúde (por cidade)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Contrato', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Contratualização', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Convenções Coletivas de Trabalho', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Cremers', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Código 7', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Datasus', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Delegacia do Trabalho (capital)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Delegacia do Trabalho (interior, município)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Departamento Médico Legal (capital)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Departamento Médico Legal (interior, município)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Departamento Médico Legal - DML', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Difamação', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('EccoSalva', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Empregado Público', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('ESF (para todos os municípios menos POA)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('ESF POA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Excesso de demanda', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Farmácia Estadual', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Farmácia Municipal', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Farmácia Popular', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Farmácia', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Fehosul', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Fundação de Saúde Canoas', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Grupo Hospitalar Conceição - GHC', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Guarda Municipal (município)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Guarda Municipal (POA)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('HMIPV', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('HNSG Canoas', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospitais POA (categoria genérica para os demais h', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital Centenário', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital de Alvorada', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital de Clínicas de Porto Alegre – HCPA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital de Pronto Socorro (municípios)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital de Viamão', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital Eernesto Dorneles – HED', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital Filantrópico', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital Getúlio Vargas', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital Mãe de Deus', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital Padre Geremia', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital Porto Alegre', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital Privado', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital Público (município)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital São Camilo', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Hospital São Lucas (da PUC)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('HPS Canoas', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('HPS', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('HU Canoas', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Imesf POA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Insalubridade', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('IPERGS', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Lei de Responsabilidade Fiscal', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Mesa de Negociação do SUS', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Ministério da Saúde', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Municipalizados POA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Municipários (para todos os municípios menos POA)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Municipários POA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Médico Plantonista', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Médico Residente', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Médico Rotineiro', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Negociação', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Negociações coletivas de trabalho (por cidade e/ou', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('PACS', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Periculosidade', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Perito Médico (DML)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Perito Médico (INSS)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Peritos INSS', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Planos de Saúde', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Policia Civil', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Política de Medicamento', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Pronto Atendimento', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Radimagem', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Regulação Municipal POA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Regulação Município', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Remuneração', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Residência Multiprofissional', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('SAMU (municipal)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('SAMU Estadual', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('SAMU POA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Santa Casa POA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Santa Casa', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Saúde Mental', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Secreataria de Saúde (interior)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Secretaria Estadual de Saúde', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Secretaria Muncipal de Saúde (região metropolitana', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Secretaria Muncipal de Saúde de POA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Segurança', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Serviço de Apoio Diagnóstico', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Serviço de Saúde (descrever os principais)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Sindiberf', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Sindihospa', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Sobreaviso', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('SUS Capital', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Tribunal de Contas da União', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Tribunal de Contas do Estado', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Tribunal Regional do Trabalho', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Unicred', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Unidade Básica de Saúde (capital)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Unidade Básica de Saúde (interior, município)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Unidade Básica de Saúde (metropolitana exceto POA)', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Unidade de Tratamento Intensivo', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Unimed', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('UPA', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Ética Médica', 1);
INSERT INTO classificacao_acao (classificacao,  STATUS) VALUES ('Classificação não definida', 1);


CREATE TABLE grupo(
	grupo_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	grupo VARCHAR(200) NOT NULL,
	STATUS INT(1) DEFAULT 1,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(grupo_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;
INSERT INTO grupo (grupo) VALUES ('4 plantonistas');
INSERT INTO grupo (grupo) VALUES ('Anestesistas e cirurgiões');
INSERT INTO grupo (grupo) VALUES ('Anestesistas');
INSERT INTO grupo (grupo) VALUES ('Cardiologistas');
INSERT INTO grupo (grupo) VALUES ('Centenário');
INSERT INTO grupo (grupo) VALUES ('Cirurgiões Hospital São Vicente');
INSERT INTO grupo (grupo) VALUES ('Cirurgiões');
INSERT INTO grupo (grupo) VALUES ('Clínico Geral');
INSERT INTO grupo (grupo) VALUES ('Clínicos Gerais');
INSERT INTO grupo (grupo) VALUES ('Delegado sindical -GHC');
INSERT INTO grupo (grupo) VALUES ('ESF Divina Providência');
INSERT INTO grupo (grupo) VALUES ('ESF Divina');
INSERT INTO grupo (grupo) VALUES ('ghc');
INSERT INTO grupo (grupo) VALUES ('Honorários');
INSERT INTO grupo (grupo) VALUES ('Hospitais');
INSERT INTO grupo (grupo) VALUES ('Hospital de Alvorada');
INSERT INTO grupo (grupo) VALUES ('Hospital Mãe de Deus');
INSERT INTO grupo (grupo) VALUES ('Hospital São Camilo');
INSERT INTO grupo (grupo) VALUES ('Hospital São José');
INSERT INTO grupo (grupo) VALUES ('Hospital São José do Norte');
INSERT INTO grupo (grupo) VALUES ('HPS Canoas');
INSERT INTO grupo (grupo) VALUES ('HPS');
INSERT INTO grupo (grupo) VALUES ('HPV');
INSERT INTO grupo (grupo) VALUES ('hyju');
INSERT INTO grupo (grupo) VALUES ('IAPI');
INSERT INTO grupo (grupo) VALUES ('IC - FUC');
INSERT INTO grupo (grupo) VALUES ('IMESF');
INSERT INTO grupo (grupo) VALUES ('Infraero');
INSERT INTO grupo (grupo) VALUES ('INSS');
INSERT INTO grupo (grupo) VALUES ('Interior');
INSERT INTO grupo (grupo) VALUES ('kjkjkj');
INSERT INTO grupo (grupo) VALUES ('Mais Médicos');
INSERT INTO grupo (grupo) VALUES ('Maquiné');
INSERT INTO grupo (grupo) VALUES ('Municipalizados');
INSERT INTO grupo (grupo) VALUES ('Municiparios');
INSERT INTO grupo (grupo) VALUES ('Municipários PA');
INSERT INTO grupo (grupo) VALUES ('Municipários');
INSERT INTO grupo (grupo) VALUES ('Municipários- HPS');
INSERT INTO grupo (grupo) VALUES ('Municipários PA');
INSERT INTO grupo (grupo) VALUES ('Municipários POA');
INSERT INTO grupo (grupo) VALUES ('Médicos contratados pelo hospital');
INSERT INTO grupo (grupo) VALUES ('Médicos da UPA');
INSERT INTO grupo (grupo) VALUES ('Médicos do Sport Club Internacional');
INSERT INTO grupo (grupo) VALUES ('Médicos Hospital César Santos');
INSERT INTO grupo (grupo) VALUES ('Médicos Municipários');
INSERT INTO grupo (grupo) VALUES ('Médicos Obstetras');
INSERT INTO grupo (grupo) VALUES ('Médicos');
INSERT INTO grupo (grupo) VALUES ('Médico cubano');
INSERT INTO grupo (grupo) VALUES ('Médicos da Santa Casa');
INSERT INTO grupo (grupo) VALUES ('Médicos de postos de saúde');
INSERT INTO grupo (grupo) VALUES ('Médicos do Pronto Socorro');
INSERT INTO grupo (grupo) VALUES ('Médicos ESF');
INSERT INTO grupo (grupo) VALUES ('Médicos Municipários');
INSERT INTO grupo (grupo) VALUES ('Médicos recém formados');
INSERT INTO grupo (grupo) VALUES ('Médicos Samu');
INSERT INTO grupo (grupo) VALUES ('Médico Uruguaio');
INSERT INTO grupo (grupo) VALUES ('Obstetras do Hospital Universitário');
INSERT INTO grupo (grupo) VALUES ('Obstetras e pediatras');
INSERT INTO grupo (grupo) VALUES ('Obstetras plantão');
INSERT INTO grupo (grupo) VALUES ('Obstetras');
INSERT INTO grupo (grupo) VALUES ('Obstetras/plantão');
INSERT INTO grupo (grupo) VALUES ('Oftalmologistas');
INSERT INTO grupo (grupo) VALUES ('Optometristas');
INSERT INTO grupo (grupo) VALUES ('Pediatras da UFpel');
INSERT INTO grupo (grupo) VALUES ('Pediatras UFpel');
INSERT INTO grupo (grupo) VALUES ('Pediatras');
INSERT INTO grupo (grupo) VALUES ('Plantonistas');
INSERT INTO grupo (grupo) VALUES ('Porto Alegre');
INSERT INTO grupo (grupo) VALUES ('Posto de Saúde Modelo');
INSERT INTO grupo (grupo) VALUES ('Pronto Atendimento 24 horas');
INSERT INTO grupo (grupo) VALUES ('SAMU');
INSERT INTO grupo (grupo) VALUES ('Secretaria Municipal de Saúde');
INSERT INTO grupo (grupo) VALUES ('Sindihospa');
INSERT INTO grupo (grupo) VALUES ('Sport Club Internacional');
INSERT INTO grupo (grupo) VALUES ('Sport Clube Internacional');
INSERT INTO grupo (grupo) VALUES ('SUE');
INSERT INTO grupo (grupo) VALUES ('Tramandaí');
INSERT INTO grupo (grupo) VALUES ('Traumatos');
INSERT INTO grupo (grupo) VALUES ('tres');
INSERT INTO grupo (grupo) VALUES ('UBS São Cristovão');
INSERT INTO grupo (grupo) VALUES ('UPA Zona Norte');
INSERT INTO grupo (grupo) VALUES ('Grupo não definido');
INSERT INTO grupo (grupo) VALUES ('UTI Nossa Senhora das Graças');

CREATE TABLE acao(
	acao_id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	ata_id INT(11) UNSIGNED NOT NULL,
	cidade_id INT(11) UNSIGNED NOT NULL,
	status_id INT(11) UNSIGNED NOT NULL,
	problema BLOB NOT NULL,
	acao BLOB NOT NULL,	
	data_conclusao DATETIME NOT NULL,
	data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	origin_id int(11) default null COMMENT 'ID da acao que originou esta no momento da realização de um clone',
	delete_flag int(1) DEFAULT 0 COMMENT 'Se igual a 1, registro excluido, não deve ser apresentado para o usuário',
	PRIMARY KEY(acao_id),
	FOREIGN KEY(ata_id) REFERENCES ata (ata_id),
	FOREIGN KEY(cidade_id) REFERENCES cidade (cidade_id),
	FOREIGN KEY(status_id) REFERENCES status_ata (status_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;


CREATE TABLE acao_grupo(
	acao_grupo_id INT(11) NOT NULL AUTO_INCREMENT,
	acao_id INT(11) UNSIGNED NOT NULL,
	grupo_id INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (acao_grupo_id),
	FOREIGN KEY (acao_id) REFERENCES acao (acao_id),
	FOREIGN KEY (grupo_id) REFERENCES grupo (grupo_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;


CREATE TABLE acao_classificao(
	acao_classificao_id INT(11) NOT NULL AUTO_INCREMENT,
	acao_id INT(11) UNSIGNED NOT NULL,
	classificacao_acao_id INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (acao_classificao_id),
	FOREIGN KEY (acao_id) REFERENCES acao (acao_id),
	FOREIGN KEY (classificacao_acao_id ) REFERENCES classificacao_acao (classificacao_acao_id )
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;


CREATE TABLE acao_responsavel(
	acao_responsavel_id INT(11) NOT NULL AUTO_INCREMENT,
	acao_id INT(11) UNSIGNED NOT NULL,
	responsavel_id INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (acao_responsavel_id),
	FOREIGN KEY (acao_id) REFERENCES acao (acao_id),
	FOREIGN KEY (responsavel_id) REFERENCES responsavel (responsavel_id)
)ENGINE=INNODB DEFAULT CHARACTER SET=utf8;


	


