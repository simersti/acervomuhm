<?php 
// verifica se existe anexos para ata
$list = $model->inventarioImagem->getData();
if(!empty($list)){	
	
	$box = $this->beginWidget('booster.widgets.TbPanel',
				array(	'title' => InventarioImagem::label(2),
						'headerIcon' => 'glyphicon glyphicon-picture',
						'padContent' => false,
						'htmlOptions' => array('class' => 'bootstrap-widget-table table-responsive'),
				)
	);
		$this->widget('booster.widgets.TbExtendedGridView', array(
				'id' 				=> 'anexos-grid',
				'type'				=> 'striped bordered condensed hover',
				'htmlOptions'		=> array('class' => 'responsive'),
				'enablePagination' 	=> true,
				'template' 			=> "{items}\n{pager}\n{summary}",
				'summaryText' 		=> false,
				'dataProvider' 		=> $model->inventarioImagem,
				'columns' 			=>
				array(
					array(
          			   	   'name'=>'file',
            			   'type'=>'html',
            			   'value'=>'(file_exists("images/tridimensional/".$data->file))?CHtml::image("images/tridimensional/".$data->file,"",array("style"=>"width:70px;height:70px;"))
            			   																:CHtml::image("images/imagem_indisponivel.JPG","",array("style"=>"width:70px;height:70px;"))',
 
        			),
					array(
							'name'				=> 'nome_arquivo',
							'headerHtmlOptions'	=> array('class'=>'col-sm-6 text-left'),
							'htmlOptions' 		=> array('class'=>'text-left'),
							'type'				=> 'raw',
							'value'				=> '$data->nome_arquivo',
					),
					array(
							'name'				=> 'data_criacao',
							'headerHtmlOptions'	=> array('class'=>'col-sm-2 text-left'),
							'htmlOptions' 		=> array('class'=>'text-left'),
							'value'				=> '$data->data_criacao',
					),
					array(
							'header' 			=> Yii::t('ses', 'Ações'),
							'class'				=> 'booster.widgets.TbButtonColumn',
							'template'			=> '{ativa}{desativa} | {download} | {delete}',
							'headerHtmlOptions'	=> array('class'=>'col-sm-4 text-center'),
							'htmlOptions'		=> array('style' => 'vertical-align: middle', 'class' => 'text-center'),
							'buttons'			=> array(
					                'ativa'=>array(
					                    'url' => '$data->id',
					                    'icon'=>'glyphicon glyphicon-star-empty',
						                'options'=>array('style'=>'color:red;'),
					                    'click' => "function( e ){e.preventDefault();  imagemDefault( $( this ).attr( 'href' ) ,'inventario', 'anexos-grid'); }",
					                    'visible'=>'$data->principal==0?1:0',
					                    'label'=>'Não Principal'
					                ),
					                'desativa'=>array(
					                    'url' => '$data->id',
					                    'icon'=>'glyphicon glyphicon-star',
						                'options'=>array('style'=>'color:green;'),
					                    'click' => "function( e ){e.preventDefault();  imagemDefault( $( this ).attr( 'href' ) ,'inventario', 'anexos-grid'); }",
					                    'visible'=>'$data->principal==1?1:0',
					                    'label'=>'Principal'
					                ),									
									'download'	=> array(
										'label'	=> '<span class="glyphicon glyphicon-cloud-download"></span>&nbsp;',
										'url'     => '$data->directory.$data->file',
										'options' => array(
												'title'		=> 'Download',
												'class'		=> 'download',
												'rel'		=> 'gallery',
												'download'	=> 'download',
										),
									),
									'delete'	=> array(
										'url'	=>	'Yii::app()->createUrl("inventario/excluirImagem", array("id"=>$data->id))'
									),
							),
					),
				),
		));
	$this->endWidget();
}
?>

