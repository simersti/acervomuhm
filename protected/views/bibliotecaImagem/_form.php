<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'biblioteca-imagem-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'biblioteca_id'); ?>
		<?php echo $form->dropDownList($model, 'biblioteca_id', GxHtml::listDataEx(Biblioteca::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'biblioteca_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'nome_arquivo'); ?>
		<?php echo $form->textField($model, 'nome_arquivo', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'nome_arquivo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'file'); ?>
		<?php echo $form->textField($model, 'file', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'file'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'directory'); ?>
		<?php echo $form->textField($model, 'directory', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'directory'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'mimetype'); ?>
		<?php echo $form->textField($model, 'mimetype', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'mimetype'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'filesize'); ?>
		<?php echo $form->textField($model, 'filesize'); ?>
		<?php echo $form->error($model,'filesize'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'principal'); ?>
		<?php echo $form->textField($model, 'principal', array('maxlength' => 1)); ?>
		<?php echo $form->error($model,'principal'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'data_criacao'); ?>
		<?php echo $form->textField($model, 'data_criacao'); ?>
		<?php echo $form->error($model,'data_criacao'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'data_alteracao'); ?>
		<?php echo $form->textField($model, 'data_alteracao'); ?>
		<?php echo $form->error($model,'data_alteracao'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton('Save');
$this->endWidget();
?>
</div><!-- form -->