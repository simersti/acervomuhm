<?php

class DoadorController extends GxController {

	public function actionCreate() {
		$model = new Doador;


		if (isset($_POST['Doador'])) {
			$model->setAttributes($_POST['Doador']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Doador');


		if (isset($_POST['Doador'])) {
			$model->setAttributes($_POST['Doador']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Doador');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}
	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Doador('search');
		$model->unsetAttributes();

		if (isset($_GET['Doador']))
			$model->setAttributes($_GET['Doador']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}