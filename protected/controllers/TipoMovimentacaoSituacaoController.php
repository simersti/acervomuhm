<?php

class TipoMovimentacaoSituacaoController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'TipoMovimentacaoSituacao'),
		));
	}

	public function actionCreate() {
		$model = new TipoMovimentacaoSituacao;


		if (isset($_POST['TipoMovimentacaoSituacao'])) {
			$model->setAttributes($_POST['TipoMovimentacaoSituacao']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'TipoMovimentacaoSituacao');


		if (isset($_POST['TipoMovimentacaoSituacao'])) {
			$model->setAttributes($_POST['TipoMovimentacaoSituacao']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'TipoMovimentacaoSituacao');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new TipoMovimentacaoSituacao('search');
		$model->unsetAttributes();

		if (isset($_GET['TipoMovimentacaoSituacao']))
			$model->setAttributes($_GET['TipoMovimentacaoSituacao']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}