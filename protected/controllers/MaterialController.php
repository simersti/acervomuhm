<?php

class MaterialController extends GxController {


	public function actionCreate() {
		$model = new Material;


		if (isset($_POST['Material'])) {
			$model->setAttributes($_POST['Material']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Material');


		if (isset($_POST['Material'])) {
			$model->setAttributes($_POST['Material']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Material');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Material('search');
		$model->unsetAttributes();

		if (isset($_GET['Material']))
			$model->setAttributes($_GET['Material']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}