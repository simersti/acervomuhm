<?php
/* @var $this MedicosController */
/* @var $model Medicos */

$this->breadcrumbs=array(
	'Medicoses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Medicos', 'url'=>array('index')),
	array('label'=>'Create Medicos', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#medicos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Medicoses</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'medicos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'crm',
		'nome',
		'apelido',
		'rg',
		'cpf',
		'inscricao_municipal',
		/*
		'datanascimento',
		'sexo',
		'cartao_numero',
		'cartao_nome',
		'cartao_gerado',
		'codtipoassociado',
		'formapgto',
		'contribuicaoanual',
		'dataassociacao',
		'residente',
		'dataformatura',
		'dddcelular',
		'celular',
		'dddcelular2',
		'celular2',
		'dddcelular3',
		'celular3',
		'email',
		'emailsecretaria',
		'emailsimers',
		'gerenterelac',
		'datatermino',
		'datanovotermino',
		'datajubilamento',
		'datareassociacao',
		'datafimresidente',
		'ghc',
		'unimed',
		'unicred',
		'municipario',
		'sindical',
		'socio_amerers',
		'ano_residencia',
		'espec_residencia',
		'local_residencia',
		'assoc_residencia',
		'dataassoc_amerers',
		'datadesassoc_amerers',
		'motivo',
		'motivo_obs',
		'motivo_usuario',
		'ref_hospital',
		'senha',
		'senhaunisimers',
		'revisado',
		'revisor',
		'data_revisao',
		'data_alteracao',
		'usuario',
		'id',
		'recebe_email',
		'recebe_torpedos',
		'capitalinicial',
		'cartao_enviado',
		'status',
		'data_geracao_cartao',
		'data_envio_cartao',
		'office2_password',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
