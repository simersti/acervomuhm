<?php

Yii::import('application.models._base.BaseArquivisticoItem');

class ArquivisticoItem extends BaseArquivisticoItem
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function setArquivistico($arquivistico_id) {
		$this->arquivistico_id = $arquivistico_id;
	}

	public function getArquivistico() {
		return $this->arquivistico_id;
	}


	public function searchPorArquivistico() {
		$criteria = new CDbCriteria;


		$criteria->compare('id', $this->id);
		$criteria->compare('codigo', $this->codigo, true);
		$criteria->compare('descricao', $this->descricao, true);		
		$criteria->compare('tipo_serie_id', $this->tipo_serie_id);		
		$criteria->compare('ativo', $this->ativo, true);
		$criteria->compare('excluido', 0);
		$criteria->compare('arquivistico_id', $this->arquivistico_id);
		///$criteria->order = 'codigo';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}	
}