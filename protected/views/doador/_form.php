<div class="form well">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'doador-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Os campos com <span class="required">*</span> são obrigatórios.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'nome', array('maxlength' => 255)); ?></div>
			<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'crm', array('maxlength' => 10)); ?></div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'email', array('maxlength' => 255)); ?></div>			
			<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'telefone_fixo', array('maxlength' => 14)); ?></div>			
			<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'telefone_celular', array('maxlength' => 14)); ?></div>						
		</div><!-- row -->

		<div class="row">
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'endereco', array('maxlength' => 255)); ?></div>			
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'numero', array('maxlength' => 20)); ?></div>			
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'complemento', array('maxlength' => 255)); ?></div>			
		</div><!-- row -->

		<div class="row">
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'bairro', array('maxlength' => 255)); ?></div>			
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'cidade', array('maxlength' => 255)); ?></div>	
			<div class="col-md-2">			
				<?php echo $form->labelEx($model,'estado_id'); ?>
				<?php echo $form->dropDownList($model, 'estado_id', GxHtml::listDataEx(Estado::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'estado_id'); ?>								
			</div>				
			<div class="col-md-2"><?php echo $form->textFieldGroup($model, 'cep', array('maxlength' => 50)); ?></div>
		</div><!-- row -->


		<div class="row">
			<div class="col-md-3">
			  <?php  echo $form->dropDownListGroup($model,'ativo',array(
						'widgetOptions' => array(
							'data' => array('1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('ativo'),
							),
						),
					)); ?>
			</div>
		</div><!-- row -->				
		<div class="row">
			<div class="col-md-6">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'size'=>'large',
					'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
					'label'=>$model->isNewRecord ? 'Cadastrar' : 'Salvar',
				)); ?>
			</div>
		</div><!-- row -->
<?php

$this->endWidget();
?>
</div><!-- form -->