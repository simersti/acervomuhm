<?php
/* @var $this MedicosController */
/* @var $model Medicos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'medicos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'crm'); ?>
		<?php echo $form->textField($model,'crm',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'crm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nome'); ?>
		<?php echo $form->textField($model,'nome',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nome'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apelido'); ?>
		<?php echo $form->textField($model,'apelido',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'apelido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rg'); ?>
		<?php echo $form->textField($model,'rg',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'rg'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cpf'); ?>
		<?php echo $form->textField($model,'cpf',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'cpf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inscricao_municipal'); ?>
		<?php echo $form->textField($model,'inscricao_municipal',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'inscricao_municipal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datanascimento'); ?>
		<?php echo $form->textField($model,'datanascimento'); ?>
		<?php echo $form->error($model,'datanascimento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sexo'); ?>
		<?php echo $form->textField($model,'sexo',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'sexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cartao_numero'); ?>
		<?php echo $form->textField($model,'cartao_numero',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'cartao_numero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cartao_nome'); ?>
		<?php echo $form->textField($model,'cartao_nome',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'cartao_nome'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cartao_gerado'); ?>
		<?php echo $form->textField($model,'cartao_gerado'); ?>
		<?php echo $form->error($model,'cartao_gerado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'codtipoassociado'); ?>
		<?php echo $form->textField($model,'codtipoassociado'); ?>
		<?php echo $form->error($model,'codtipoassociado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'formapgto'); ?>
		<?php echo $form->textField($model,'formapgto'); ?>
		<?php echo $form->error($model,'formapgto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contribuicaoanual'); ?>
		<?php echo $form->textField($model,'contribuicaoanual'); ?>
		<?php echo $form->error($model,'contribuicaoanual'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dataassociacao'); ?>
		<?php echo $form->textField($model,'dataassociacao'); ?>
		<?php echo $form->error($model,'dataassociacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'residente'); ?>
		<?php echo $form->textField($model,'residente',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'residente'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dataformatura'); ?>
		<?php echo $form->textField($model,'dataformatura'); ?>
		<?php echo $form->error($model,'dataformatura'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dddcelular'); ?>
		<?php echo $form->textField($model,'dddcelular',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'dddcelular'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'celular'); ?>
		<?php echo $form->textField($model,'celular',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'celular'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dddcelular2'); ?>
		<?php echo $form->textField($model,'dddcelular2',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'dddcelular2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'celular2'); ?>
		<?php echo $form->textField($model,'celular2',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'celular2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dddcelular3'); ?>
		<?php echo $form->textField($model,'dddcelular3',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'dddcelular3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'celular3'); ?>
		<?php echo $form->textField($model,'celular3',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'celular3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'emailsecretaria'); ?>
		<?php echo $form->textField($model,'emailsecretaria',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'emailsecretaria'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'emailsimers'); ?>
		<?php echo $form->textField($model,'emailsimers',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'emailsimers'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gerenterelac'); ?>
		<?php echo $form->textField($model,'gerenterelac',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'gerenterelac'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datatermino'); ?>
		<?php echo $form->textField($model,'datatermino'); ?>
		<?php echo $form->error($model,'datatermino'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datanovotermino'); ?>
		<?php echo $form->textField($model,'datanovotermino'); ?>
		<?php echo $form->error($model,'datanovotermino'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datajubilamento'); ?>
		<?php echo $form->textField($model,'datajubilamento'); ?>
		<?php echo $form->error($model,'datajubilamento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datareassociacao'); ?>
		<?php echo $form->textField($model,'datareassociacao'); ?>
		<?php echo $form->error($model,'datareassociacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datafimresidente'); ?>
		<?php echo $form->textField($model,'datafimresidente'); ?>
		<?php echo $form->error($model,'datafimresidente'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ghc'); ?>
		<?php echo $form->textField($model,'ghc',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'ghc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unimed'); ?>
		<?php echo $form->textField($model,'unimed',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'unimed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unicred'); ?>
		<?php echo $form->textField($model,'unicred',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'unicred'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'municipario'); ?>
		<?php echo $form->textField($model,'municipario',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'municipario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sindical'); ?>
		<?php echo $form->textField($model,'sindical',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'sindical'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'socio_amerers'); ?>
		<?php echo $form->textField($model,'socio_amerers'); ?>
		<?php echo $form->error($model,'socio_amerers'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ano_residencia'); ?>
		<?php echo $form->textField($model,'ano_residencia',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'ano_residencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'espec_residencia'); ?>
		<?php echo $form->textField($model,'espec_residencia'); ?>
		<?php echo $form->error($model,'espec_residencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'local_residencia'); ?>
		<?php echo $form->textField($model,'local_residencia'); ?>
		<?php echo $form->error($model,'local_residencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'assoc_residencia'); ?>
		<?php echo $form->textField($model,'assoc_residencia'); ?>
		<?php echo $form->error($model,'assoc_residencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dataassoc_amerers'); ?>
		<?php echo $form->textField($model,'dataassoc_amerers'); ?>
		<?php echo $form->error($model,'dataassoc_amerers'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datadesassoc_amerers'); ?>
		<?php echo $form->textField($model,'datadesassoc_amerers'); ?>
		<?php echo $form->error($model,'datadesassoc_amerers'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'motivo'); ?>
		<?php echo $form->textField($model,'motivo',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'motivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'motivo_obs'); ?>
		<?php echo $form->textField($model,'motivo_obs',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'motivo_obs'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'motivo_usuario'); ?>
		<?php echo $form->textField($model,'motivo_usuario',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'motivo_usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ref_hospital'); ?>
		<?php echo $form->textField($model,'ref_hospital'); ?>
		<?php echo $form->error($model,'ref_hospital'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'senha'); ?>
		<?php echo $form->textField($model,'senha',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'senha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'senhaunisimers'); ?>
		<?php echo $form->textField($model,'senhaunisimers',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'senhaunisimers'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'revisado'); ?>
		<?php echo $form->textField($model,'revisado'); ?>
		<?php echo $form->error($model,'revisado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'revisor'); ?>
		<?php echo $form->textField($model,'revisor',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'revisor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_revisao'); ?>
		<?php echo $form->textField($model,'data_revisao'); ?>
		<?php echo $form->error($model,'data_revisao'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_alteracao'); ?>
		<?php echo $form->textField($model,'data_alteracao'); ?>
		<?php echo $form->error($model,'data_alteracao'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario'); ?>
		<?php echo $form->textField($model,'usuario',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'recebe_email'); ?>
		<?php echo $form->textField($model,'recebe_email'); ?>
		<?php echo $form->error($model,'recebe_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'recebe_torpedos'); ?>
		<?php echo $form->textField($model,'recebe_torpedos'); ?>
		<?php echo $form->error($model,'recebe_torpedos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'capitalinicial'); ?>
		<?php echo $form->textField($model,'capitalinicial',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'capitalinicial'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cartao_enviado'); ?>
		<?php echo $form->textField($model,'cartao_enviado'); ?>
		<?php echo $form->error($model,'cartao_enviado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_geracao_cartao'); ?>
		<?php echo $form->textField($model,'data_geracao_cartao'); ?>
		<?php echo $form->error($model,'data_geracao_cartao'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_envio_cartao'); ?>
		<?php echo $form->textField($model,'data_envio_cartao'); ?>
		<?php echo $form->error($model,'data_envio_cartao'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'office2_password'); ?>
		<?php echo $form->textField($model,'office2_password',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'office2_password'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->