<?php

class MovimentacaoBibliotecaItensController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'MovimentacaoBibliotecaItens'),
		));
	}

	public function actionCreate() {
		$model = new MovimentacaoBibliotecaItens;


		if (isset($_POST['MovimentacaoBibliotecaItens'])) {
			$model->setAttributes($_POST['MovimentacaoBibliotecaItens']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'MovimentacaoBibliotecaItens');


		if (isset($_POST['MovimentacaoBibliotecaItens'])) {
			$model->setAttributes($_POST['MovimentacaoBibliotecaItens']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'MovimentacaoBibliotecaItens')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('MovimentacaoBibliotecaItens');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new MovimentacaoBibliotecaItens('search');
		$model->unsetAttributes();

		if (isset($_GET['MovimentacaoBibliotecaItens']))
			$model->setAttributes($_GET['MovimentacaoBibliotecaItens']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}