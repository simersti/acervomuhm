<?php
/* @var $this MedicosController */
/* @var $data Medicos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crm')); ?>:</b>
	<?php echo CHtml::encode($data->crm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome')); ?>:</b>
	<?php echo CHtml::encode($data->nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apelido')); ?>:</b>
	<?php echo CHtml::encode($data->apelido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rg')); ?>:</b>
	<?php echo CHtml::encode($data->rg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cpf')); ?>:</b>
	<?php echo CHtml::encode($data->cpf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inscricao_municipal')); ?>:</b>
	<?php echo CHtml::encode($data->inscricao_municipal); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('datanascimento')); ?>:</b>
	<?php echo CHtml::encode($data->datanascimento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sexo')); ?>:</b>
	<?php echo CHtml::encode($data->sexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cartao_numero')); ?>:</b>
	<?php echo CHtml::encode($data->cartao_numero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cartao_nome')); ?>:</b>
	<?php echo CHtml::encode($data->cartao_nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cartao_gerado')); ?>:</b>
	<?php echo CHtml::encode($data->cartao_gerado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codtipoassociado')); ?>:</b>
	<?php echo CHtml::encode($data->codtipoassociado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('formapgto')); ?>:</b>
	<?php echo CHtml::encode($data->formapgto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contribuicaoanual')); ?>:</b>
	<?php echo CHtml::encode($data->contribuicaoanual); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dataassociacao')); ?>:</b>
	<?php echo CHtml::encode($data->dataassociacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('residente')); ?>:</b>
	<?php echo CHtml::encode($data->residente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dataformatura')); ?>:</b>
	<?php echo CHtml::encode($data->dataformatura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dddcelular')); ?>:</b>
	<?php echo CHtml::encode($data->dddcelular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celular')); ?>:</b>
	<?php echo CHtml::encode($data->celular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dddcelular2')); ?>:</b>
	<?php echo CHtml::encode($data->dddcelular2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celular2')); ?>:</b>
	<?php echo CHtml::encode($data->celular2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dddcelular3')); ?>:</b>
	<?php echo CHtml::encode($data->dddcelular3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celular3')); ?>:</b>
	<?php echo CHtml::encode($data->celular3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailsecretaria')); ?>:</b>
	<?php echo CHtml::encode($data->emailsecretaria); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailsimers')); ?>:</b>
	<?php echo CHtml::encode($data->emailsimers); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gerenterelac')); ?>:</b>
	<?php echo CHtml::encode($data->gerenterelac); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datatermino')); ?>:</b>
	<?php echo CHtml::encode($data->datatermino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datanovotermino')); ?>:</b>
	<?php echo CHtml::encode($data->datanovotermino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datajubilamento')); ?>:</b>
	<?php echo CHtml::encode($data->datajubilamento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datareassociacao')); ?>:</b>
	<?php echo CHtml::encode($data->datareassociacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datafimresidente')); ?>:</b>
	<?php echo CHtml::encode($data->datafimresidente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ghc')); ?>:</b>
	<?php echo CHtml::encode($data->ghc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unimed')); ?>:</b>
	<?php echo CHtml::encode($data->unimed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unicred')); ?>:</b>
	<?php echo CHtml::encode($data->unicred); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('municipario')); ?>:</b>
	<?php echo CHtml::encode($data->municipario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sindical')); ?>:</b>
	<?php echo CHtml::encode($data->sindical); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('socio_amerers')); ?>:</b>
	<?php echo CHtml::encode($data->socio_amerers); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ano_residencia')); ?>:</b>
	<?php echo CHtml::encode($data->ano_residencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('espec_residencia')); ?>:</b>
	<?php echo CHtml::encode($data->espec_residencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('local_residencia')); ?>:</b>
	<?php echo CHtml::encode($data->local_residencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('assoc_residencia')); ?>:</b>
	<?php echo CHtml::encode($data->assoc_residencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dataassoc_amerers')); ?>:</b>
	<?php echo CHtml::encode($data->dataassoc_amerers); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datadesassoc_amerers')); ?>:</b>
	<?php echo CHtml::encode($data->datadesassoc_amerers); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('motivo')); ?>:</b>
	<?php echo CHtml::encode($data->motivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('motivo_obs')); ?>:</b>
	<?php echo CHtml::encode($data->motivo_obs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('motivo_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->motivo_usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ref_hospital')); ?>:</b>
	<?php echo CHtml::encode($data->ref_hospital); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('senha')); ?>:</b>
	<?php echo CHtml::encode($data->senha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('senhaunisimers')); ?>:</b>
	<?php echo CHtml::encode($data->senhaunisimers); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('revisado')); ?>:</b>
	<?php echo CHtml::encode($data->revisado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('revisor')); ?>:</b>
	<?php echo CHtml::encode($data->revisor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_revisao')); ?>:</b>
	<?php echo CHtml::encode($data->data_revisao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_alteracao')); ?>:</b>
	<?php echo CHtml::encode($data->data_alteracao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario')); ?>:</b>
	<?php echo CHtml::encode($data->usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recebe_email')); ?>:</b>
	<?php echo CHtml::encode($data->recebe_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recebe_torpedos')); ?>:</b>
	<?php echo CHtml::encode($data->recebe_torpedos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('capitalinicial')); ?>:</b>
	<?php echo CHtml::encode($data->capitalinicial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cartao_enviado')); ?>:</b>
	<?php echo CHtml::encode($data->cartao_enviado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_geracao_cartao')); ?>:</b>
	<?php echo CHtml::encode($data->data_geracao_cartao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_envio_cartao')); ?>:</b>
	<?php echo CHtml::encode($data->data_envio_cartao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('office2_password')); ?>:</b>
	<?php echo CHtml::encode($data->office2_password); ?>
	<br />

	*/ ?>

</div>