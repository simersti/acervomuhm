<?php

$this->breadcrumbs = array(
	BibliotecaImagem::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . BibliotecaImagem::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . BibliotecaImagem::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(BibliotecaImagem::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 