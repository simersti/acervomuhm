<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);

// $this->menu = array(
// 		array('label'=>'Listar' . ' ' . $model->label(2), 'url'=>array('index')),
// 		array('label'=>'Cadastrar ' . ' ' . $model->label(), 'url'=>array('create')),
// 	);

$this->title_action = GxHtml::encode($model->label(2));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('usuario-grid', {
		data: $(this).serialize()
	});
	return false;
});

");

$this->widget(
    'booster.widgets.TbButtonGroup',
    array(
        'buttons' => array(
            array('buttonType'=>'link' ,'label' => 'Cadastrar novo '.$model->label(), 'url'=>array('create'), 'context'=>'primary'),
        ),
    )
);

?>




<?php $this->widget('booster.widgets.TbExtendedGridView', array(
	'id' => 'usuario-grid',
	'type' => 'striped bordered condensed hover',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array(
			'name'=>'id',
			'value'=>'$data->id',			
			'htmlOptions'=>array('style'=>'width:10%;')
		),
		'nome',
		'login',
		array(			
			'name'=>'grupo_id',
			'value'=>'GxHtml::valueEx($data->idGrupo)',
			'filter'=>GxHtml::listDataEx(Grupo::model()->findAllAttributes(null, true)),			
		),
		array(
			'name'=>'permissao',
			'value'=>'UtilModel::getPermissao($data->permissao)',
			'filter'=>array('1'=>'Administrador','2'=>'Usuário'),
		),
		array(
			'name'=>'status',
			'value'=>'UtilModel::statusAtual($data->status)',
			'filter'=>array('1'=>'Ativo','0'=>'Inativo'),
			'htmlOptions'=>array('style'=>'width:10%;')

		),
		array(
			'header' 		=> Yii::t('ses', 'Ações'),
			'class'			=> 'booster.widgets.TbButtonColumn',
			'template'		=> '{update} | {ativa}{desativa}',
			'buttons'=>array(
                'update'=>array(
                    // 'icon'=>'icon-edit',
                    'label'=>'Editar',
                ),
                'ativa'=>array(
                    'url' => '$data->id',
                    'icon'=>'glyphicon glyphicon-remove-sign',
	                'options'=>array('style'=>'color:red;'),
                    'click' => "function( e ){e.preventDefault();  alterStatus( $( this ).attr( 'href' ) ,'usuario', 'usuario-grid'); }",
                    'visible'=>'$data->status==0?1:0'
                ),
                'desativa'=>array(
                    'url' => '$data->id',
                    'icon'=>'glyphicon glyphicon-ok-sign',
	                'options'=>array('style'=>'color:green;','onclick'=>"if(confirm('Tem certeza que deseja desativar a conta selecionada?')){return true}else{return false}"),
                    'click' => "function( e ){e.preventDefault();  alterStatus( $( this ).attr( 'href' ) ,'usuario', 'usuario-grid' ); }",
                    'visible'=>'$data->status==1?1:0'
                ),
    		),
			'headerHtmlOptions'	=> array('class'=>'col-sm-1 text-center'),
			'htmlOptions'	=> array('style' => 'vertical-align: middle; width:20%;', 'class' => 'text-center'),
		),
	),
)); ?>