<?php

class TipoSerieController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'TipoSerie'),
		));
	}

	public function actionCreate() {
		$model = new TipoSerie;


		if (isset($_POST['TipoSerie'])) {
			$model->setAttributes($_POST['TipoSerie']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'TipoSerie');


		if (isset($_POST['TipoSerie'])) {
			$model->setAttributes($_POST['TipoSerie']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'TipoSerie');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new TipoSerie('search');
		$model->unsetAttributes();

		if (isset($_GET['TipoSerie']))
			$model->setAttributes($_GET['TipoSerie']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}