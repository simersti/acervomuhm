<?php

Yii::import('application.models._base.BaseDesdobramento');

class Desdobramento extends BaseDesdobramento
{
	///$inventario_id = null;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function setInventario($inventario_id) {
		$this->inventario_id = $inventario_id;
	}

	public function getInventario() {
		return $this->inventario_id;
	}


	public function searchPorInventario() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('numero', $this->numero, true);
		$criteria->compare('nome', $this->nome, true);
		$criteria->compare('ativo', $this->ativo, true);
		$criteria->compare('excluido', 0);
		$criteria->compare('inventario_id', $this->inventario_id);
		$criteria->order = 'numero';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}