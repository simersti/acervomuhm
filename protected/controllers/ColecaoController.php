<?php

class ColecaoController extends GxController {

	public function actionCreate() {
		$model = new Colecao;


		if (isset($_POST['Colecao'])) {
			$model->setAttributes($_POST['Colecao']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Colecao');


		if (isset($_POST['Colecao'])) {
			$model->setAttributes($_POST['Colecao']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Colecao');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Colecao('search');
		$model->unsetAttributes();

		if (isset($_GET['Colecao']))
			$model->setAttributes($_GET['Colecao']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}