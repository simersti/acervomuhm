$(document).ready(function(){

/*mascaras para doadores*/
$('#Doador_telefone_fixo').mask('(099)9999-9999');
$('#Doador_telefone_celular').mask('(099)9999-9999');
$('#Doador_cep').mask('99999-999');    

/*mascaras para movimentações*/
$('#MovimentacaoArquivistico_rg_solicitante').mask('9999999999');    
$('#Movimentacao_rg_solicitante').mask('9999999999');    
$('#MovimentacaoBiblioteca_rg_solicitante').mask('9999999999');    


    
});


var somaContinha = 0;

function continha(a,b,op,id)
{
    $.ajax({
        type:"POST",
        url: '?r=lancamentoNota/continha',
        data: {'a':a,'b':b,'op':op},
        async: false,
        success: function(x) {

            $('#'+id).val(x); 
            somaContinha = x;
        }
    });
    
}

function getResultContinha(){
  return somaContinha;
}

function recursiveReplace(value, search, replace){

    if(value.indexOf(search) > 0){
        value = value.replace(search, replace);
        value = recursiveReplace(value, search, replace);
    }
    
    return value;   
}




function alterStatus(id,controller,grid){


    $.ajax({
        type:"POST",
        url: '?r='+controller+'/alterstatus',
        data: {'id':id},
        success: function(x) {

            $.fn.yiiGridView.update(grid, {
                data: $(this).serialize()
            });  
            return false;
       
        }
    });
    return false;
}

function imagemDefault(id,controller,grid){


    $.ajax({
        type:"POST",
        url: '?r='+controller+'/imagemDefault',
        data: {'id':id},
        success: function(x) {

            $.fn.yiiGridView.update(grid, {
                data: $(this).serialize()
            });  
            return false;
       
        }
    });
    return false;
}

function publicarCatalogo(id,controller,grid){


    $.ajax({
        type:"POST",
        url: '?r='+controller+'/publicarCatalogo',
        data: {'id':id},
        success: function(x) {

            $.fn.yiiGridView.update(grid, {
                data: $(this).serialize()
            });  
            return false;
       
        }
    });
    return false;
}

function deleta(id,controller,grid){

if(confirm('Tem certeza que deseja desativar a conta selecionada?')){
    $.ajax({
        type:"POST",
        url: '?r='+controller+'/delete',
        data: {'id':id},
        success: function(x) {



            $.fn.yiiGridView.update(grid, {
                data: $(this).serialize()
            });  

       
        }
    });
}else{
    return false;
}

    return false;
}
    var icon = 1;
    

//menu lateral

/*Menu-toggle*/
$("#menu-toggle").click(function(e) {

    e.preventDefault();

    $("#wrapper").toggleClass("active");

    if(icon == 1){
        $('#menu-toggle').attr('class','navbar-brand glyphicon glyphicon-indent-left');
        icon = 2;

    }else{
        $('#menu-toggle').attr('class','navbar-brand glyphicon glyphicon-indent-right');
        icon = 1;
    }


});
/*Scroll Spy*/
$('body').scrollspy({ target: '#spy', offset:80});

/*Smooth link animation*/
$('a[href*=menu]:not([href=menu])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});

/*mostra as informações que devem ser preenchidas caso seja marcado como inativo(Arquivistico)*/
$('#Arquivistico_ativo').change(function(){
    var valor = this.value;

    if(valor == "0"){
       $('#motivo_inativo').css('display','block');
       $('#observacao_inativo').css('display','block');
    }else{
       $('#motivo_inativo').css('display','none');
       $('#observacao_inativo').css('display','none');
    }    
});
/*mostra as informações que devem ser preenchidas caso seja marcado como inativo(Biblioteca)*/
$('#Biblioteca_ativo').change(function(){
    var valor = this.value;

    if(valor == "0"){
       $('#motivo_inativo').css('display','block');
       $('#observacao_inativo').css('display','block');
    }else{
       $('#motivo_inativo').css('display','none');
       $('#observacao_inativo').css('display','none');
    }    
});

/*mostra as informações que devem ser preenchidas caso seja marcado como inativo(Tridimensional)*/
$('#Inventario_ativo').change(function(){
    var valor = this.value;

    if(valor == "0"){
       $('#motivo_inativo').css('display','block');
       $('#observacao_inativo').css('display','block');
    }else{
       $('#motivo_inativo').css('display','none');
       $('#observacao_inativo').css('display','none');
    }    
});