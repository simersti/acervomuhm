<h1><?php echo 'Relatório ' . GxHtml::encode($model->label()); ?></h1>

<div class="form well">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'arquivistico-form',
	'htmlOptions'=>array('target'=>'_blank'),
	'enableAjaxValidation' => false,
));
?>
		<?php echo $form->errorSummary($model); ?>

		<div class="row">
			
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'doador_id'); ?>
				<?php echo $form->dropDownList($model, 'doador_id', GxHtml::listDataEx(Doador::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'doador_id'); ?>								
			</div>
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'classificacao_arquivistico_id'); ?>
				<?php echo $form->dropDownList($model, 'classificacao_arquivistico_id', GxHtml::listDataEx(ClassificacaoArquivistico::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'classificacao_arquivistico_id'); ?>								
			</div>	
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'modo_aquisicao_id'); ?>
				<?php echo $form->dropDownList($model, 'modo_aquisicao_id', GxHtml::listDataEx(ModoAquisicao::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'modo_aquisicao_id'); ?>								
			</div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-6">			
				<?php echo $form->labelEx($model,'acumulado_por_id'); ?>
				<?php echo $form->dropDownList($model, 'acumulado_por_id', GxHtml::listDataEx(AcumuladoPor::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'acumulado_por_id'); ?>								
			</div>
			<div class="col-md-3">
			  <?php  echo $form->dropDownListGroup($model,'tem_termo_doacao',array(
						'widgetOptions' => array(
							'data' => array('2'=>'Ambos','1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('tem_termo_doacao'),
							),
						),
					)); ?>
			</div>
			<div class="col-md-3">
			  <?php  echo $form->dropDownListGroup($model,'ativo',array(
						'widgetOptions' => array(
							'data' => array('2'=>'Ambos','1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('ativo'),
							),
						),
					)); ?>
			</div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-6">
			<?php $this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'size'=>'large',
					'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
					'label'=> 'Gerar Relatório',
				)); ?>
			</div>
		</div><!-- row -->
<?php

$this->endWidget();
?>
</div><!-- form -->