<?php

class ClassificacaoArquivisticoController extends GxController {


	public function actionCreate() {
		$model = new ClassificacaoArquivistico;


		if (isset($_POST['ClassificacaoArquivistico'])) {
			$model->setAttributes($_POST['ClassificacaoArquivistico']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'ClassificacaoArquivistico');


		if (isset($_POST['ClassificacaoArquivistico'])) {
			$model->setAttributes($_POST['ClassificacaoArquivistico']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'ClassificacaoArquivistico');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new ClassificacaoArquivistico('search');
		$model->unsetAttributes();

		if (isset($_GET['ClassificacaoArquivistico']))
			$model->setAttributes($_GET['ClassificacaoArquivistico']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}