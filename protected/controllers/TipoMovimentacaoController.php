<?php

class TipoMovimentacaoController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'TipoMovimentacao'),
		));
	}

	public function actionCreate() {
		$model = new TipoMovimentacao;


		if (isset($_POST['TipoMovimentacao'])) {
			$model->setAttributes($_POST['TipoMovimentacao']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'TipoMovimentacao');


		if (isset($_POST['TipoMovimentacao'])) {
			$model->setAttributes($_POST['TipoMovimentacao']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'TipoMovimentacao');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new TipoMovimentacao('search');
		$model->unsetAttributes();

		if (isset($_GET['TipoMovimentacao']))
			$model->setAttributes($_GET['TipoMovimentacao']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}