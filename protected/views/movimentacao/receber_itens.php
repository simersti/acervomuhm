<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
	'Update',
);

$this->menu = array(
	array('label' => 'List' . ' ' . $model->label(2), 'url'=>array('index')),
	array('label' => 'Create' . ' ' . $model->label(), 'url'=>array('create')),
	array('label' => 'View' . ' ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
	array('label' => 'Manage' . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo 'Receber Itens da ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->renderPartial('_form', array(
		'model' => $model, 'inventario' => $inventario));

?>
<h4 style="margin-left: 14px;"><?php echo 'Itens da ' . ' ' . GxHtml::encode($model->label()); ?></h4>

<?php
/*variavel de controle, para não mostrar os checks, pois está apenas recebendo os itens*/
$is_recebimento_itens = true;
$this->renderPartial('_box_movimentacao', array(
	'is_recebimento_itens'=> $is_recebimento_itens,'grid' => $grid));
?>