<?php

class MovimentacaoController extends GxController {

	public function actionCreate() {
		$model = new Movimentacao;

		$inventario = new Inventario('search');
		
		if (isset($_POST['Movimentacao'])) {
			$model->setAttributes($_POST['Movimentacao']);

			/*seta o id do usuario para o usuario que está fazendo a transação*/
			$model->usuario_id = Yii::app()->user->id;

			if ($model->save()) {
				
				foreach ($_POST['itens'] as $key => $item) {

					/*Salva os itens no movimentação itens*/
					foreach ($item as $key => $value) {

						$model2 = new MovimentacaoItens;						

						$inventario_id = $key;
						$desdobramento_id = (int) $value['incluir_item'];

						if($inventario_id == 0){
							$model2->inventario_id = $desdobramento_id;
							$model2->desdobramento_id = 0;
						}else{
							$model2->inventario_id = $inventario_id;
							$model2->desdobramento_id = $desdobramento_id;					
						}

						$model2->movimentacao_id = $model->id;
						$model2->ativo= 1;	


						$model2->save();					
					}

				}				 

				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model, 'inventario' => $inventario));
	}

	/*
	*  Método responsavel por receber os itens que estão em movimentação
	*
	*/

	public function actionReceberItens($id) {
		$model = $this->loadModel($id, 'Movimentacao');

		$inventario = new Inventario('search');
		

		if (isset($_POST['Movimentacao'])) {		

			$model->setAttributes($_POST['Movimentacao']);


			$model->observacao = $_POST['Movimentacao']['observacao'];
			$model->ativo = 0;

			if ($model->save()) {

				$itens = MovimentacaoItens::model()->findAllByAttributes(array('movimentacao_id'=>$id));			
			
				foreach ($itens as $key => $item) {
					$item->ativo = 0;				
					$item->update();
				}

				$this->redirect(array('admin'));
				
			}

		}else{
			/*busca todos os itens que compoe está movimentação*/
		    $itens = Yii::app()->db->createCommand("select * from movimentacao_itens 
		    										where movimentacao_id =".$id)->queryAll();

			$grid = array();
			$i = 0;
			foreach ($itens as $chave => $item) {						
				$grid[$i]['id']= $item['desdobramento_id'];
				$grid[$i]['inventario_id']= $item['inventario_id'];

				if($item['desdobramento_id'] ==0 ){
					$inventario_item = Inventario::model()->findByPk($item['inventario_id']);

					$grid[$i]['codigo']=$inventario_item->numero_inventario;
					$grid[$i]['nome']=$inventario_item->nome;

				}else{
					$desdobramento_item = Desdobramento::model()->findByPk($item['desdobramento_id']);

					$grid[$i]['codigo']=$desdobramento_item->numero;
					$grid[$i]['nome']=$desdobramento_item->nome;					
				}
				$i++;											
			}			
		}		

		$this->render('receber_itens', array(
				'model' => $model,
				'inventario' => $inventario,
				'grid' => $grid,				
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Movimentacao');
		$model->excluido = 1;
		$model->ativo = 0;

		if ($model->save()) {

			$itens = MovimentacaoItens::model()->findAllByAttributes(array('movimentacao_id'=>$id));
		
			
			foreach ($itens as $key => $item) {
				$item->ativo = 0;				
				$item->update();
			}
			

			$this->redirect(array('admin'));
		}
	}


	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Movimentacao('search');
		$model->unsetAttributes();

		if (isset($_GET['Movimentacao']))
			$model->setAttributes($_GET['Movimentacao']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionCreateItens() {
		$model = new MovimentacaoItens;		
		
		if(isset($_POST['Inventario'])){			

			$grid = array();
			$i = 0;
			foreach ($_POST['Inventario']['id'] as $key => $value) {
					$itens = Yii::app()->db->createCommand("select id, numero_inventario, nome, '0' as inventario_id from 
															inventario
															where id =:inventario_id and ativo = 1 and excluido = 0
															UNION
															select id, numero, nome, inventario_id from desdobramento where 
															inventario_id =:inventario_id and ativo = 1 and excluido = 0")
											->bindValue(":inventario_id",(int)$value)											
											->queryAll();

					foreach ($itens as $chave => $item) {

						if($item['inventario_id'] == 0){
							$inventario = $item['id'];
							$desdobramento = $item['inventario_id'];
						}else{
							$inventario = $item['inventario_id'];
							$desdobramento = $item['id'];
						}

						$itens = MovimentacaoItens::model()->findAllByAttributes(array('desdobramento_id'=>$desdobramento,
																					   'inventario_id'=>$inventario,
																					   'ativo'=>1));						

						if(!$itens){

							$grid[$i]['id']= $item['id'];
							$grid[$i]['inventario_id']= $item['inventario_id'];
							$grid[$i]['codigo']=$item['numero_inventario'];
							$grid[$i]['nome']=$item['nome'];

							$i++;
						}									
					}					
			}			
		}else{
			$grid = array();
		}

		/*variavel de controle, para mostrar os checks, pois deve criar os itens na movimentação*/
		$is_recebimento_itens = false;
		$html =  $this->renderPartial(
			'_box_movimentacao', 			
			array(				
				'is_recebimento_itens'=> $is_recebimento_itens,				
				'grid' => $grid,
			),true
		);

		$return["html"] = $html;

		echo json_encode($return);
		die();		
	}	
}