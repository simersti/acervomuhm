<?php

class GrupoController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Grupo'),
		));
	}

	public function actionCreate() {
		$model = new Grupo;


		if (isset($_POST['Grupo'])) {
			$model->setAttributes($_POST['Grupo']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Grupo');


		if (isset($_POST['Grupo'])) {
			$model->setAttributes($_POST['Grupo']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Grupo');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Grupo('search');
		$model->unsetAttributes();

		if (isset($_GET['Grupo']))
			$model->setAttributes($_GET['Grupo']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}