<?php
	Yii::app()->clientScript->registerScript('search', "
			$('.detail-button').click(function(){
				$('.details').toggle();
				return false;
			});
	");
?>
<h2><?php echo 'Imagens Vinculadas ao ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h2>

<div class="alert alert-warning" role="alert">
	<strong>Avisos!&nbsp;</strong><br />
	&rarr;&nbsp;Os anexos devem ser de no máximo <strong>1MB</strong>.<br />
	&rarr;&nbsp;Podem ser anexado no máximo <strong><?php echo Yii::app()->params['maxfileupload'];?> arquivos simultâneos.</strong>.<br />
	&rarr;&nbsp;Arquivos permitidos: <strong><?php echo str_replace('?', '', Yii::app()->params['fileUploadConfig']['PHP']['type']);?></strong>
</div>

<?php 

	$this->widget('booster.widgets.TbFileUpload',
					array(
						'model' 	=> $model,
						'attribute' => 'inventarioImagem',
						'url'		=> $this->createUrl(Yii::app()->controller->getId().'/anexarImagens', array('id' => $model->id)),
						'formView'	=> 'application.views.inventario._formAnexo',
						'multiple' 	=> true,
						'options'	=> array(
							'maxFileSize'		=> Yii::app()->params['fileUploadConfig']['JS']['size'],
							'acceptFileTypes' 	=> 'js:/(\.|\/)('.Yii::app()->params['fileUploadConfig']['JS']['type'].')$/i',
							'maxNumberOfFiles' 	=> Yii::app()->params['maxfileupload'],
							'imageForceResize'	=> true,
						),
					)
	);
	
	$this->renderPartial('_listaAnexos', compact('model'));
	
?>