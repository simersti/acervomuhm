<?php

class ConservacaoController extends GxController {


	public function actionCreate() {
		$model = new Conservacao;


		if (isset($_POST['Conservacao'])) {
			$model->setAttributes($_POST['Conservacao']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Conservacao');


		if (isset($_POST['Conservacao'])) {
			$model->setAttributes($_POST['Conservacao']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Conservacao');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Conservacao('search');
		$model->unsetAttributes();

		if (isset($_GET['Conservacao']))
			$model->setAttributes($_GET['Conservacao']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}