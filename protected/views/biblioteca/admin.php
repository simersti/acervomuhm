<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);

$this->title_action = GxHtml::encode($model->label(2));

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('biblioteca-grid', {
		data: $(this).serialize()
	});
	return false;
});

");

$this->widget(
    'booster.widgets.TbButtonGroup',
    array(
        'buttons' => array(
            array(
                'buttonType'=> 'link',
                'label'     => 'Cadastrar Novo Item na '.$model->label(),
                'url'       => array('create'),
                'context'   => 'primary'
            ),
        ),
    )
);

?>


<?php $this->widget('booster.widgets.TbExtendedGridView', array(
	'id' => 'biblioteca-grid',
	'type' => 'striped bordered condensed hover',
	'dataProvider' => $model->search(),
	'filter' => $model,
    'pager' => array(
        'class' => 'booster.widgets.TbPager', // **use extended CLinkPager class**
        'cssFile' => false, //prevent Yii autoloading css
        'header' => false, // hide 'go to page' header
        'firstPageLabel' => '&lt;&lt;', // change pager button labels
        'prevPageLabel' => '&lt;',
        'nextPageLabel' => '&gt;',
        'lastPageLabel' => '&gt;&gt;',
        'displayFirstAndLast' => true,
    ),
	'columns' => array(
		'numero',
		'titulo',
		array(
				'name'=>'tipologia_id',
				'value'=>'GxHtml::valueEx($data->tipologia)',				
				'filter'=>GxHtml::listDataEx(Tipologia::model()->findAllAttributes(null, true)),
		),
		'autor',
		///'ano_publicacao',	
		'edicao',
		'tomo_volume',
		array(
				'name'=>'editora_id',
				'value'=>'GxHtml::valueEx($data->editora)',				
				'filter'=>GxHtml::listDataEx(Editora::model()->findAllAttributes(null, true)),
		),		
		array(
			'name'=>'ativo',
			'value'=>'UtilModel::statusAtual($data->ativo)',
			'filter'=>array('1'=>'Ativo','0'=>'Inativo'),
			'htmlOptions'=>array('style'=>'width:10%;')

		),		
		///'excluido',
		array(
			'header' 		=> Yii::t('ses', 'Ações'),
			'class'			=> 'booster.widgets.TbButtonColumn',
			'template'		=> '{update} | {view} | {imagens_sim}{imagens_nao} | {publicar}{despublicar} | {delete}|{clonar}',
			'buttons'=>array(
                'update'=>array(
                    // 'icon'=>'icon-edit',
                    'label'=>'Editar',
                ),
                'view'=>array(
                    // 'icon'=>'icon-edit',
                    'label'=>'Visualizar',
                ),
                'imagens_sim'=>array(
                    'url'       => 'Yii::app()->createUrl("biblioteca/imagens/", array("id"=>$data->id))',
                    'icon'      => 'glyphicon glyphicon-picture',
                    'options'   => array('class'=>'view','style'=>'color:green;'),
                    'label'     => 'Contém Imagens Cadastradas',
                    'visible'   => '$data->bibliotecaImagem!=null?1:0'
                ),
                'imagens_nao'=>array(
                    'url'       => 'Yii::app()->createUrl("biblioteca/imagens/", array("id"=>$data->id))',
                    'icon'      => 'glyphicon glyphicon-picture',
                    'options'   => array('class'=>'view','style'=>'color:red;'),
                    'label'     => 'Não Contém Imagens Cadastradas',
                    'visible'   => '$data->bibliotecaImagem==null?1:0'
                ),                 
                'publicar'=>array(
                    'url'       => '$data->id',
                    'icon'      => 'glyphicon glyphicon-globe',
	                'options'   => array('style'=>'color:red;'),
                    'click'     => "function( e ){e.preventDefault();  publicarCatalogo( $( this ).attr( 'href' ) ,'biblioteca', 'biblioteca-grid'); }",
                    'label'     => 'Não Publicado no Site',
                    'visible'   => '$data->catalogo==0?1:0'
                ),
                'despublicar'=>array(
                    'url'       => '$data->id',
                    'icon'      => 'glyphicon glyphicon-globe',
	                'options'   => array('style'=>'color:green;'),
                    'click'     => "function( e ){e.preventDefault();  publicarCatalogo( $( this ).attr( 'href' ) ,'biblioteca', 'biblioteca-grid' ); }",
                    'label'     => 'Publicado no Site',
                    'visible'   => '$data->catalogo==1?1:0'
                ),                   
                'delete'=>array(
                    // 'icon'=>'delete',
                    'label'=>'delete',
                ),
                'clonar'=>array(
                    'url'=> 'Yii::app()->createUrl("biblioteca/clonar", array("id"=>$data->id))',
                    'icon'=>'glyphicon glyphicon-copy',
                    'options'=>array('class'=>'view'),
                    'label'=>'Clonar',
                ),                    

    		),
			'headerHtmlOptions'	=> array(
                'class'=>'col-sm-1 text-center'
            ),
			'htmlOptions'	=> array(
                'style' => 'vertical-align: middle; width:20%;',
                'class' => 'text-center'
            ),
		),
	),
)); ?>