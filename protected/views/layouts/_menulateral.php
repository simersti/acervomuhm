<!-- <div class="panel-group" id="accordion"> -->
<?php 
	foreach ($this->menuLateral() as $key => $menu_lateral_) {

		if(in_array(Yii::app()->user->permissao, $menu_lateral_['permissao'])){

?>

	<div class="panel panel-default panel-menu" style="margin-bottom:1px;margin-top:1px;">

		<?php if(!empty($menu_lateral_['link'])){ ?>
	  		<a href="<?php echo $menu_lateral_['link']; ?>" style="text-decoration: none">
	  	<?php }else{ ?>
	  		<a data-toggle="collapse" data-parent="#accordion"  href="#collapse<?php echo $key; ?>" style="text-decoration: none">
	  	<?php } ?>

			    <div class="panel-heading panel-heading-menu">
			      <h4 class="panel-title">
			        <?php if(!empty($menu_lateral_['icon'])){ ?>	
			          	<span class="glyphicon glyphicon-<?php echo $menu_lateral_['icon']; ?>"></span>
			        <?php } ?>

			        <?php if(!empty($menu_lateral_['label'])) echo $menu_lateral_['label']; ?>
			        
			      </h4>
			    </div> 
			</a>

		<?php 
			if (count($menu_lateral_['items'])) {
		?>
		   <div id="collapse<?php echo $key; ?>" class="panel-collapse collapse ">
		     	<div class="panel-body" style="padding:0px;">
			     	<?php 
			     	foreach ($menu_lateral_['items'] as $key => $value) {
			       	///if(in_array(Yii::app()->user->permissao, $value['permissao'])){
			     	?>

						<ul class="nav nav-list" id="yw<?php echo $key; ?>">
							<li>
								<a target="<?php echo $value['target']; ?>" href="<?php echo $value['url']; ?>">
									<span class="glyphicon glyphicon-<?php echo $value['icon']; ?>"></span>
									<?php echo $value['label']; ?>
								</a>
							</li>
						</ul>

						<?php /// } ?>
					<?php } ?>
		     	</div>
		   </div>

		<?php } ?>
	</div>

	<?php } ?>
<?php } ?>
<!-- </div> -->