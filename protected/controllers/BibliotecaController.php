<?php

class BibliotecaController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Biblioteca'),
		));
	}

	public function actionCreate() {
		$model = new Biblioteca;

	

		if (isset($_POST['Biblioteca'])) {
			$model->setAttributes($_POST['Biblioteca']);

			/*cria o um novo codigo apartir do numero anterior do acervo sequencial*/
			$acervo = new Acervo;
			$codigo = $acervo->geraCodigo();
			$model->numero = $codigo;

			$acervo->codigo = $codigo;
			$acervo->descricao = $model->titulo;
			$acervo->tipo_acervo_id = 2; /*código na tabela tipo_acervo*/
			$acervo->ativo = $model->ativo;
			$acervo->excluido = $model->excluido;
 		
 			if($acervo->save()){	
 				/*var_dump($acervo);*/
 				$model->acervo_id = $acervo->id;
				if ($model->save()) {
					if (Yii::app()->getRequest()->getIsAjaxRequest())
						Yii::app()->end();
					else
						$this->redirect(array('admin'));
				}else{
					$acervo->delete();
				}
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Biblioteca');


		if (isset($_POST['Biblioteca'])) {
			$model->setAttributes($_POST['Biblioteca']);
			
			/*echo "<pre>";
			print_r($_POST['Biblioteca']);
			echo "</pre>";*/

			if ($model->save()) {		
				/*atualiza a descrição do acervo*/		
				$acervo = $this->loadModel($model->acervo_id, 'Acervo');
				$acervo->descricao = $model->titulo;
				$acervo->save(false);
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Biblioteca');
		$model->excluido ='1';

		if($model->update(array('excluido'))){
			$this->redirect(array('admin'));
		}
	}		

	public function actionClonar($id) {
		$model_original = $this->loadModel($id, 'Biblioteca');

		/*cria o um novo codigo apartir do numero anterior do acervo sequencial*/
		$acervo = new Acervo;
		$codigo = $acervo->geraCodigo();
		$model_original->numero = $codigo;

		$acervo->codigo = $codigo;
		$acervo->descricao = $model_original->titulo;
		$acervo->tipo_acervo_id = 2; /*código na tabela tipo_acervo*/
		$acervo->ativo = $model_original->ativo;
		$acervo->excluido = $model_original->excluido;

		if($acervo->save()){				
				$model_original->id 		  = null;

				$model = new Biblioteca;
				$model->setAttributes($model_original->attributes);
				$model->acervo_id = $acervo->id;

			if ($model->save(false)) {				
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect('index.php?r=biblioteca/update&id='.$model->id);
			}else{				
				$acervo->delete();
			}
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Biblioteca('search');
		$model->unsetAttributes();

		if (isset($_GET['Biblioteca']))
			$model->setAttributes($_GET['Biblioteca']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionImagens($id){	

		$model = $this->loadModel($id, 'Biblioteca');		

		$anexos	= new BibliotecaImagem();				
		$model->bibliotecaImagem= $anexos->getAnexos($id);	

		
		$this->render('_anexos', array(
			'model' => $model,
		));

	}

	public function actionAnexarImagens($id){	

		// verifica se é uma requisão ajax
		if (Yii::app()->getRequest()->getIsAjaxRequest()){
			
			// verifica se o formulário foi submetido
			if(isset($_POST['Biblioteca'])){

				// instancia a model responsável pela validação dos dados do arquivo
				$biblioteca = new Biblioteca();
				
				// defini o cenário para relização das validações
				$biblioteca->setScenario('upload');

				// captura o arquivos submetido
				$biblioteca->bibliotecaImagem = CUploadedFile::getInstance($biblioteca, 'bibliotecaImagem');
								
				// valida os dados
				if(!empty($biblioteca->bibliotecaImagem) && $biblioteca->validate(array('bibliotecaImagem'))){
					
					// inicializa variavel que ira receber os erros encontrados no momento de salvar os arquivos
					$error = null;
					
					// inicializa variavel que ira a mensagem de sucesso quando encontrado
					$success = null;
					
					// instancia model responsável por armazenar o anexo
					$anexo = new BibliotecaImagem();
					
					// armazena o arquivo e captura os resultados
					$result = $anexo->saveUpload($biblioteca->bibliotecaImagem, $id);
					
					// verifica o tipo de resultado gerado
					if(isset($result[BibliotecaImagem::FILE_ERROR])){
						echo json_encode($result[BibliotecaImagem::FILE_ERROR]);
					}elseif(isset($result[BibliotecaImagem::FILE_SUCCESS])){
						echo json_encode($result[BibliotecaImagem::FILE_SUCCESS]);
					}
				}else{
					$error 	= null;
					$error 	= str_replace('?', '', $biblioteca->getError('bibliotecaImagem')); // para garantir que a mensagem de erro ira ser apresentada corretamente nos casos de tipo inválido
					$name	= $biblioteca->bibliotecaImagem->getName();
 					$data	= array(compact('error', 'name'));
					echo json_encode($data);					
				}
			}
		}
	}
	
	public function actionExcluirImagem($id){
		
		// verifica se é uma requisão ajax
		if (Yii::app()->getRequest()->getIsAjaxRequest()){
			if (Yii::app()->getRequest()->getIsPostRequest()) {
				
				// captura os dados do arquivo
				$model = $this->loadModel($id, 'BibliotecaImagem');

				// verifica se o arquivo a ser excuido existe no servidor
				if(file_exists($model->directory.$model->file)){
					// exclui o arquivo do servidor
					unlink($model->directory.$model->file);
					
					// exclui do banco de dados o registro
					$model->delete();
					
				}
				
			}else{
				throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
			}
		}else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}	

	public function actionImagemDefault() {

		if (isset($_POST['id'])) {
			$model = $this->loadModel($_POST['id'], 'BibliotecaImagem');
			$model->principal=1;


			if ($model->update()) {
				/*Se colocou a imagem selecionada como principal, coloca as demais como não principal*/
				$id = $model->id;
				$biblioteca_id = $model->biblioteca_id;

				$criteria = new CDbCriteria;
				$criteria->addCondition('t.id !='.$id);
				$criteria->addCondition('t.biblioteca_id ='.$biblioteca_id);
				$demais_itens = BibliotecaImagem::Model()->findAll($criteria);

				foreach ($demais_itens as $key => $value) {
					$model2 = $this->loadModel($value->id, 'BibliotecaImagem');
					$model2->principal=0;
					$model2->update();
				}

				die;

					die('ok');	
			}else{
				die('error');
			}
		}	
	}

	public function actionPublicarCatalogo() {

		if (isset($_POST['id'])) {
			$model = $this->loadModel($_POST['id'], 'Biblioteca');

			if($model->catalogo == 1){
				$model->catalogo=0;
			}else{
				$model->catalogo=1;
			}

			if ($model->update()) {
					die('ok');	
			}else{
				die('error');
			}
		}	
	}	


	public function actionRelatorios(){
		
		if($_POST){


			$complementoSql = "";

			if($_POST['Biblioteca']['ativo'] == 2){
				$ativo = '0,1';
			}else{
				$ativo = $_POST['Biblioteca']['ativo'];
			}

			if($_POST['Biblioteca']['autor'] != ""){
				$complementoSql .= " AND autor = '".$_POST['Biblioteca']['autor']."'";
			}

			if($_POST['Biblioteca']['titulo'] != ""){
				$complementoSql .= " AND titulo = '".$_POST['Biblioteca']['titulo']."'";
			}

			

			if($_POST['Biblioteca']['doador_id'] != ""){
				$complementoSql .= " AND doador_id =".$_POST['Biblioteca']['doador_id'];
				/*$doador = Yii::app()->db->createCommand('SELECT id FROM doador where ativo=1 and excluido=0')->queryAll();

				$doadores = array();
				foreach ($doador as $key => $value) {
					array_push($doadores, $value['id']);
				}

				$doador_id = implode(",", $doadores);
			}else{
				$doador_id = $_POST['Biblioteca']['doador_id'];*/
			}

			if($_POST['Biblioteca']['modo_aquisicao_id'] != ""){
				$complementoSql .= "AND modo_aquisicao_id =".$_POST['Biblioteca']['modo_aquisicao_id'];
				/*$modo_aquisicao = Yii::app()->db->createCommand('SELECT id FROM modo_aquisicao where ativo=1 and excluido=0')->queryAll();

				$modo_aquisicoes = array();
				foreach ($modo_aquisicao as $key => $value) {
					array_push($modo_aquisicoes, $value['id']);
				}

				$modo_aquisicao_id = implode(",", $modo_aquisicoes);
			}else{
				$modo_aquisicao_id = $_POST['Biblioteca']['modo_aquisicao_id'];*/
			}

			if($_POST['Biblioteca']['acumulado_por_id'] != ""){
				$complementoSql .= " AND acumulado_por_id =".$_POST['Biblioteca']['acumulado_por_id'];
				/*$acumulado_por = Yii::app()->db->createCommand('SELECT id FROM acumulado_por where ativo=1 and excluido=0')->queryAll();

				$acumulados = array();
				foreach ($acumulado_por as $key => $value) {
					array_push($acumulados, $value['id']);
				}

				$acumulado_por_id = implode(",", $acumulados);
			}else{
				$acumulado_por_id = $_POST['Biblioteca']['acumulado_por_id'];*/
			}

			if($_POST['Biblioteca']['tipologia_id'] != ""){
				$complementoSql .= " AND tipologia_id =".$_POST['Biblioteca']['tipologia_id'];
				/*$tipologia = Yii::app()->db->createCommand('SELECT id FROM tipologia where ativo=1 and excluido=0')->queryAll();

				$tipologias = array();
				foreach ($tipologia as $key => $value) {
					array_push($tipologias, $value['id']);
				}

				$tipologia_id = implode(",", $tipologias);
			}else{
				$tipologia_id = $_POST['Biblioteca']['tipologia_id'];*/
			}

			if($_POST['Biblioteca']['editora_id'] != ""){
				$complementoSql .=" AND editora_id =".$_POST['Biblioteca']['editora_id'];
				/*$editora = Yii::app()->db->createCommand('SELECT id FROM editora where ativo=1 and excluido=0')->queryAll();

				$editoras = array();
				foreach ($editora as $key => $value) {
					array_push($editoras, $value['id']);
				}

				$editora_id = implode(",", $editoras);
			}else{
				$editora_id = $_POST['Biblioteca']['editora_id'];*/
			}

			if($_POST['Biblioteca']['especialidade_medica_id'] != ""){
				$complementoSql .=" AND especialidade_medica_id =".$_POST['Biblioteca']['especialidade_medica_id'];
				/*$especialidade_medica = Yii::app()->db->createCommand('SELECT id FROM especialidade_medica where ativo=1 and excluido=0')->queryAll();

				$especialidade_medicas = array();
				foreach ($especialidade_medica as $key => $value) {
					array_push($especialidade_medicas, $value['id']);
				}

				$especialidade_medica_id = implode(",", $especialidade_medicas);
			}else{
				$especialidade_medica_id = $_POST['Biblioteca']['especialidade_medica_id'];*/
			}

			$biblioteca = Yii::app()->db->createCommand('SELECT *
														FROM biblioteca 
														WHERE 
														excluido = 0 
														'.$complementoSql)->queryAll();

			
			$this->geraPdf($biblioteca);
		}

		$model = new Biblioteca('search');
		$model->unsetAttributes();

		if (isset($_GET['Biblioteca']))
			$model->setAttributes($_GET['Biblioteca']);
//	var_dump($this->listarEditora());die;

		$this->render('_relatorio', array(
				'model' => $model,
				'autor' => $this->listarAutor(),
				'editora'=>$this->listarEditora(),
				'titulo' => $this->listarTitulo(),
				));		
	}		


	public function geraPdf($biblioteca){

			//die('tipoRelatorio');
		    $arquivo = 'biblioteca_'.date('d').'_'.date('m').'_'.date('Y').'.pdf';		  

		    $header = "";

		    $titulo = "RELATÓRIO DO BIBLIOGRÁFICO";

		    if($_POST['tipoRelatorio'][0] == 'S'){
				
				$pdf=Yii::app()->pdfFactory->getTCPDF();
				$pdf->SetTitle($titulo);
				$pdf->Ln(30);
				$pdf->setPrintHeader(false);
				$pdf->setFooterData($tc=array(0,0,0), $lc=array(0,0,0));
				$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));		
				$pdf->SetAutoPageBreak(TRUE, 10);
				$pdf->SetMargins(5,5,5);
				$pdf->SetFont('', '', 8);
				$pdf->AddPage();

		    	$pdf->Write(20,$header);
		    	$pdf->Image('images/logo_horizontal.png',10,8, 90);
		    	$pdf->Ln(20);

		    	$pdf->SetFont('','ub',12);
		    	$pdf->SetX(80);
		    	$pdf->Write(5,$titulo);	    
		    	$pdf->Ln(20);

		    	// header tabela
				//$this->linhaTabela(true, null, $pdf, '200,200,200','Número','Especialidade','Título','Autor','Edição','Volume','Editora','Ano de Publicação');
				$pdf->SetFont('','',12);
				$bgColor = "255,255,255";
				$html.= '<table style="border: 1px solid black;">
							<tr>
								<th style=" border: 1px solid black;" align="center"><b>Número</b></th>
								<th style=" border: 1px solid black;" align="center"><b>Espec.</b></th>
								<th style=" border: 1px solid black;" align="center"><b>Título</b></th>
								<th style=" border: 1px solid black;" align="center"><b>Autor</b></th>
								<th style=" border: 1px solid black;" align="center"><b>Edição</b></th>
								<th style=" border: 1px solid black;" align="center"><b>Volume</b></th>
								<th style=" border: 1px solid black;" align="center"><b>Editora</b></th>
								<th style=" border: 1px solid black;" align="center"><b>Publicação</b></th>
							</tr>';
				foreach ($biblioteca as $value) {
					if ( $bgColor == "255,255,255" )
						$bgColor = "235,235,235";
					else
						$bgColor = "255,255,255";
					$html .='<tr style="background-color: rgb('.$bgColor.'); ">
								<td style=" border: 1px solid black;">'.$value['numero'].'</td>
								<td style=" border: 1px solid black;" align="center">'.$value['especialidade_medica_id'].'</td>
								<td style=" border: 1px solid black;">'.$value['titulo'].'</td>
								<td style=" border: 1px solid black;">'.$value['autor'].'</td>
								<td style=" border: 1px solid black;" align="center">'.$value['edicao'].'</td>
								<td style=" border: 1px solid black;" align="center">'.$value['tomo_volume'].'</td>
								<td style=" border: 1px solid black;" align="center">'.$value['editora_id'].'</td>
								<td style=" border: 1px solid black;" align="center">'.$value['ano_publicacao'].'</td>
							</tr>';
					//$this->linhaTabela (false,$value,$pdf, $bgColor, '', $value['numero'], $value['especialidade_medica_id'], $value['titulo'], $value['autor'],$value['edicao'], $value['tomo_volume'], $value['editora_id'], $value['ano_publicacao']);
				}
				
				$html .='</table>';
				$pdf->writeHTML($html, true, false, false, false, '');
				$pdf->SetY(272);
	      		$pdf->SetX(10);
	      		$pdf->SetFont('','b',9);
	      		$pdf->Cell(0,10,"Rua Cel. Corte Real, 975 Porto Alegre-RS",0,0,'L');
	      		$pdf->Ln(5);
	      		$pdf->SetX(10);
	      		$pdf->Cell(10,10,"CEP: 90630-080 / Fone (51) 3027.3737 / www.simers.org.br",0,0,'L');

			}else if($_POST['tipoRelatorio'][0] == 'D'){
				//echo "tipo de relatorio é = D<br>";


				$pdf=Yii::app()->pdfFactory->getTCPDF();
				$pdf->SetTitle($titulo);
				$pdf->Ln(30);
				$pdf->setPrintHeader(false);
				$pdf->setFooterData($tc=array(0,0,0), $lc=array(0,0,0));
				$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));		
				$pdf->SetAutoPageBreak(TRUE, 10);
				$pdf->SetMargins(25,25,25);

				$pdf->SetFont('', '', 10);
				$pdf->AddPage();

		    	$pdf->Write(20,$header);
		    	$pdf->Image('images/logo_horizontal.png',10,8, 90);
		    	$pdf->Ln(20);

		    	$pdf->SetFont('','ub',12);
		    	$pdf->SetX(80);
		    	$pdf->Write(5,$titulo);	    
		    	$pdf->Ln(20);

				// header tabela
				$this->linhaTabela(true, null, $pdf, '200,200,200','Imagem', 'Número','Título', 'Conservação','Ativo');
				$pdf->SetFont('','',12);

			    $bgColor = "255,255,255";
			    foreach ($biblioteca as $value) {

					if ( $bgColor == "255,255,255" )
						$bgColor = "235,235,235";
					else
						$bgColor = "255,255,255";

					switch ($value['ativo']) {
					  case "0": $value['ativo'] = "Não"; break;
					  case "1": $value['ativo'] = "Sim"; break;
					}

					if($value['conservacao_id'] != 0){
						$conservacao = $this->loadModel($value['conservacao_id'], 'Conservacao');
						$conservacao = $conservacao->nome;
			    	}else{
						$conservacao = "Não especificado";
			    	}

			    	$this->linhaTabela (false,$value,$pdf, $bgColor, '', $value['numero'], 
			    										 $value['titulo'], $conservacao, $value['ativo']);
			    }
			  
	      		$pdf->SetY(272);
	      		$pdf->SetX(10);      	
	      		$pdf->SetFont('','b',9);      	
	      		$pdf->Cell(0,10,"Rua Cel. Corte Real, 975 Porto Alegre-RS",0,0,'L');
	      		$pdf->Ln(5);
	      		$pdf->SetX(10);
	      		$pdf->Cell(10,10,"CEP: 90630-080 / Fone (51) 3027.3737 / www.simers.org.br",0,0,'L');
			}
			$pdf->Output($arquivo, 'I');
		

		

	}	

	function linhaTabela ( $header=false, $item, $pdf = null, $bgColor = null, $coluna1, $coluna2, $coluna3, $coluna4, $coluna5 ){

		if ( is_null( $bgColor ) )
		{
			$pdf->setFillColor( 255, 255, 255);

		} else {

			$bgColor = explode (",",$bgColor);
			$pdf->SetFillColor( $bgColor[0], $bgColor[1], $bgColor[2]);
		}

		$pdf->SetX( 20 );
		$pdf->SetFont('','',8);

		
		if(!is_null($item)){
			$biblioteca_imagem= Yii::app()->db->createCommand('SELECT *															
															FROM biblioteca_imagem 
															WHERE															
															biblioteca_id = '.$item['id'].'
															AND principal = 1')->queryAll();

			if(!empty($biblioteca_imagem)){

				foreach ($biblioteca_imagem as $key => $imagem) {
					$coluna1 = 'images/biblioteca/'.$imagem['file'];
				}				
				
			}else{
				$coluna1 = 'images/imagem_indisponivel.jpg';
			}

			$pdf->Cell( 24, 20, $pdf->Image($coluna1, $pdf->GetX(), $pdf->GetY(), 23.78), 1, 0, 'C', 0 );
			$pdf->Cell( 20, 20, $coluna2, 1, 0, "C", 1 ); // codigo
			$pdf->Cell( 60, 20, $coluna3, 1, 0, "C", 1 ); // descricao		
			$pdf->Cell( 53, 20, $coluna4, 1, 0, "C", 1 ); // 'condicoes_fisicas
			$pdf->Cell( 15, 20, $coluna5, 1, 1, "C", 1 ); // ativo			
		}else{
			$pdf->Cell( 24, 5, $coluna1, 1, 0, "C", 1 ); // imagem	
			$pdf->Cell( 20, 5, $coluna2, 1, 0, "C", 1 ); // codigo
			$pdf->Cell( 60, 5, $coluna3, 1, 0, "C", 1 ); // descricao		
			$pdf->Cell( 53, 5, $coluna4, 1, 0, "C", 1 ); // 'condicoes_fisicas
			$pdf->Cell( 15, 5, $coluna5, 1, 1, "C", 1 ); // ativo
		}
		

		if(!$header){

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Autor:'.$item['autor'], 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Ano de Publicação:'.$item['ano_publicacao'], 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Edição:'.$item['edicao'], 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Tomo/Volume:'.$item['tomo_volume'], 1, 1, "L", 1 ); // ativo	

			$editora = $this->loadModel($item['editora_id'], 'Editora');

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Editora:'.$editora->descricao, 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Número de Páginas:'.$item['numero_paginas'], 1, 1, "L", 1 ); // ativo	


			$tipologia = $this->loadModel($item['tipologia_id'], 'Tipologia');

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Tipologia:'.$tipologia->descricao, 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Cidade:'.$item['cidade'], 1, 1, "L", 1 ); // ativo	

			$especialidade_medica = $this->loadModel($item['especialidade_medica_id'], 'EspecialidadeMedica');

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Especialidade Médica:'.$especialidade_medica->descricao, 1, 1, "L", 1 ); // ativo	

			$acumulado_por = $this->loadModel($item['acumulado_por_id'], 'AcumuladoPor');

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Acumulado Por:'.$acumulado_por->descricao, 1, 1, "L", 1 ); // ativo	


			$doador = $this->loadModel($item['doador_id'], 'Doador');

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Doador:'.$doador->nome, 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			if($item['data_doacao'] !="0000-00-00"){				
				$pdf->Cell( 172, 5,'Data da Doação:'.DateTimeComponent::getBrazilianData($item['data_doacao']), 1, 1, "L", 1 ); // ativo	
			}else{
				$pdf->Cell( 172, 5,'Data da Doação: -', 1, 1, "L", 1 ); // ativo	
			}

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Localização:'.$item['localizacao'] , 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );	
			$pdf->MultiCell(172, 20, 'Observações:'.strip_tags($item['observacoes']) , 1, 1 ,'J',1 );
			$pdf->Ln(4);

		}

		return $pdf;	
	}
	public function listarAutor(){
		$autor = Yii::app()->db->createCommand('SELECT 
												autor 
												FROM 
												biblioteca 
												WHERE 
												autor <> ""
												ORDER BY autor ASC')->queryAll();
		return $autor;
	}

	public function listarEditora(){
		$editora = Yii::app()->db->createCommand("SELECT
													*
													FROM
													editora
													WHERE
													descricao != ''
													")->queryAll();
		return $editora;
	}

	public function listarTitulo(){
		$tituloBiblioteca = Yii::app()->db->createCommand("SELECT 
															titulo
															FROM 
															biblioteca
															WHERE
															titulo <> ''
															ORDER BY titulo ASC")->queryAll();
		return $tituloBiblioteca;
	}

	public function actionMovimentacaoAtiva(){


		$movimentacao= Yii::app()->db->createCommand('SELECT * 
													FROM movimentacao_biblioteca 
													WHERE ativo = 1 and excluido =0')->queryAll();


		$this->geraPdfMovimentacao($movimentacao, 1);
	}

	public function actionMovimentacaoInativa(){


		$movimentacao= Yii::app()->db->createCommand('SELECT * 
													FROM movimentacao_biblioteca 
													WHERE ativo = 0 and excluido =0')->queryAll();


		$this->geraPdfMovimentacao($movimentacao, 0);
	}


	public function geraPdfMovimentacao($movimentacao, $ativo){

	    $arquivo = 'biblioteca_'.date('d').'_'.date('m').'_'.date('Y').'.pdf';		  

	    $header = "";

	    if($ativo == 1){
	    	$titulo = "RELATÓRIO DE MOVIMENTAÇÕES ATIVAS DA BIBLIOTECA";	
	    }else{
	    	$titulo = "RELATÓRIO DE MOVIMENTAÇÕES INATIVAS DA BIBLIOTECA";
	    }	    

		$pdf=Yii::app()->pdfFactory->getTCPDF();		
		$pdf->SetTitle($titulo);
		$pdf->Ln(30);
		$pdf->setPrintHeader(false);
		$pdf->setFooterData($tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));		
		$pdf->SetAutoPageBreak(TRUE, 10);
		$pdf->SetMargins(25,25,25);

		$pdf->SetFont('', '', 10);
		$pdf->AddPage();

	    $pdf->Write(20,$header);
	    $pdf->Image('images/logo_horizontal.png',10,8, 90);
	    $pdf->Ln(20);

	    $pdf->SetFont('','ub',12);
	    $pdf->SetX(40);
	    $pdf->Write(5,$titulo);	    
	    $pdf->Ln(20);

		$pdf->SetX( 20 );
		$pdf->SetFont('','',8);	    
		$pdf->setFillColor( 200, 200, 200);
		$pdf->Cell( 15, 5, 'Código', 1, 0, 'C', 1 );
		$pdf->Cell( 53, 5, 'Instituição', 1, 0, "C", 1 ); // codigo
		$pdf->Cell( 60, 5, 'Motivo', 1, 0, "C", 1 ); // descricao		
		$pdf->Cell( 20, 5, 'Saída', 1, 0, "C", 1 ); // 'condicoes_fisicas
		$pdf->Cell( 24, 5, 'Movimentação', 1, 1, "C", 1 ); // ativo		

		foreach ($movimentacao as $key => $value) {

			$pdf->setFillColor( 255, 255, 255);
			
			$pdf->SetX( 20 );
			$pdf->Cell( 15, 5, $value['codigo'], 1, 0, 'C', 1 );
			$pdf->Cell( 53, 5, $value['instituicao'], 1, 0, "C", 1 ); 
			$pdf->Cell( 60, 5, $value['motivo'], 1, 0, "C", 1 ); 
			$pdf->Cell( 20, 5, DateTimeComponent::getBrazilianData($value['data_saida']) , 1, 0, "C", 1 ); 

			$tipo_movimentacao = $this->loadModel($value['tipo_movimentacao_id'], 'TipoMovimentacao');
			$pdf->Cell( 24, 5, $tipo_movimentacao->nome, 1, 1, "C", 1 ); 

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Período:'.DateTimeComponent::getBrazilianData($value['periodo_inicio']).' à '.DateTimeComponent::getBrazilianData($value['periodo_fim']), 1, 1, "L", 1 ); // ativo				

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Responsável:'.$value['responsavel'], 1, 1, "L", 1 ); // ativo				

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Solicitante:'.$value['solicitante'], 1, 1, "L", 1 ); // ativo								

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Localização:'.$value['localizacao'], 1, 1, "L", 1 ); // ativo				

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'ITENS', 1, 1, "C", 1 ); // ativo	

				$movimentacao_itens= Yii::app()->db->createCommand('SELECT * 
																	FROM movimentacao_biblioteca_itens 
																	WHERE 
																	movimentacao_biblioteca_id ='. $value['id'] .'
																	AND ativo ='.$ativo)->queryAll();				
				if(!empty($movimentacao_itens)){
					foreach ($movimentacao_itens as $k => $item) {
						$livro = $this->loadModel($item['biblioteca_id'], 'Biblioteca');
						
						$pdf->SetX( 20 );				
						$pdf->Cell( 172, 5,$livro['numero'].' - '.$livro['titulo'], 1, 1, "L", 1 ); // ativo						
					}
				}else{
					$pdf->SetX( 20 );				
					$pdf->Cell( 172, 5,'Sem itens cadastrados para esta movimentação.', 1, 1, "L", 1 ); // ativo											
				}

			$pdf->Ln(4);		
		}




      	$pdf->SetY(272);
      	$pdf->SetX(10);      	
      	$pdf->SetFont('','b',9);      	
      	$pdf->Cell(0,10,"Rua Cel. Corte Real, 975 Porto Alegre-RS",0,0,'L');
      	$pdf->Ln(5);
      	$pdf->SetX(10);
      	$pdf->Cell(10,10,"CEP: 90630-080 / Fone (51) 3027.3737 / www.simers.org.br",0,0,'L');
		
		$pdf->Output($arquivo, 'I');
	}	
}