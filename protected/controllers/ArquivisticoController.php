<?php

class ArquivisticoController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Arquivistico'),
		));
	}

	public function actionCreate() {
		$model = new Arquivistico;

		if (isset($_POST['Arquivistico'])) {
			$model->setAttributes($_POST['Arquivistico']);

			/*cria o um novo codigo apartir do numero anterior do acervo sequencial*/
			$acervo = new Acervo;
			$codigo = $acervo->geraCodigo();
			$model->codigo = $codigo;

			$acervo->codigo = $codigo;
			$acervo->descricao = $model->descricao;
			$acervo->tipo_acervo_id = 1; /*código na tabela tipo_acervo*/
			$acervo->ativo = $model->ativo;
			$acervo->excluido = $model->excluido;
 
 			if($acervo->save()){
 				$model->acervo_id = $acervo->id;					
				if ($model->save()) {
					if (Yii::app()->getRequest()->getIsAjaxRequest())
						Yii::app()->end();
					else
						$this->redirect(array('admin'));
				}else{
					$acervo->delete();
				}
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Arquivistico');


		if (isset($_POST['Arquivistico'])) {
			$model->setAttributes($_POST['Arquivistico']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {

		$model = $this->loadModel($id, 'Arquivistico');
		$model->excluido = '1';

		if($model->update(array('excluido'))){
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Arquivistico('search');
		$model->unsetAttributes();

		if (isset($_GET['Arquivistico']))
			$model->setAttributes($_GET['Arquivistico']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionImagens($id){	

		$model = $this->loadModel($id, 'Arquivistico');		

		$anexos	= new ArquivisticoImagem();				
		$model->arquivisticoImagem	= $anexos->getAnexos($id);	

		
		$this->render('_anexos', array(
			'model' => $model,
		));

	}

	public function actionAnexarImagens($id){	

		// verifica se é uma requisão ajax
		if (Yii::app()->getRequest()->getIsAjaxRequest()){
			
			// verifica se o formulário foi submetido
			if(isset($_POST['Arquivistico'])){

				// instancia a model responsável pela validação dos dados do arquivo
				$arquivistico = new Arquivistico();
				
				// defini o cenário para relização das validações
				$arquivistico->setScenario('upload');

				// captura o arquivos submetido
				$arquivistico->arquivisticoImagem = CUploadedFile::getInstance($arquivistico, 'arquivisticoImagem');

				// valida os dados
				if(!empty($arquivistico->arquivisticoImagem) && $arquivistico->validate(array('arquivisticoImagem'))){

					
					// inicializa variavel que ira receber os erros encontrados no momento de salvar os arquivos
					$error = null;
					
					// inicializa variavel que ira a mensagem de sucesso quando encontrado
					$success = null;
					
					// instancia model responsável por armazenar o anexo
					$anexo = new ArquivisticoImagem();
					
					// armazena o arquivo e captura os resultados
					$result = $anexo->saveUpload($arquivistico->arquivisticoImagem, $id);
										
					// verifica o tipo de resultado gerado
					if(isset($result[ArquivisticoImagem::FILE_ERROR])){
						echo json_encode($result[ArquivisticoImagem::FILE_ERROR]);
					}elseif(isset($result[ArquivisticoImagem::FILE_SUCCESS])){
						echo json_encode($result[ArquivisticoImagem::FILE_SUCCESS]);
					}
				}else{
					$error 	= null;
					$error 	= str_replace('?', '', $arquivistico->getError('arquivisticoImagem')); // para garantir que a mensagem de erro ira ser apresentada corretamente nos casos de tipo inválido
					$name	= $arquivistico->arquivisticoImagem->getName();
 					$data	= array(compact('error', 'name'));
					echo json_encode($data);					
				}
			}
		}
	}
	
	public function actionExcluirImagem($id){
		
		// verifica se é uma requisão ajax
		if (Yii::app()->getRequest()->getIsAjaxRequest()){
			if (Yii::app()->getRequest()->getIsPostRequest()) {
				
				// captura os dados do arquivo
				$model = $this->loadModel($id, 'ArquivisticoImagem');

				// verifica se o arquivo a ser excuido existe no servidor
				if(file_exists($model->directory.$model->file)){
					// exclui o arquivo do servidor
					unlink($model->directory.$model->file);
					
					// exclui do banco de dados o registro
					$model->delete();
					
				}
				
			}else{
				throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
			}
		}else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}	

	public function actionImagemDefault() {

		if (isset($_POST['id'])) {
			$model = $this->loadModel($_POST['id'], 'ArquivisticoImagem');
			$model->principal=1;


			if ($model->update()) {
				/*Se colocou a imagem selecionada como principal, coloca as demais como não principal*/
				$id = $model->id;
				$arquivistico_id = $model->arquivistico_id;

				$criteria = new CDbCriteria;
				$criteria->addCondition('t.id !='.$id);
				$criteria->addCondition('t.arquivistico_id ='.$arquivistico_id);
				$demais_itens = ArquivisticoImagem::Model()->findAll($criteria);

				foreach ($demais_itens as $key => $value) {
					$model2 = $this->loadModel($value->id, 'ArquivisticoImagem');
					$model2->principal=0;
					$model2->update();
				}

				die;

					die('ok');	
			}else{
				die('error');
			}
		}	
	}	

	public function actionPublicarCatalogo() {

		if (isset($_POST['id'])) {
			$model = $this->loadModel($_POST['id'], 'Arquivistico');

			if($model->catalogo == 1){
				$model->catalogo=0;
			}else{
				$model->catalogo=1;
			}

			if ($model->update()) {
					die('ok');	
			}else{
				die('error');
			}
		}	
	}	

	public function actionRelatorios(){

		if($_POST){			
			
			if($_POST['Arquivistico']['ativo'] == 2){
				$ativo = '0,1';
			}else{
				$ativo = $_POST['Arquivistico']['ativo'];
			}

			if($_POST['Arquivistico']['tem_termo_doacao'] == 2){
				$tem_termo_doacao = '0,1';
			}else{
				$tem_termo_doacao = $_POST['Arquivistico']['tem_termo_doacao'];
			}

			if($_POST['Arquivistico']['doador_id'] == ''){
				$doador = Yii::app()->db->createCommand('SELECT id FROM doador where ativo=1 and excluido=0')->queryAll();

				$doadores = array();
				foreach ($doador as $key => $value) {
					array_push($doadores, $value['id']);
				}

				$doador_id = implode(",", $doadores);
			}else{
				$doador_id = $_POST['Arquivistico']['doador_id'];
			}


			if($_POST['Arquivistico']['classificacao_arquivistico_id'] == ''){
				$classificacao_arquivistico = Yii::app()->db->createCommand('SELECT id FROM classificacao_arquivistico where ativo=1 and excluido=0')->queryAll();

				$classificacoes = array();
				foreach ($classificacao_arquivistico as $key => $value) {
					array_push($classificacoes, $value['id']);
				}

				$classificacao_arquivistico_id = implode(",", $classificacoes);
			}else{
				$classificacao_arquivistico_id = $_POST['Arquivistico']['classificacao_arquivistico_id'];
			}


			if($_POST['Arquivistico']['modo_aquisicao_id'] == ''){
				$modo_aquisicao = Yii::app()->db->createCommand('SELECT id FROM modo_aquisicao where ativo=1 and excluido=0')->queryAll();

				$modo_aquisicoes = array();
				foreach ($modo_aquisicao as $key => $value) {
					array_push($modo_aquisicoes, $value['id']);
				}

				$modo_aquisicao_id = implode(",", $modo_aquisicoes);
			}else{
				$modo_aquisicao_id = $_POST['Arquivistico']['modo_aquisicao_id'];
			}

			if($_POST['Arquivistico']['acumulado_por_id'] == ''){
				$acumulado_por = Yii::app()->db->createCommand('SELECT id FROM acumulado_por where ativo=1 and excluido=0')->queryAll();

				$acumulados = array();
				foreach ($acumulado_por as $key => $value) {
					array_push($acumulados, $value['id']);
				}

				$acumulado_por_id = implode(",", $acumulados);
			}else{
				$acumulado_por_id = $_POST['Arquivistico']['acumulado_por_id'];
			}
		
			$arquivistico= Yii::app()->db->createCommand('SELECT *															
													FROM arquivistico 
													WHERE 
													excluido = 0 AND 
													doador_id in ('.$doador_id.') AND
													classificacao_arquivistico_id in ('.$classificacao_arquivistico_id.') AND
													modo_aquisicao_id in ('.$modo_aquisicao_id.') AND
													acumulado_por_id in ('.$acumulado_por_id.') AND
													tem_termo_doacao in ('.$tem_termo_doacao.') AND 
													ativo in ('.$ativo.')')->queryAll();
			

			$this->geraPdf($arquivistico);
		}

		$model = new Arquivistico('search');
		$model->unsetAttributes();

		if (isset($_GET['Arquivistico']))
			$model->setAttributes($_GET['Arquivistico']);


		$this->render('_relatorio', array(
				'model' => $model,
				));		
	}	


	public function geraPdf($arquivistico){

	    $arquivo = 'arquivistico_'.date('d').'_'.date('m').'_'.date('Y').'.pdf';		  

	    $header = "";

	    $titulo = "RELATÓRIO DO ARQUIVISTICO";

		$pdf=Yii::app()->pdfFactory->getTCPDF();
		$pdf->SetTitle($titulo);
		$pdf->Ln(30);
		$pdf->setPrintHeader(false);
		$pdf->setFooterData($tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));		
		$pdf->SetAutoPageBreak(TRUE, 10);
		$pdf->SetMargins(25,25,25);

		$pdf->SetFont('', '', 10);
		$pdf->AddPage();

	    $pdf->Write(20,$header);
	    $pdf->Image('images/logo_horizontal.png',10,8, 50);
	    $pdf->Ln(20);

	    $pdf->SetFont('','ub',12);
	    $pdf->SetX(80);
	    $pdf->Write(5,$titulo);	    
	    $pdf->Ln(20);

		// header tabela
		$this->linhaTabela(true, null, $pdf, '200,200,200','Imagem', 'Código','Descrição', 'Condições Fisicas','Ativo');
		$pdf->SetFont('','',12);

		    $bgColor = "255,255,255";
		    foreach ($arquivistico as $value) {

				if ( $bgColor == "255,255,255" )
					$bgColor = "235,235,235";
				else
					$bgColor = "255,255,255";

				switch ($value['ativo']) {
				  case "0": $value['ativo'] = "Não"; break;
				  case "1": $value['ativo'] = "Sim"; break;
				}



		    	$this->linhaTabela (false,$value,$pdf, $bgColor, $value['codigo'], $value['codigo'], 
		    										 $value['descricao'], $value['condicoes_fisicas'], $value['ativo']);
		    }
		  
      	$pdf->SetY(272);
      	$pdf->SetX(10);      	
      	$pdf->SetFont('','b',9);      	
      	$pdf->Cell(0,10,"Av. Bento Gonçalves, 2318  - Bairro Partenon - Porto Alegre-RS",0,0,'L');
      	$pdf->Ln(5);
      	$pdf->SetX(10);
      	$pdf->Cell(10,10,"CEP: 90650-001 / Fone (51) 3330-2963 / www.muhm.org.br",0,0,'L');
		
		$pdf->Output($arquivo, 'I');

	}	


	function linhaTabela ( $header=false, $item, $pdf = null, $bgColor = null, $coluna1, $coluna2, $coluna3, $coluna4, $coluna5 ){

		if ( is_null( $bgColor ) )
		{
			$pdf->setFillColor( 255, 255, 255);

		} else {

			$bgColor = explode (",",$bgColor);
			$pdf->SetFillColor( $bgColor[0], $bgColor[1], $bgColor[2]);
		}

		$pdf->SetX( 20 );
		$pdf->SetFont('','',8);

		
		if(!is_null($item)){
			$arquivistico_imagem= Yii::app()->db->createCommand('SELECT *															
															FROM arquivistico_imagem 
															WHERE															
															arquivistico_id = '.$item['id'].'
															AND principal = 1')->queryAll();

			if(!empty($arquivistico_imagem)){

				foreach ($arquivistico_imagem as $key => $imagem) {
					$coluna1 = 'images/arquivistico/'.$imagem['file'];
				}				
				
			}else{
				$coluna1 = 'images/imagem_indisponivel.jpg';
			}

			$pdf->Cell( 24, 20, $pdf->Image($coluna1, $pdf->GetX(), $pdf->GetY(), 23.78), 1, 0, 'C', 0 );
			$pdf->Cell( 20, 20, $coluna2, 1, 0, "C", 1 ); // codigo
			$pdf->Cell( 60, 20, $coluna3, 1, 0, "C", 1 ); // descricao		
			$pdf->Cell( 53, 20, $coluna4, 1, 0, "C", 1 ); // 'condicoes_fisicas
			$pdf->Cell( 15, 20, $coluna5, 1, 1, "C", 1 ); // ativo			
		}else{
			$pdf->Cell( 24, 5, $coluna1, 1, 0, "C", 1 ); // imagem	
			$pdf->Cell( 20, 5, $coluna2, 1, 0, "C", 1 ); // codigo
			$pdf->Cell( 60, 5, $coluna3, 1, 0, "C", 1 ); // descricao		
			$pdf->Cell( 53, 5, $coluna4, 1, 0, "C", 1 ); // 'condicoes_fisicas
			$pdf->Cell( 15, 5, $coluna5, 1, 1, "C", 1 ); // ativo
		}
		

		if(!$header){

			$doador = $this->loadModel($item['doador_id'], 'Doador');

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Doador:'.$doador->nome, 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Localização:'.$item['localizacao'] , 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Condições de Reprodutividade:'.$item['condicoes_de_reprodutividade'] , 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Instrumentos de Pesquisa:'.$item['instrumentos_de_pesquisa'] , 1, 1, "L", 1 ); // ativo	

					if($item['data_limite_inicio'] == 0){
						$item['data_limite_inicio'] = '-';
					}

					if($item['data_limite_fim'] == 0){
						$item['data_limite_fim'] = '-';
					}

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Data Limite:'.$item['data_limite_inicio'].' à '.$item['data_limite_fim'], 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );			
			$pdf->MultiCell(172, 20, 'Histórico:'.strip_tags($item['historico']) , 1, 1 ,'J',1 );			

			$pdf->SetX( 20 );			
			$pdf->MultiCell(172, 20, 'Caracterização Súmaria:'.strip_tags($item['caracterizacao_sumaria']) , 1, 1 ,'J',1 );

					$arquivistico_item= Yii::app()->db->createCommand('SELECT *															
															FROM arquivistico_item 
															WHERE
															excluido = 0 AND
															arquivistico_id = '.$item['id'].'
															AND ativo = 1')->queryAll();

					$itens = 'ITENS';					
					
					$pdf->SetX( 20 );				
					$pdf->MultiCell(172, 5, $itens , 1,1,'L',1 );

					foreach ($arquivistico_item as $chave => $compoe) {

						$tipo_serie = $this->loadModel($compoe['tipo_serie_id'], 'TipoSerie');
						$itens = 'Código:'.$compoe['codigo'].' - '.$compoe['descricao'] .' Tipo Série:'.$tipo_serie->descricao;

						$pdf->SetX( 20 );				
						$pdf->MultiCell(172, 5, $itens , 1,1,'L',1 );							
					}

					if(empty($arquivistico_item)){
						$pdf->SetX( 20 );				
						$pdf->MultiCell(172, 5, 'Sem itens cadastrados.' , 1,1,'L',1 );						
					}

			$pdf->SetX( 20 );	
			$pdf->MultiCell(172, 20, 'Observações:'.strip_tags($item['observacoes']) , 1, 1 ,'J',1 );
			$pdf->Ln(4);

		}

		return $pdf;	
	}	

	public function actionMovimentacaoAtiva(){

		$movimentacao= Yii::app()->db->createCommand('SELECT * 
													FROM movimentacao_arquivistico 
													WHERE ativo = 1 and excluido =0')->queryAll();

		$this->geraPdfMovimentacao($movimentacao, 1);
	}


	public function actionMovimentacaoInativa(){

		$movimentacao= Yii::app()->db->createCommand('SELECT * 
													FROM movimentacao_arquivistico 
													WHERE ativo = 0 and excluido =0')->queryAll();

		$this->geraPdfMovimentacao($movimentacao, 0);
	}


	public function geraPdfMovimentacao($movimentacao, $ativo){

	    $arquivo = 'arquivistico_'.date('d').'_'.date('m').'_'.date('Y').'.pdf';		  

	    $header = "";

	    if($ativo == 1){
	    	$titulo = "RELATÓRIO DE MOVIMENTAÇÕES ATIVAS DO ARQUIVÍSTICO";
	    }else{
	    	$titulo = "RELATÓRIO DE MOVIMENTAÇÕES INATIVAS DO ARQUIVÍSTICO";
	    }	
	    

		$pdf=Yii::app()->pdfFactory->getTCPDF();		
		$pdf->SetTitle($titulo);
		$pdf->Ln(30);
		$pdf->setPrintHeader(false);
		$pdf->setFooterData($tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));		
		$pdf->SetAutoPageBreak(TRUE, 10);
		$pdf->SetMargins(25,25,25);

		$pdf->SetFont('', '', 10);
		$pdf->AddPage();

	    $pdf->Write(20,$header);
	    $pdf->Image('images/logo_horizontal.png',10,8, 90);
	    $pdf->Ln(20);

	    $pdf->SetFont('','ub',12);
	    $pdf->SetX(40);
	    $pdf->Write(5,$titulo);	    
	    $pdf->Ln(20);

		$pdf->SetX( 20 );
		$pdf->SetFont('','',8);	    
		$pdf->setFillColor( 200, 200, 200);
		$pdf->Cell( 15, 5, 'Código', 1, 0, 'C', 1 );
		$pdf->Cell( 53, 5, 'Instituição', 1, 0, "C", 1 ); // codigo
		$pdf->Cell( 60, 5, 'Motivo', 1, 0, "C", 1 ); // descricao		
		$pdf->Cell( 20, 5, 'Saída', 1, 0, "C", 1 ); // 'condicoes_fisicas
		$pdf->Cell( 24, 5, 'Movimentação', 1, 1, "C", 1 ); // ativo		

		foreach ($movimentacao as $key => $value) {

			$pdf->setFillColor( 255, 255, 255);
			
			$pdf->SetX( 20 );
			$pdf->Cell( 15, 5, $value['codigo'], 1, 0, 'C', 1 );
			$pdf->Cell( 53, 5, $value['instituicao'], 1, 0, "C", 1 ); 
			$pdf->Cell( 60, 5, $value['motivo'], 1, 0, "C", 1 ); 
			$pdf->Cell( 20, 5, DateTimeComponent::getBrazilianData($value['data_saida']) , 1, 0, "C", 1 ); 

			$tipo_movimentacao = $this->loadModel($value['tipo_movimentacao_id'], 'TipoMovimentacao');
			$pdf->Cell( 24, 5, $tipo_movimentacao->nome, 1, 1, "C", 1 ); 

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Período:'.DateTimeComponent::getBrazilianData($value['periodo_inicio']).' à '.DateTimeComponent::getBrazilianData($value['periodo_fim']), 1, 1, "L", 1 ); // ativo				

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Responsável:'.$value['responsavel'], 1, 1, "L", 1 ); // ativo				

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Solicitante:'.$value['solicitante'], 1, 1, "L", 1 ); // ativo								

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Localização:'.$value['localizacao'], 1, 1, "L", 1 ); // ativo				

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'ITENS', 1, 1, "C", 1 ); // ativo	

				$movimentacao_itens= Yii::app()->db->createCommand('SELECT * 
																	FROM movimentacao_arquivistico_itens 
																	WHERE 
																	movimentacao_arquivistico_id ='. $value['id'] .'
																	AND ativo ='.$ativo)->queryAll();				
				if(!empty($movimentacao_itens)){
					foreach ($movimentacao_itens as $k => $item) {

							$item = $this->loadModel($item['arquivistico_item_id'], 'ArquivisticoItem');
							$pdf->SetX( 20 );				
							$pdf->MultiCell( 172, 5,$item['codigo'].' - '.$item['descricao'], 1, 1, "L", 1 ); // ativo

					}
				}else{
					$pdf->SetX( 20 );				
					$pdf->Cell( 172, 5,'Sem itens cadastrados para esta movimentação.', 1, 1, "L", 1 ); // ativo											
				}

			$pdf->Ln(4);		
		}




      	$pdf->SetY(272);
      	$pdf->SetX(10);      	
      	$pdf->SetFont('','b',9);      	
      	$pdf->Cell(0,10,"Av. Bento Gonçalves, 2318  - Bairro Partenon - Porto Alegre-RS",0,0,'L');
      	$pdf->Ln(5);
      	$pdf->SetX(10);
      	$pdf->Cell(10,10,"CEP: 90650-001 / Fone (51) 3330-2963 / www.muhm.org.br",0,0,'L');
		
		$pdf->Output($arquivo, 'I');
	}	

}