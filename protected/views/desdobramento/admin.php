<?php
$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Manage',
);

$this->title_action = GxHtml::encode($model->label(2).' '.$model2->numero_inventario.' - '.$model2->nome);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('desdobramento-grid', {
		data: $(this).serialize()
	});
	return false;
});

");

$this->widget(
    'booster.widgets.TbButtonGroup',
    array(
        'buttons' => array(
            array('buttonType'=>'link' ,'label' => 'Adicionar Novo '.$model->label(), 'url'=>array('create&id='.$model2->id), 'context'=>'primary'),
        ),
    )
);

?>


<?php $this->widget('booster.widgets.TbExtendedGridView', array(
	'id' => 'desdobramento-grid',
	'type' => 'striped bordered condensed hover',
	'dataProvider' => $model->searchPorInventario($model2->id),
	'filter' => $model,
	'columns' => array(				
		'numero',
		'nome',		
		array(
			'name'=>'ativo',
			'value'=>'UtilModel::statusAtual($data->ativo)',
			'filter'=>array('1'=>'Ativo','0'=>'Inativo'),
			'htmlOptions'=>array('style'=>'width:10%;')

		),		
		array(
			'header' 		=> Yii::t('ses', 'Ações'),
			'class'			=> 'booster.widgets.TbButtonColumn',
			'template'		=> '{update} | {delete}',
			'buttons'=>array(
                'update'=>array(                    
                    'label'=>'Editar',
                ),
                'delete'=>array(                    
                    'label'=>'delete',
                ),

    		),
			'headerHtmlOptions'	=> array('class'=>'col-sm-1 text-center'),
			'htmlOptions'	=> array('style' => 'vertical-align: middle; width: 20%;', 'class' => 'text-center'),
		),
	),
)); ?>

