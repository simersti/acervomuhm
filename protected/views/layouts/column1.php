<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
	<?php echo $content; ?>
</div><!-- content -->

<div id="footer" style="position: static; width: 100%; bottom: 0px;">
    <div id="footer-content">
            © SIMERS - Sindicato Médico do Rio Grande do Sul - 2015  - Todos os direitos reservados.
    </div>
    
    <div id="footer-user-display-name">
        <!-- Olá, <? //Yii::app()->user->nome?>! -->
    </div>
</div>

<?php $this->endContent(); ?>