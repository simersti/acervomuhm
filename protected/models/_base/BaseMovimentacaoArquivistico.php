<?php

/**
 * This is the model base class for the table "movimentacao_arquivistico".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "MovimentacaoArquivistico".
 *
 * Columns in table "movimentacao_arquivistico" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property integer $tipo_movimentacao_id
 * @property string $codigo
 * @property string $instituicao
 * @property string $motivo
 * @property string $data_saida
 * @property string $periodo_inicio
 * @property string $periodo_fim
 * @property string $data_retorno
 * @property string $localizacao
 * @property string $observacao
 * @property string $responsavel
 * @property string $solicitante
 * @property integer $rg_solicitante
 * @property integer $usuario_id
 * @property string $ativo
 * @property string $excluido
 *
 */
abstract class BaseMovimentacaoArquivistico extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'movimentacao_arquivistico';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Movimentação do Arquivístico|Movimentações dos Arquivísticos', $n);
	}

	public static function representingColumn() {
		return 'codigo';
	}

	public function rules() {
		return array(
			array('tipo_movimentacao_id, codigo, instituicao, motivo, localizacao, observacao, responsavel, solicitante, rg_solicitante, usuario_id, data_saida, periodo_inicio, periodo_fim', 'required', 'on'=>'insert'),
			array('tipo_movimentacao_id, rg_solicitante, usuario_id', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>50),
			array('instituicao, responsavel, solicitante', 'length', 'max'=>100),
			array('motivo, localizacao', 'length', 'max'=>255),
			array('ativo, excluido', 'length', 'max'=>1),
			array('data_saida, periodo_inicio, periodo_fim, data_retorno', 'safe'),			
			array('data_saida, periodo_inicio, periodo_fim, data_retorno, ativo, excluido', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, tipo_movimentacao_id, codigo, instituicao, motivo, data_saida, periodo_inicio, periodo_fim, data_retorno, localizacao, observacao, responsavel, solicitante, rg_solicitante, usuario_id, ativo, excluido', 'safe', 'on'=>'search'),			
		);
	}

	public function relations() {
		return array(
			'tipoMovimentacao' => array(self::BELONGS_TO, 'TipoMovimentacao', 'tipo_movimentacao_id'),
		);
	}

	public function pivotModels() {
		return array(			
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'tipo_movimentacao_id' => Yii::t('app', 'Tipo Movimentação'),
			'codigo' => Yii::t('app', 'Código'),
			'instituicao' => Yii::t('app', 'Instituição'),
			'motivo' => Yii::t('app', 'Motivo'),
			'data_saida' => Yii::t('app', 'Data Saída'),
			'periodo_inicio' => Yii::t('app', 'Periodo Inicio'),
			'periodo_fim' => Yii::t('app', 'Periodo Fim'),
			'data_retorno' => Yii::t('app', 'Data Retorno'),
			'localizacao' => Yii::t('app', 'Localização'),
			'observacao' => Yii::t('app', 'Observação'),
			'responsavel' => Yii::t('app', 'Responsável'),
			'solicitante' => Yii::t('app', 'Solicitante'),
			'rg_solicitante' => Yii::t('app', 'Rg Solicitante'),
			'usuario_id' => Yii::t('app', 'Usuário'),
			'ativo' => Yii::t('app', 'Ativo'),
			'excluido' => Yii::t('app', 'Excluido'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('tipo_movimentacao_id', $this->tipo_movimentacao_id);
		$criteria->compare('codigo', $this->codigo, true);
		$criteria->compare('instituicao', $this->instituicao, true);
		$criteria->compare('motivo', $this->motivo, true);
		$criteria->compare('data_saida', $this->data_saida, true);
		$criteria->compare('periodo_inicio', $this->periodo_inicio, true);
		$criteria->compare('periodo_fim', $this->periodo_fim, true);
		$criteria->compare('data_retorno', $this->data_retorno, true);
		$criteria->compare('localizacao', $this->localizacao, true);
		$criteria->compare('observacao', $this->observacao, true);
		$criteria->compare('responsavel', $this->responsavel, true);
		$criteria->compare('solicitante', $this->solicitante, true);
		$criteria->compare('rg_solicitante', $this->rg_solicitante);
		$criteria->compare('usuario_id', $this->usuario_id);
		$criteria->compare('ativo', $this->ativo, true);
		$criteria->compare('excluido', 0);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}


	public function beforeSave()
	{
		$this->data_saida = DateTimeComponent::convertToTimestamp($this->data_saida. ' 00:00:00');
		$this->periodo_inicio = DateTimeComponent::convertToTimestamp($this->periodo_inicio. ' 00:00:00');
		$this->periodo_fim = DateTimeComponent::convertToTimestamp($this->periodo_fim. ' 00:00:00');	

		if($this->data_retorno == ''){
			$this->data_retorno = '0000-00-00';
		}
		
		if($this->data_retorno != '0000-00-00'){
			$this->data_retorno = DateTimeComponent::convertToTimestamp($this->data_retorno. ' 00:00:00');			
		}
	
		return parent::beforeSave();
	}

	public function afterFind(){
		$this->data_saida	= DateTimeComponent::getBrazilianData($this->data_saida);
		$this->periodo_inicio	= DateTimeComponent::getBrazilianData($this->periodo_inicio);
		$this->periodo_fim	= DateTimeComponent::getBrazilianData($this->periodo_fim);

		if($this->data_retorno != '0000-00-00'){
			$this->data_retorno	= DateTimeComponent::getBrazilianData($this->data_retorno);
		}else{
			$this->data_retorno	= '';
		}
	}	

}