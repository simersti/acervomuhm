<?php

/**
 * Classe responsável por manipular todos os campos de data e hora vindos dos formulários e banco de dados 
 * @author Rodrigo Conceição de Araujo
 *
 */
class DateTimeComponent extends CComponent{

	/**
	 * Constante que define o padrão de data e hora brasileiro
	 * @var String
	 */
	const BRAZILIAN_FORMAT 	= 'd/m/Y H:i:s';
	
	/**
	 * Método responsável por converter uma data no formato brasileiro para o formato timestamp
	 * @access public
	 * @author Rodrigo Conceição de Araujo
	 * 
	 * @param String $datetime - data e hora no formato brasileiro
	 * 
	 * @return Strign com a data e hora no formato brasileiro
	 */
	static public function convertToTimestamp($datetime){
		
		// captura a data e hora 
		list($date, $time) = explode(' ', $datetime);
		
		// ajusta a data para o formato do timestamp
		$date  = self::convertToDateSql($date);
		
		// retorna a data e hora  no formato timestamp
		return implode(' ', compact('date', 'time'));
	}

	/**
	 * Método responsável por converter uma data no formato brasileiro para o formato do banco de dados
	 * @access public
	 * @author Rodrigo Conceição de Araujo
	 * 
	 * @param String $date - data e hora no formato brasileiro
	 * 
	 * @return Strign com a data no formato sql
	 */
	static public function convertToDateSql($date){
		
		// ajusta a data para o formato do banco de dados
		return implode('-', array_reverse(explode('/', $date)));
	}

	/**
	 * Método responsável por converter uma data no formato timestamp para o formato brasileiro
	 * @access public
	 * @author Rodrigo Conceição de Araujo
	 *
	 * @param Timestamp $datetime - timestamp com a data e hora vindos do banco de dados
	 *
	 * @return Strign com a data e hora no formato brasileiro
	 */
	static public function convertToBrazilian($datetime){
		// retorna a data e hora no formato brasileiro		
		return date(self::BRAZILIAN_FORMAT, strtotime($datetime));
	}
	
	/**
	 * Método responsável por converter uma data no formato timestamp para o formato brasileiro retornando apenas a data
	 * @access public
	 * @author Rodrigo Conceição de Araujo
	 *
	 * @param Timestamp $datetime - timestamp com a data e hora vindos do banco de dados
	 *
	 * @return Strign com a data no formato brasileiro
	 */
	static public function getBrazilianData($datetime){
		// retorna a data no formato brasileiro
		return current(explode(' ', date(self::BRAZILIAN_FORMAT, strtotime($datetime))));
	}
	
	/**
	 * Método responsável por calcular o intervalo em minutos entre duas datas no formato brasileiro
	 * @access public
	 * @author Rodrigo Conceição de Araujo
	 * 
	 * @param String $dateTimeStart - data e hora inicial
	 * @param String $dateTimeEnd - datae hora final
	 * 
	 * @return String intervalor em minutos entre as duas datas e hora
	 */
	static public function intervalMinutesBetweenBrazilianDates($dateTimeStart, $dateTimeEnd){
		
		// converte a data inicial para timestamp
		$dateTimeStart = self::convertToTimestamp($dateTimeStart);
		
		// converte a data final para timestamp
		$dateTimeEnd = self::convertToTimestamp($dateTimeEnd);
		
		// retorna a diferença em minutos entre as duas datas
		return self::intervalMinutesBetweenTimestampDates($dateTimeStart, $dateTimeEnd);
	}

	/**
	 * Método responsável por calcular o intervalo em minutos entre duas datas no formato timestamp
	 * @access public
	 * @author Rodrigo Conceição de Araujo
	 *
	 * @param Timestamp $dateTimeStart - data e hora inicial
	 * @param Timestamp $dateTimeEnd - datae hora final
	 *
	 * @return String intervalor em minutos entre as duas datas e hora
	 */
	static public function intervalMinutesBetweenTimestampDates($dateTimeStart, $dateTimeEnd){

		// inicializa variavel que ira receber o intervalo entre as duas datas no formato timestamp
		$intervalTimestamp = null;
		
		// converte para timestamp a data inicial
		$dateTimeStart = strtotime($dateTimeStart);

		// converte para timestamp a data final
		$dateTimeEnd = strtotime($dateTimeEnd);
		
		// subtrai os datas		
		$intervalTimestamp = $dateTimeEnd - $dateTimeStart;
		
		// retorna a diferença de minutos entre a data inicial e final
		return self::convertTimestampToMinutes($intervalTimestamp);
	}
	
	/**
	 * Método responsável por calcular o intervalo em horas entre duas datas no formato brasileiro
	 * @access public
	 * @author Rodrigo Conceição de Araujo
	 *
	 * @param String $dateTimeStart - data e hora inicial
	 * @param String $dateTimeEnd - datae hora final
	 *
	 * @return String intervalor em horas entre as duas datas e hora
	 */
	static public function intervalHoursBetweenBrazilianDates($dateTimeStart, $dateTimeEnd){
		
		// converte a data inicial para timestamp
		$dateTimeStart = self::convertToTimestamp($dateTimeStart);
		
		// converte a data final para timestamp
		$dateTimeEnd = self::convertToTimestamp($dateTimeEnd);
		
		// retorna a diferença em minutos entre as duas datas
		return self::intervalHoursBetweenTimestampDates($dateTimeStart, $dateTimeEnd);
	}

	/**
	 * Método responsável por calcular o intervalo em horas entre duas datas no formato timestamp
	 * @access public
	 * @author Rodrigo Conceição de Araujo
	 *
	 * @param Timestamp $dateTimeStart - data e hora inicial
	 * @param Timestamp $dateTimeEnd - datae hora final
	 *
	 * @return String intervalor em horas entre as duas datas e hora
	 */
	static public function intervalHoursBetweenTimestampDates($dateTimeStart, $dateTimeEnd){
		
		// inicializa variavel que ira receber o intervalo entre as duas datas no formato timestamp
		$intervalTimestamp = null;
		
		// converte para timestamp a data inicial
		$dateTimeStart = strtotime($dateTimeStart);
		
		// converte para timestamp a data final
		$dateTimeEnd = strtotime($dateTimeEnd);
		
		// subtrai os datas
		$intervalTimestamp = $dateTimeEnd - $dateTimeStart;
		
		// retorna a diferença de minutos entre a data inicial e final
		return self::convertTimestampToHours($intervalTimestamp);
	}
		
	/**
	 * Private methods
	 **/
	
	/**
	 * Método responsável por converter uma data timestamp em numero de dias
	 * @access private 
	 *  
	 * @param Timestamp $timestamp - data e hora convertida para timestamp
	 * 
	 * @return Integer conversão em dias do valor timestamp informado
	 */
	private function convertTimestampToDays($timestamp){
		return floor($timestamp/86400);
	}

	/**
	 * Método responsável por converter uma data timestamp em numero de horas
	 * @access private
	 *
	 * @param Timestamp $timestamp - data e hora convertida para timestamp
	 *
	 * @return Integer conversão em horas do valor timestamp informado
	 */
	private function convertTimestampToHours($timestamp){
		return floor($timestamp / 3600);
	}
	
	/**
	 * Método responsável por converter uma data timestamp em numero de dias
	 * @access private 
	 *  
	 * @param Timestamp $timestamp - data e hora convertida para timestamp
	 * 
	 * @return Integer conversão em dias do valor timestamp informado
	 */
	private function convertTimestampToMinutes($timestamp){
		return floor($timestamp  / 60);
	}

	/**
	 * Método responsável por converter uma data timestamp em numero de dias
	 * @access private
	 *
	 * @param Timestamp $timestamp - data e hora convertida para timestamp
	 *
	 * @return Integer conversão em dias do valor timestamp informado
	 */
	private function convertTimestampToSecond($timestamp){
		return $timestamp;
	}
	
	
}