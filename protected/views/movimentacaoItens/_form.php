<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'movimentacao-itens-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'movimentacao_id'); ?>
		<?php echo $form->textField($model, 'movimentacao_id'); ?>
		<?php echo $form->error($model,'movimentacao_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'inventario_id'); ?>
		<?php echo $form->textField($model, 'inventario_id'); ?>
		<?php echo $form->error($model,'inventario_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'desdobramento_id'); ?>
		<?php echo $form->textField($model, 'desdobramento_id'); ?>
		<?php echo $form->error($model,'desdobramento_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ativo'); ?>
		<?php echo $form->textField($model, 'ativo', array('maxlength' => 1)); ?>
		<?php echo $form->error($model,'ativo'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton('Save');
$this->endWidget();
?>
</div><!-- form -->