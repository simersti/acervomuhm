<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
	'Update',
);

$this->title_action = 'Editar ' . ' ' . GxHtml::encode($model->label()) . ' - ' . GxHtml::encode(GxHtml::valueEx($model));

// $this->menu = array(
// 	array('label' => 'List' . ' ' . $model->label(2), 'url'=>array('index')),
// 	array('label' => 'Create' . ' ' . $model->label(), 'url'=>array('create')),
// 	array('label' => 'View' . ' ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
// 	array('label' => 'Manage' . ' ' . $model->label(2), 'url'=>array('admin')),
// );


?>

<?php 
$this->widget(
    'booster.widgets.TbButtonGroup',
    array(
        'context' => 'primary',
        // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons' => array(
            array('label' => 'Ações', 'url' => '#'),
            array(
                'items' => array(
					array('label' => 'Visualizar '.$model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true)), 'context'=>'primary'),
					array('label'=>'Excluir ' . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Deseja realmente excluir este item?')),
					array('label' => 'Cadastrar novo', 'url'=>array('create'), 'context'=>'primary'),
					array('label' => 'Grade de '.$model->label(2), 'url'=>array('admin'), 'context'=>'primary'),
                )
            ),
        ),
    )
);

?>

<?php
$this->renderPartial('_form', array(
		'model' => $model));
?>


