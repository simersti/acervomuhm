<?php

class EspecialidadeMedicaController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'EspecialidadeMedica'),
		));
	}

	public function actionCreate() {
		$model = new EspecialidadeMedica;


		if (isset($_POST['EspecialidadeMedica'])) {
			$model->setAttributes($_POST['EspecialidadeMedica']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'EspecialidadeMedica');


		if (isset($_POST['EspecialidadeMedica'])) {
			$model->setAttributes($_POST['EspecialidadeMedica']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'EspecialidadeMedica');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new EspecialidadeMedica('search');
		$model->unsetAttributes();

		if (isset($_GET['EspecialidadeMedica']))
			$model->setAttributes($_GET['EspecialidadeMedica']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}