<?php

class BaseUtilModel extends GxActiveRecord{

	private $listStatus = array(
								array(
									'status_id' => 1,
									'status'	=> 'Ativo'
								),
								array(
									'status_id' => 0,
									'status'	=> 'Inativo'
								),
							);
	
	public function listStatusDefaul(){
		return $this->listStatus;
	}
	
	public function labelStatus($key){
		$label = $key;
		
		foreach($this->listStatus as $status){
			if($status['status_id'] == $key){
				$label = $status['status'];
				break;
			}
		}
		
		return $label;
	}

    public static function statusAtual($x){        
        if($x == 1){
            return 'Ativo';
        }else if($x == 0){
            return 'Inativo';
        }else{
            return 'Indefinido';
        }
    }
    public static function ativo($x){        
        if($x == 'S'){
            return 'Ativo';
        }else if($x == 'N'){
            return 'Inativo';
        }else{
            return 'Indefinido';
        }
    }
    public static function getPermissao($x){
        if($x == 1){
            return 'Administrador Master';
        }else if($x == 2){
            return 'Administrador';
        }else if($x == 3){
            return 'Usuário';
        }else{
            return 'Indefinido';
        }
    }
    public static function statusSimNao($x){        
        if($x == 1){
            return 'Sim';
        }else if($x == 0){
            return 'Não';
        }else{
            return 'Indefinido';
        }
    }
	
    public static function getInformado($x){        
        if($x == 0){
            return 'Não informado';
        }else{
            return $x;
        }
    }

    public static function reverteValor($valor,$reverse=0){
        if($reverse){
            return number_format($valor,2,",",".");
        }else{
            $valor = str_replace('.', '', $valor);
            return str_replace(',', '.', $valor);
        }
    }
    
    /* Inverter data-tempo mysql/pt-BR */
    public static  function reverteDataHora($data,$reverse=0){
        if($reverse){
            $dataHora = explode (' ', $data);
            return implode("-",array_reverse(explode("/",$dataHora[0]))) . ' ' . $dataHora[1];
        }else{
            $dataHora = explode (' ', $data);
            return implode("/",array_reverse(explode("-",$dataHora[0]))) . ' ' . $dataHora[1];
        }
    }
    /* Inverter data mysql/pt-BR */
    public static  function reverteData($data,$reverse=0){
        if($reverse)
            return implode("-",array_reverse(explode("/",$data)));
        else{
            if( $data <> '0000-00-00' )
                return implode("/",array_reverse(explode("-",$data)));
        }
    }


    public static function mesAno($data){
        // configuração mes
        switch (date('m',  strtotime($data))){
        case 1: $mes = "jan"; break;
        case 2: $mes = "fev"; break;
        case 3: $mes = "mar"; break;
        case 4: $mes = "abr"; break;
        case 5: $mes = "mai"; break;
        case 6: $mes = "jun"; break;
        case 7: $mes = "jul"; break;
        case 8: $mes = "ago"; break;
        case 9: $mes = "set"; break;
        case 10: $mes = "out"; break;
        case 11: $mes = "nov"; break;
        case 12: $mes = "dez"; break;
        }

        //Agora basta imprimir na tela...
        return ("$mes/").date('Y',  strtotime($data));
    }



}