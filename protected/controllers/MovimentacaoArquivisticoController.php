<?php

class MovimentacaoArquivisticoController extends GxController {

	public function actionCreate() {
		$model = new MovimentacaoArquivistico;

		$arquivistico = new Arquivistico('search');
		
		if (isset($_POST['MovimentacaoArquivistico'])) {
			$model->setAttributes($_POST['MovimentacaoArquivistico']);

			/*seta o id do usuario para o usuario que está fazendo a transação*/
			$model->usuario_id = Yii::app()->user->id;

			if ($model->save()) {
				
				foreach ($_POST['itens'] as $key => $item) {

					/*Salva os itens no movimentação itens*/
					foreach ($item as $key => $value) {

						$model2 = new MovimentacaoArquivisticoItens;						

						$model2->arquivistico_id = $key;
						$model2->arquivistico_item_id = (int) $value['incluir_item'];
						$model2->movimentacao_arquivistico_id = $model->id;
						$model2->ativo= 1;	


						$model2->save();					
					}

				}				 

				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model, 'arquivistico' => $arquivistico));
	}

	/*
	*  Método responsavel por receber os itens que estão em movimentação
	*
	*/

	public function actionReceberItens($id) {
		$model = $this->loadModel($id, 'MovimentacaoArquivistico');

		$arquivistico = new Arquivistico('search');
		

		if (isset($_POST['MovimentacaoArquivistico'])) {		

			$model->setAttributes($_POST['MovimentacaoArquivistico']);


			$model->observacao = $_POST['MovimentacaoArquivistico']['observacao'];
			$model->ativo = 0;

			if ($model->save()) {

				$itens = MovimentacaoArquivisticoItens::model()->findAllByAttributes(array('movimentacao_arquivistico_id'=>$id));			
			
				foreach ($itens as $key => $item) {
					$item->ativo = 0;				
					$item->update();
				}

				$this->redirect(array('admin'));
				
			}

		}else{
			/*busca todos os itens que compoe esta movimentação*/
		    $itens = Yii::app()->db->createCommand("select * from movimentacao_arquivistico_itens 
		    										where movimentacao_arquivistico_id =".$id)->queryAll();

			$grid = array();
			$i = 0;
			foreach ($itens as $chave => $item) {						
				$grid[$i]['id']= $item['arquivistico_item_id'];
				$grid[$i]['arquivistico_id']= $item['arquivistico_id'];

				$arquivistico_item = Arquivistico::model()->findByPk($item['arquivistico_id']);

				
				$grid[$i]['codigo_arquivistico']=$arquivistico_item->codigo;
				$grid[$i]['descricao_arquivistico']=$arquivistico_item->descricao;

				$arquivistico_item_item = ArquivisticoItem::model()->findByPk($item['arquivistico_item_id']);

				$grid[$i]['codigo_arquivistico_item']=$arquivistico_item_item->codigo;
				$grid[$i]['descricao_arquivistico_item']=$arquivistico_item_item->descricao;

				$i++;											
			}			
		}		

		$this->render('receber_itens', array(
				'model' => $model,
				'arquivistico' => $arquivistico,
				'grid' => $grid,				
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'MovimentacaoArquivistico');
		$model->excluido = 1;
		$model->ativo = 0;

		if ($model->save()) {

			$itens = MovimentacaoArquivisticoItens::model()->findAllByAttributes(array('movimentacao_arquivistico_id'=>$id));
		
			
			foreach ($itens as $key => $item) {
				$item->ativo = 0;				
				$item->update();
			}
			

			$this->redirect(array('admin'));
		}
	}


	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new MovimentacaoArquivistico('search');
		$model->unsetAttributes();

		if (isset($_GET['MovimentacaoArquivistico']))
			$model->setAttributes($_GET['MovimentacaoArquivistico']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionCreateItens() {
		$model = new MovimentacaoArquivisticoItens;		
		
		if(isset($_POST['Arquivistico'])){			

			$grid = array();
			$i = 0;
			foreach ($_POST['Arquivistico']['id'] as $key => $value) {
					$itens = Yii::app()->db->createCommand("select 
												a.id id_arquivistico,a.codigo codigo_arquivistico,a.descricao descricao_arquivistico,
												ai.id id_arquivistico_item, ai.codigo codigo_arquivistico_item, ai.descricao descricao_arquivistico_item 
												from arquivistico a 
												inner join arquivistico_item ai on ai.arquivistico_id = a.id
												where a.id =:arquivistico_id and ai.ativo=1 and ai.excluido=0")
											->bindValue(":arquivistico_id",(int)$value)											
											->queryAll();


					foreach ($itens as $chave => $item) {

						$existe_item = MovimentacaoArquivisticoItens::model()->findAllByAttributes(array('arquivistico_item_id'=>$item['id_arquivistico_item'],
																					   'arquivistico_id'=>$item['id_arquivistico'],
																					   'ativo'=>1));						

						if(!$existe_item){

							$grid[$i]['id']= $item['id_arquivistico_item'];
							$grid[$i]['arquivistico_id']= $item['id_arquivistico'];
							$grid[$i]['codigo_arquivistico']=$item['codigo_arquivistico'];
							$grid[$i]['descricao_arquivistico']=$item['descricao_arquivistico'];
							$grid[$i]['codigo_arquivistico_item']=$item['codigo_arquivistico_item'];
							$grid[$i]['descricao_arquivistico_item']=$item['descricao_arquivistico_item'];
							$i++;
						}									
					}					
			}			
		}else{
			$grid = array();
		}

		/*variavel de controle, para mostrar os checks, pois deve criar os itens na movimentação*/
		$is_recebimento_itens = false;
		$html =  $this->renderPartial(
			'_box_movimentacao', 			
			array(				
				'is_recebimento_itens'=> $is_recebimento_itens,				
				'grid' => $grid,
			),true
		);

		$return["html"] = $html;

		echo json_encode($return);
		die();		
	}	
}