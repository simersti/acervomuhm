<?php

class InventarioController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Inventario'),
		));
	}

	public function actionCreate() {
		$model = new Inventario;


		if (isset($_POST['Inventario'])) {
			$model->setAttributes($_POST['Inventario']);
			
			/*setando situação, campo a principio sem utilidade*/
			$model->situacao = 1;

			/*cria o um novo codigo apartir do numero anterior do acervo sequencial*/
			$acervo = new Acervo;
			$codigo = $acervo->geraCodigo();
			$model->numero_inventario = $codigo;

			$acervo->codigo = $codigo;
			$acervo->descricao = $model->nome;
			$acervo->tipo_acervo_id = 3; /*código na tabela tipo_acervo*/
			$acervo->ativo = $model->ativo;
			$acervo->excluido = $model->excluido;
 
 			if($acervo->save()){	
 				$model->acervo_id = $acervo->id;				
				if ($model->save()) {
					if (Yii::app()->getRequest()->getIsAjaxRequest())
						Yii::app()->end();
					else
						$this->redirect(array('admin'));
				}else{
					$acervo->delete();
				}
			}	
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Inventario');


		if (isset($_POST['Inventario'])) {
			$model->setAttributes($_POST['Inventario']);

			if ($model->save()) {		
				/*atualiza a descrição do acervo*/		
				$acervo = $this->loadModel($model->acervo_id, 'Acervo');
				$acervo->descricao = $model->nome;
				$acervo->save(false);
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Inventario');
		$model->excluido = '1';

		if($model->update(array('excluido'))){
			$this->redirect(array('admin'));
		}
	}

	public function actionClonar($id) {
		$model_original = $this->loadModel($id, 'Inventario');

		/*cria o um novo codigo apartir do numero anterior do acervo sequencial*/
		$acervo = new Acervo;
		$codigo = $acervo->geraCodigo();
		$model_original->numero_inventario = $codigo;

		$acervo->codigo = $codigo;
		$acervo->descricao = $model_original->nome;
		$acervo->tipo_acervo_id = 1; /*código na tabela tipo_acervo*/
		$acervo->ativo = $model_original->ativo;
		$acervo->excluido = $model_original->excluido;

		if($acervo->save()){				
				$model_original->id 		  = null;

				$model = new Inventario;
				$model->setAttributes($model_original->attributes);
				$model->acervo_id = $acervo->id;

			if ($model->save(false)) {				
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect('index.php?r=inventario/update&id='.$model->id);
			}else{				
				$acervo->delete();
			}
		}
	}

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new Inventario('search');
		$model->unsetAttributes();

		if (isset($_GET['Inventario']))
			$model->setAttributes($_GET['Inventario']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionImagens($id){	

		$model = $this->loadModel($id, 'Inventario');		

		$anexos	= new InventarioImagem();				
		$model->inventarioImagem	= $anexos->getAnexos($id);	

		
		$this->render('_anexos', array(
			'model' => $model,
		));

	}

	public function actionAnexarImagens($id){	

		// verifica se é uma requisão ajax
		if (Yii::app()->getRequest()->getIsAjaxRequest()){
			
			// verifica se o formulário foi submetido
			if(isset($_POST['Inventario'])){

				// instancia a model responsável pela validação dos dados do arquivo
				$inventario = new Inventario();
				
				// defini o cenário para relização das validações
				$inventario->setScenario('upload');

				// captura o arquivos submetido
				$inventario->inventarioImagem = CUploadedFile::getInstance($inventario, 'inventarioImagem');
								
				// valida os dados
				if(!empty($inventario->inventarioImagem) && $inventario->validate(array('inventarioImagem'))){
					
					// inicializa variavel que ira receber os erros encontrados no momento de salvar os arquivos
					$error = null;
					
					// inicializa variavel que ira a mensagem de sucesso quando encontrado
					$success = null;
					
					// instancia model responsável por armazenar o anexo
					$anexo = new InventarioImagem();
					
					// armazena o arquivo e captura os resultados
					$result = $anexo->saveUpload($inventario->inventarioImagem, $id);
					
					// verifica o tipo de resultado gerado
					if(isset($result[InventarioImagem::FILE_ERROR])){
						echo json_encode($result[InventarioImagem::FILE_ERROR]);
					}elseif(isset($result[InventarioImagem::FILE_SUCCESS])){
						echo json_encode($result[InventarioImagem::FILE_SUCCESS]);
					}
				}else{
					$error 	= null;
					$error 	= str_replace('?', '', $inventario->getError('inventarioImagem')); // para garantir que a mensagem de erro ira ser apresentada corretamente nos casos de tipo inválido
					$name	= $inventario->inventarioImagem->getName();
 					$data	= array(compact('error', 'name'));
					echo json_encode($data);					
				}
			}
		}
	}
	
	public function actionExcluirImagem($id){
		
		// verifica se é uma requisão ajax
		if (Yii::app()->getRequest()->getIsAjaxRequest()){
			if (Yii::app()->getRequest()->getIsPostRequest()) {
				
				// captura os dados do arquivo
				$model = $this->loadModel($id, 'InventarioImagem');

				// verifica se o arquivo a ser excuido existe no servidor
				if(file_exists($model->directory.$model->file)){
					// exclui o arquivo do servidor
					unlink($model->directory.$model->file);
					
					// exclui do banco de dados o registro
					$model->delete();
					
				}
				
			}else{
				throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
			}
		}else{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}	

	public function actionImagemDefault() {

		if (isset($_POST['id'])) {
			$model = $this->loadModel($_POST['id'], 'InventarioImagem');
			$model->principal=1;


			if ($model->update()) {
				/*Se colocou a imagem selecionada como principal, coloca as demais como não principal*/
				$id = $model->id;
				$inventario_id = $model->inventario_id;

				$criteria = new CDbCriteria;
				$criteria->addCondition('t.id !='.$id);
				$criteria->addCondition('t.inventario_id ='.$inventario_id);
				$demais_itens = InventarioImagem::Model()->findAll($criteria);

				foreach ($demais_itens as $key => $value) {
					$model2 = $this->loadModel($value->id, 'InventarioImagem');
					$model2->principal=0;
					$model2->update();
				}

				die;

					die('ok');	
			}else{
				die('error');
			}
		}	
	}	


	public function actionPublicarCatalogo() {

		if (isset($_POST['id'])) {
			$model = $this->loadModel($_POST['id'], 'Inventario');

			if($model->catalogo == 1){
				$model->catalogo=0;
			}else{
				$model->catalogo=1;
			}

			if ($model->update()) {
					die('ok');	
			}else{
				die('error');
			}
		}	
	}	

	public function actionRelatorios(){

		if($_POST){			
						
			if($_POST['Inventario']['ativo'] == 2){
				$ativo = '0,1';
			}else{
				$ativo = $_POST['Inventario']['ativo'];
			}


			if($_POST['Inventario']['doador_id'] == ''){
				$doador = Yii::app()->db->createCommand('SELECT id FROM doador where ativo=1 and excluido=0')->queryAll();

				$doadores = array();
				foreach ($doador as $key => $value) {
					array_push($doadores, $value['id']);
				}

				$doador_id = implode(",", $doadores);
			}else{
				$doador_id = $_POST['Inventario']['doador_id'];
			}

			if($_POST['Inventario']['modo_aquisicao_id'] == ''){
				$modo_aquisicao = Yii::app()->db->createCommand('SELECT id FROM modo_aquisicao where ativo=1 and excluido=0')->queryAll();

				$modo_aquisicoes = array();
				foreach ($modo_aquisicao as $key => $value) {
					array_push($modo_aquisicoes, $value['id']);
				}

				$modo_aquisicao_id = implode(",", $modo_aquisicoes);
			}else{
				$modo_aquisicao_id = $_POST['Inventario']['modo_aquisicao_id'];
			}

			if($_POST['Inventario']['acumulado_por_id'] == ''){
				$acumulado_por = Yii::app()->db->createCommand('SELECT id FROM acumulado_por where ativo=1 and excluido=0')->queryAll();

				$acumulados = array();
				foreach ($acumulado_por as $key => $value) {
					array_push($acumulados, $value['id']);
				}

				$acumulado_por_id = implode(",", $acumulados);
			}else{
				$acumulado_por_id = $_POST['Inventario']['acumulado_por_id'];
			}

			if($_POST['Inventario']['tipo_inventario_id'] == ''){
				$tipo_inventario = Yii::app()->db->createCommand('SELECT id FROM tipo_inventario where ativo=1 and excluido=0')->queryAll();

				$tipo_inventarios = array();
				foreach ($tipo_inventario as $key => $value) {
					array_push($tipo_inventarios, $value['id']);
				}

				$tipo_inventario_id = implode(",", $tipo_inventarios);
			}else{
				$tipo_inventario_id = $_POST['Inventario']['tipo_inventario_id'];
			}

			if($_POST['Inventario']['classificacao_id'] == ''){
				$classificacao = Yii::app()->db->createCommand('SELECT id FROM classificacao where ativo=1 and excluido=0')->queryAll();

				$classificacoes = array();
				foreach ($classificacao as $key => $value) {
					array_push($classificacoes, $value['id']);
				}

				$classificacao_id = implode(",", $classificacoes);
			}else{
				$classificacao_id = $_POST['Inventario']['classificacao_id'];
			}

			$colecao_id = $_POST['Inventario']['colecao_id'];


			$sub_colecao = array();
			if($_POST['Inventario']['sub_colecao_id'] == ''){
				$sub_colecao_id = 0;				
			}else{
				$sub_colecao_id = $_POST['Inventario']['sub_colecao_id'];
			}
			array_push($sub_colecao, array('id' => $sub_colecao_id));


			if($_POST['Inventario']['material_id'] == ''){
				$material = Yii::app()->db->createCommand('SELECT id FROM material where ativo=1 and excluido=0')->queryAll();

				$materiais = array();
				foreach ($material as $key => $value) {
					array_push($materiais, $value['id']);
				}

				$material_id = implode(",", $materiais);
			}else{
				$material_id = $_POST['Inventario']['material_id'];
			}			

			if($_POST['Inventario']['acondicionado_id'] == ''){
				$acondicionado = Yii::app()->db->createCommand('SELECT id FROM acondicionado where ativo=1 and excluido=0')->queryAll();

				$acondicionados = array();
				foreach ($acondicionado as $key => $value) {
					array_push($acondicionados, $value['id']);
				}

				$acondicionado_id = implode(",", $acondicionados);
			}else{
				$acondicionado_id = $_POST['Inventario']['acondicionado_id'];
			}		


			$inventario= Yii::app()->db->createCommand('SELECT *															
														FROM inventario 
														WHERE 
														excluido = 0 AND 
														modo_aquisicao_id in ('.$modo_aquisicao_id.') AND
														acumulado_por_id in ('.$acumulado_por_id.') AND
														tipo_inventario_id in ('.$tipo_inventario_id.') AND
														classificacao_id in ('.$classificacao_id.') AND
														colecao_id in ('.$colecao_id.') AND
														sub_colecao_id in ('.$sub_colecao_id.') AND
														material_id in ('.$material_id.') AND
														acondicionado_id in ('.$acondicionado_id.') AND
														doador_id in ('.$doador_id.')
														AND ativo in ('.$ativo.')')->queryAll();


			$this->geraPdf($inventario);
		}

		$model = new Inventario('search');
		$model->unsetAttributes();

		if (isset($_GET['Inventario']))
			$model->setAttributes($_GET['Inventario']);


		$this->render('_relatorio', array(
				'model' => $model,
				));		
	}	

	public function geraPdf($inventario){

	    $arquivo = 'inventario_'.date('d').'_'.date('m').'_'.date('Y').'.pdf';		  

	    $header = "";

	    $titulo = "RELATÓRIO DO TRIDIMENSIONAL";

		$pdf=Yii::app()->pdfFactory->getTCPDF();
		$pdf->SetTitle($titulo);
		$pdf->Ln(30);
		$pdf->setPrintHeader(false);
		$pdf->setFooterData($tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));		
		$pdf->SetAutoPageBreak(TRUE, 10);
		$pdf->SetMargins(25,25,25);

		$pdf->SetFont('', '', 10);
		$pdf->AddPage();

	    $pdf->Write(20,$header);
	    $pdf->Image('images/logo_horizontal.png',10,8, 90);
	    $pdf->Ln(20);

	    $pdf->SetFont('','ub',12);
	    $pdf->SetX(80);
	    $pdf->Write(5,$titulo);	    
	    $pdf->Ln(20);

		// header tabela
		$this->linhaTabela(true, null, $pdf, '200,200,200','Imagem', 'Código','Descrição', 'Condições Fisicas','Ativo');
		$pdf->SetFont('','',12);

		    $bgColor = "255,255,255";
		    foreach ($inventario as $value) {

				if ( $bgColor == "255,255,255" )
					$bgColor = "235,235,235";
				else
					$bgColor = "255,255,255";

				switch ($value['ativo']) {
				  case "0": $value['ativo'] = "Não"; break;
				  case "1": $value['ativo'] = "Sim"; break;
				}

				if($value['conservacao_id'] != 0){
					$conservacao = $this->loadModel($value['conservacao_id'], 'Conservacao');
					$conservacao = $conservacao->nome;
		    	}else{
					$conservacao = "Não especificado";
		    	}
				$this->linhaTabela (false,$value,$pdf, $bgColor, $value['numero_inventario'], $value['numero_inventario'], 
		    										 $value['nome'], $conservacao, $value['ativo']);		    	
		    }
		  
      	$pdf->SetY(272);
      	$pdf->SetX(10);      	
      	$pdf->SetFont('','b',9);      	
      	$pdf->Cell(0,10,"Rua Cel. Corte Real, 975 Porto Alegre-RS",0,0,'L');
      	$pdf->Ln(5);
      	$pdf->SetX(10);
      	$pdf->Cell(10,10,"CEP: 90630-080 / Fone (51) 3027.3737 / www.simers.org.br",0,0,'L');
		
		$pdf->Output($arquivo, 'I');

	}	


	function linhaTabela ( $header=false, $item, $pdf = null, $bgColor = null, $coluna1, $coluna2, $coluna3, $coluna4, $coluna5 ){

		if ( is_null( $bgColor ) )
		{
			$pdf->setFillColor( 255, 255, 255);

		} else {

			$bgColor = explode (",",$bgColor);
			$pdf->SetFillColor( $bgColor[0], $bgColor[1], $bgColor[2]);
		}

		$pdf->SetX( 20 );
		$pdf->SetFont('','',8);

		
		if(!is_null($item)){
			$inventario_imagem= Yii::app()->db->createCommand('SELECT *															
															FROM inventario_imagem 
															WHERE															
															inventario_id = '.$item['id'].'
															AND principal = 1')->queryAll();

			if(!empty($inventario_imagem)){

				foreach ($inventario_imagem as $key => $imagem) {
					$coluna1 = 'images/tridimensional/'.$imagem['file'];
				}				
				
			}else{
				$coluna1 = 'images/imagem_indisponivel.jpg';
			}

			$pdf->Cell( 24, 20, $pdf->Image($coluna1, $pdf->GetX(), $pdf->GetY(), 23.78), 1, 0, 'C', 0 );
			$pdf->Cell( 20, 20, $coluna2, 1, 0, "C", 1 ); // codigo
			$pdf->Cell( 60, 20, $coluna3, 1, 0, "C", 1 ); // descricao		
			$pdf->Cell( 53, 20, $coluna4, 1, 0, "C", 1 ); // 'condicoes_fisicas
			$pdf->Cell( 15, 20, $coluna5, 1, 1, "C", 1 ); // ativo			
		}else{
			$pdf->Cell( 24, 5, $coluna1, 1, 0, "C", 1 ); // imagem	
			$pdf->Cell( 20, 5, $coluna2, 1, 0, "C", 1 ); // codigo
			$pdf->Cell( 60, 5, $coluna3, 1, 0, "C", 1 ); // descricao		
			$pdf->Cell( 53, 5, $coluna4, 1, 0, "C", 1 ); // 'condicoes_fisicas
			$pdf->Cell( 15, 5, $coluna5, 1, 1, "C", 1 ); // ativo
		}
		

		if(!$header){

			if($item['colecao_id'] == 0){	
				$colecao = 'Sem Coleção';
			}else{
				$colecao = $this->loadModel($item['colecao_id'], 'Colecao');
				$colecao = $colecao->nome;
			}			

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Coleção:'.$colecao, 1, 1, "L", 1 ); // ativo

			if($item['sub_colecao_id'] == 0){	
				$subColecao = 'Sem Sub Coleção';
			}else{
				$subColecao = $this->loadModel($item['sub_colecao_id'], 'SubColecao');
				$subColecao = $subColecao->nome;
			}

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Sub Coleção:'.$subColecao, 1, 1, "L", 1 ); // ativo	

			if($item['doador_id'] == 0){	
				$doador = 'Sem Doador';
			}else{
				$doador = $this->loadModel($item['doador_id'], 'Doador');
				$doador = $doador->nome;
			}			

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Doador:'.$doador, 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Inventariado:'.$item['inventariado'], 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Inventariado Data:'.DateTimeComponent::getBrazilianData($item['inventariado_data']), 1, 1, "L", 1 ); // ativo	
		

			if($item['tipo_inventario_id'] != 0){
				$tipo_inventario = $this->loadModel($item['tipo_inventario_id'], 'TipoInventario');
				$tipo_inventario = $tipo_inventario->nome;
	    	}else{
				$tipo_inventario = "Não especificado";
	    	}

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Tipo Inventário:'.$tipo_inventario, 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Localização:'.$item['localizacao'] , 1, 1, "L", 1 ); // ativo	


			$pdf->SetX( 20 );			
			$pdf->MultiCell(172, 20, 'Descrição Peça:'.strip_tags($item['descricao_peca']) , 1, 1 ,'J',1 );			

			$pdf->SetX( 20 );			
			$pdf->MultiCell(172, 20, 'Inscrições Peça:'.strip_tags($item['inscricoes_peca']) , 1, 1 ,'J',1 );

			$pdf->SetX( 20 );			
			$pdf->MultiCell(172, 20, 'Histórico Peça:'.strip_tags($item['historico_peca']) , 1, 1 ,'J',1 );

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Referência Histórico:'.$item['referencias_historico'] , 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Referência Bibliográficas:'.$item['referencias_bibliograficas'] , 1, 1, "L", 1 ); // ativo	

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Altura:'.$item['altura'].' Largura:'.$item['largura'].' Comprimento:'.$item['comprimento'].' Diametro:'.$item['diametro'].' Quantidade:'.$item['quantidade'] , 1, 1, "L", 1 ); // ativo				

					$inventario_item= Yii::app()->db->createCommand('SELECT *															
															FROM desdobramento 
															WHERE
															excluido = 0 AND
															inventario_id = '.$item['id'].'
															AND ativo = 1 ORDER BY numero')->queryAll();

					$itens = 'ITENS';					
					
					$pdf->SetX( 20 );				
					$pdf->MultiCell(172, 5, $itens , 1,1,'L',1 );

					foreach ($inventario_item as $chave => $compoe) {						
						$itens = 'Código:'.$compoe['numero'].' - '.$compoe['nome'];

						$pdf->SetX( 20 );				
						$pdf->MultiCell(172, 5, $itens , 1,1,'L',1 );							
					}

					if(empty($inventario_item)){
						$pdf->SetX( 20 );				
						$pdf->MultiCell(172, 5, 'Sem itens cadastrados.' , 1,1,'L',1 );						
					}

			$pdf->SetX( 20 );	
			$pdf->MultiCell(172, 20, 'Observações:'.strip_tags($item['observacoes']) , 1, 1 ,'J',1 );
			$pdf->Ln(4);

		}

		return $pdf;	
	}	


	public function actionMovimentacaoAtiva(){

		$movimentacao= Yii::app()->db->createCommand('SELECT * 
													FROM movimentacao 
													WHERE ativo = 1 and excluido =0')->queryAll();

		$this->geraPdfMovimentacao($movimentacao, 1);
	}


	public function actionMovimentacaoInativa(){

		$movimentacao= Yii::app()->db->createCommand('SELECT * 
													FROM movimentacao 
													WHERE ativo = 0 and excluido =0')->queryAll();

		$this->geraPdfMovimentacao($movimentacao, 0);
	}


	public function geraPdfMovimentacao($movimentacao, $ativo){

	    $arquivo = 'tridimensional_'.date('d').'_'.date('m').'_'.date('Y').'.pdf';		  

	    $header = "";

	    if($ativo == 1){
	    	$titulo = "RELATÓRIO DE MOVIMENTAÇÕES ATIVAS DO TRIDIMENSIONAL";
	    }else{
	    	$titulo = "RELATÓRIO DE MOVIMENTAÇÕES INATIVAS DO TRIDIMENSIONAL";
	    }	
	    

		$pdf=Yii::app()->pdfFactory->getTCPDF();		
		$pdf->SetTitle($titulo);
		$pdf->Ln(30);
		$pdf->setPrintHeader(false);
		$pdf->setFooterData($tc=array(0,0,0), $lc=array(0,0,0));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));		
		$pdf->SetAutoPageBreak(TRUE, 10);
		$pdf->SetMargins(25,25,25);

		$pdf->SetFont('', '', 10);
		$pdf->AddPage();

	    $pdf->Write(20,$header);
	    $pdf->Image('images/logo_horizontal.png',10,8, 90);
	    $pdf->Ln(20);

	    $pdf->SetFont('','ub',12);
	    $pdf->SetX(40);
	    $pdf->Write(5,$titulo);	    
	    $pdf->Ln(20);

		$pdf->SetX( 20 );
		$pdf->SetFont('','',8);	    
		$pdf->setFillColor( 200, 200, 200);
		$pdf->Cell( 15, 5, 'Código', 1, 0, 'C', 1 );
		$pdf->Cell( 53, 5, 'Instituição', 1, 0, "C", 1 ); // codigo
		$pdf->Cell( 60, 5, 'Motivo', 1, 0, "C", 1 ); // descricao		
		$pdf->Cell( 20, 5, 'Saída', 1, 0, "C", 1 ); // 'condicoes_fisicas
		$pdf->Cell( 24, 5, 'Movimentação', 1, 1, "C", 1 ); // ativo		

		foreach ($movimentacao as $key => $value) {

			$pdf->setFillColor( 255, 255, 255);
			
			$pdf->SetX( 20 );
			$pdf->Cell( 15, 5, $value['codigo'], 1, 0, 'C', 1 );
			$pdf->Cell( 53, 5, $value['instituicao'], 1, 0, "C", 1 ); 
			$pdf->Cell( 60, 5, $value['motivo'], 1, 0, "C", 1 ); 
			$pdf->Cell( 20, 5, DateTimeComponent::getBrazilianData($value['data_saida']) , 1, 0, "C", 1 ); 

			$tipo_movimentacao = $this->loadModel($value['tipo_movimentacao_id'], 'TipoMovimentacao');
			$pdf->Cell( 24, 5, $tipo_movimentacao->nome, 1, 1, "C", 1 ); 

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Período:'.DateTimeComponent::getBrazilianData($value['periodo_inicio']).' à '.DateTimeComponent::getBrazilianData($value['periodo_fim']), 1, 1, "L", 1 ); // ativo				

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Responsável:'.$value['responsavel'], 1, 1, "L", 1 ); // ativo				

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Solicitante:'.$value['solicitante'], 1, 1, "L", 1 ); // ativo								

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'Localização:'.$value['localizacao'], 1, 1, "L", 1 ); // ativo				

			$pdf->SetX( 20 );				
			$pdf->Cell( 172, 5,'ITENS', 1, 1, "C", 1 ); // ativo	

				$movimentacao_itens= Yii::app()->db->createCommand('SELECT * 
																	FROM movimentacao_itens 
																	WHERE 
																	movimentacao_id ='. $value['id'] .'
																	AND ativo ='.$ativo)->queryAll();				
				if(!empty($movimentacao_itens)){
					foreach ($movimentacao_itens as $k => $item) {

						if($item['desdobramento_id'] == 0){
							$item = $this->loadModel($item['inventario_id'], 'Inventario');
							$pdf->SetX( 20 );				
							$pdf->Cell( 172, 5,$item['numero_inventario'].' - '.$item['nome'], 1, 1, "L", 1 ); // ativo
						}else{
							$item = $this->loadModel($item['desdobramento_id'], 'Desdobramento');
							$pdf->SetX( 20 );				
							$pdf->Cell( 172, 5,$item['numero'].' - '.$item['nome'], 1, 1, "L", 1 ); // ativo							
						}
						
					}
				}else{
					$pdf->SetX( 20 );				
					$pdf->Cell( 172, 5,'Sem itens cadastrados para esta movimentação.', 1, 1, "L", 1 ); // ativo											
				}

			$pdf->Ln(4);		
		}




      	$pdf->SetY(272);
      	$pdf->SetX(10);      	
      	$pdf->SetFont('','b',9);      	
      	$pdf->Cell(0,10,"Rua Cel. Corte Real, 975 Porto Alegre-RS",0,0,'L');
      	$pdf->Ln(5);
      	$pdf->SetX(10);
      	$pdf->Cell(10,10,"CEP: 90630-080 / Fone (51) 3027.3737 / www.simers.org.br",0,0,'L');
		
		$pdf->Output($arquivo, 'I');
	}	
}