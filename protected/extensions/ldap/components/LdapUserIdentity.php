<?php
/**
 * LdapUserIdentity
 *
 * @author: Rodrigo Conceição de Araujo <omb.rodrigo@gmail.com>
 * @package components.LdapUserIdentity
 * @version: 1.0
 */

/**
 * LdapUserIdentity
 *
 * Classe responsável por validar a existencia de um usuário no AD
 * 
 */
class LdapUserIdentity extends CApplicationComponent{

	/**
	 * @var Resource - Atributo que ira receber o id da conexão
	 */
	private $connectId = null;
	
	/**
	 * @var Resource - Atributo que ira receber o usuário a ser verificado
	 */
	public $user = null;
	
	/**
	 * @var String - Atributo que define  o host de conexão com AD
	 */
	public $host = null;

	/**
	 * @var Integer - Atributo que define a porta de conexão com o AD
	 */
	public $port = null;
	
	/**
	 * @var String - Atributo que define o dominio de acesso ao AD
	 */
	public $userDomain = null;
	
	/**
	 * @var String - Atributo que define as identificações de entrada
	 */
	public $dn = null;
	
	/**
	 * @var Array - Atributo que define o filtro da pesquisa no usuário no AD
	 */
	private $filter = 'sAMAccountName';
	
	/**
	 * @var Array - Atributo que define as informações que serão capturadas do usuário no AD
	 */
	private $fieldsToFind = array('samaccountname', 'displayname', 'mail');
	
	/**
	 * @var Array - Atributo que define a versão do protocolo de conexão
	 */
	public $version = 3;
	
	/**
	 * Enter description here ...
	 * @var unknown_type
	 */
	public $referrals = 0;
	
	public function init(){
		
		// cria a conexão com AD
		$this->connect();
	}

	/**
	 * Método responsável por realizar a conexão com o AD
	 * 
	 * @access private
	 * @return void
	 */
	private function connect() {

		try {
			// realiza a conexão com AD
			$this->connectId = ldap_connect($this->host, $this->port);

			// verifica se gerou um recurso válido
			if(!is_resource($this->connectId)){
				throw new CException(Yii::t('LdapUserIdentity.connect','Erro ao realizar a conexão.'));
			}
			
			// seta o protocolo e a referencia
			ldap_set_option( $this->connectId, LDAP_OPT_PROTOCOL_VERSION, $this->version );
			ldap_set_option( $this->connectId, LDAP_OPT_REFERRALS, $this->referrals ) ;
			
		}catch (CException $e){
			die($e->getMessage());
		}
	}
	
	/**
	 * Método responsável por autenticar o usuário no AD
	 * @param String $username - email do usuário a ser validado
	 * @param String $password - senha do usuário a ser validado
	 * 
	 * @access public
	 * 
	 * @return CUserIdentity error
	 */
	public function Authenticate($username, $password){

		// seta o username
		$this->user = $username;
		
		// verfica se o usuário e senha informados existem no AD 
		if(@ldap_bind($this->connectId, $this->user.$this->userDomain, $password)){
			// se não encontrou erros, retorna error none
			return CUserIdentity::ERROR_NONE;
		}else{
			// se não for localizado o usuário com os dados informados , retorna o erro
			return CUserIdentity::ERROR_UNKNOWN_IDENTITY;
		}
	}

	/**
	 * Método responsável por retornar lista com os dados do usuário autenticado no AD
	 * 
	 * @access public
	 * 
	 * @return Array com os dados
	 */
	public function getInfoUser(){

		// inicializa variavel que ira definir o filtro de pesquisa dos dados do usuário no AD
		$_filter = null;

		// inicializa variavel que ira receber a pesquisa do usuário no AD
		$_search = null;

		// inicializa variavel que ira recebe os dados do usuário
		$_data = null;

		// define o filtro de pesquisa
		$_filter = '('.$this->filter.'='.$this->user.')';

		// procura os dados do usuário no AD
		if($_search = ldap_search ( $this->connectId, $this->dn, $_filter, $this->fieldsToFind)) {
	
			// captura as informações do usuário no AD	
			$_data = ldap_get_entries($this->connectId, $_search) ;

			// verifica se retornou resultados
			if(sizeof($_data[0]) > 0) {
				
				// inicializa variavel que ira receber os dados do usuário
				$infoUser = array();				
				$infoUser['name'] 				= $_data[0]["displayname"][0];
				$infoUser['username'] 			= $_data[0]['samaccountname'][0];
				//$infoUser['telephonenumber']	= $_data[0]['telephonenumber'][0];
				return (object) $infoUser;
				
			} else {
				CUserIdentity::ERROR_USERNAME_INVALID;	
			}
		}
	}
	
}