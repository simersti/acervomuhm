<?php

/**
 * This is the model base class for the table "colecao".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Colecao".
 *
 * Columns in table "colecao" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property string $nome
 * @property string $ativo
 * @property string $excluido
 *
 */
abstract class BaseColecao extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'colecao';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Coleção|Coleções', $n);
	}

	public static function representingColumn() {
		return 'nome';
	}

	public function rules() {
		return array(			
			array('nome', 'length', 'max'=>50),
			array('ativo, excluido', 'length', 'max'=>1),
			array('nome, ativo, excluido', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, nome, ativo, excluido', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'subColecao' => array(self::HAS_MANY, 'SubColecao', 'colecao_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'nome' => Yii::t('app', 'Nome'),
			'ativo' => Yii::t('app', 'Ativo'),
			'excluido' => Yii::t('app', 'Excluido'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('nome', $this->nome, true);
		$criteria->compare('ativo', $this->ativo, true);
		$criteria->compare('excluido', 0);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}