<?php

Yii::import('application.models._base.BaseBibliotecaImagem');

class BibliotecaImagem extends BaseBibliotecaImagem
{

	/**
	 * Constantes que iram definir se no momento do upload ocorreu erro ou não
	 * @var String
	 */
	const FILE_ERROR 	= 'error';
	const FILE_SUCCESS 	= 'success';

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


	/**
	 * Método responsável por salvar o anexo adicionado a ata
	 * @author Rodrigo Conceicao de Araujo
	 * @access public
	 * @param CUploadedFile $file - instancia CUploadedFile com os dados do arquivo
	 * @param Integer $inventario_id - id da ata que esta sendo manipulada
	 * @return Resultado de sucesso ou erro 
	 */
	public function saveUpload(CUploadedFile $file, $biblioteca_id){
	
		// inicializa variavel que ira receber as mensagens de erro ou sucesso
		$result = array();
		
		// inicializa classe que ira gerar o nome criptografado do arquivo
		$securityManager = new CSecurityManager();
	
		// seta os parametros
		$this->biblioteca_id= $biblioteca_id;
		$this->nome_arquivo	= $file->getName();
		$this->file			= $securityManager->hashData(date('YmdHms')).'.'.$file->getExtensionName();
		$this->directory	= Yii::app()->params['pathBiblioteca'];
		$this->mimetype 	= $file->getType();
		$this->filesize		= $file->getSize();

		//verifica se já existe imagem cadastrada, se não existir está é a principal.
		if(!$this->existeImagemPrincipal($this->biblioteca_id)){
			$this->principal = 1;
		}


		// executa método que ira verificar se o arquivo a ser armazenado já existe no servidor, se existir, exclui o mesmo
		$this->verificaExistenciaArquivo();
		
		// valida os dados
		if($this->validate()){			
			if($this->save()){
				if($file->saveAs($this->directory.$this->file)){
					$result[self::FILE_SUCCESS] = array($this->mountSuccessMessageUpload($file));
				}else{
					$result[self::FILE_ERROR] = $this->mountErrorMessageUpload($file);					
				}
			}else{
				$result[self::FILE_ERROR] = $this->mountErrorMessageUpload($file);	
			}
		}else{
			$result[self::FILE_ERROR] = array(self::FILE_ERROR => $this->getErrors(), 'name' => $file->getName()); 
		}
		
		return $result;
	}
	
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Método responsável por verificar se existe anexos para uma ata
	 * @author Rodrigo Conceicao de Araujo
	 * @access public
	 * @param Integer $id - id da ata
	 * @return void
	 */
	public function possuiAnexos($biblioteca_id){
		
		// executa método que captura os anexos de uma ata
		$anexos = count($this->getAnexos($biblioteca_id)->getData());
		
		// verifica se o memos possui anexos
		if($anexos > 0){
			echo '<span class="label label-success"><i class="glyphicon glyphicon-file"></i>&nbsp;Possui '.$anexos.' anexo(s)</span>';
		}else{
			echo '';
		}
	}

	// --------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método responsável por capturar todos os anexos vinculados a um anexo
	 * @author Rodrigo Conceicao de Araujo
	 * @access public
	 * @return Array com os anexos ou null no caso de não localizar 
	 */
	public function getAnexos($biblioteca_id){

		// inicializa variavel que ira receber a lista com os anexos
		$anexos = array();
		
		// seta o id da ata ser pesquisada
		$this->biblioteca_id = $biblioteca_id;
		$this->data_alteracao = null;

		// retorna o resultado da consulta
		return $this->search();
	}
	
	// --------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método responsável por verificar se o arquivo que ira ser armazenado já existe
	 * No caso de ser localizado, exclui o mesmo
	 * @author Rodrigo Conceicao de Araujo
	 * @access private 
	 * @return void
	 */
	private function verificaExistenciaArquivo(){

		// vericia se existe o arquivo com os dados informados
		$verificacao = $this->analisaDadosArquivo();
		
		// se não retornou resultados
		if($verificacao){

			// veririca se o arquivo a ser excluido existe, se existir
			if(file_exists($verificacao->directory.$verificacao->file)){
				
				// seta o id do anexo a ser atualizado com os dados no novo arquivo
 				$this->id = $verificacao->BibliotecaImagem;
				
				// exclui do servidor o arquivo
				unlink($verificacao->directory.$verificacao->file);

				$this->setIsNewRecord(false);
			}
		}
	}
	
	// --------------------------------------------------------------------------------------------------------------
	
	/**
	 * Método responsável por montar a estrutura padrão das mensagens de erro de upload
	 * @author Rodrigo Conceicao de Araujo
	 * @access privte
	 * @param CUploadedFile $file
	 * @return Array com as mensagens
	 */
	private function mountErrorMessageUpload(CUploadedFile $file){
		$error 	= $file->getError();
		$name	= $file->getName();
		return array(compact('error', 'name'));
	}
	
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Método responsável por montar a estrutura padrão das mensagens de sucesso de upload
	 * @author Rodrigo Conceicao de Araujo
	 * @access privte
	 * @param CUploadedFile $file
	 * @return Array com as mensagens
	 */
	private function mountSuccessMessageUpload(CUploadedFile $file){

		// inicializa variavel que ira receber o caminho do thumbnail a ser adicionado
		$thumbnail_url = null;
		
		// verifica se é o arquivo a ser apresentado é um documento de texto ou imagem
		/*if(strpos(Yii::app()->params['fileUploadConfig']['PHP']['typeImage'], next(explode('.', strtolower($this->file)))) === false){
			$thumbnail_url = 'images/main/img-doc-default.png'; 
		}else{
			$thumbnail_url = $this->directory.$this->file;
		}*/

		$thumbnail_url = $this->directory.$this->file;

		$result =	array(	'name' 			=> $this->nome_arquivo,
							'type' 			=> $file->getType(),
							'size' 			=> $this->filesize,
							'url'			=> $this->directory.$this->file,
							'thumbnail_url'	=> $thumbnail_url,
							'delete_url'	=> Yii::app()->createUrl('biblioteca/excluirImagem', array('id' => $this->id)), 
							'delete_type' 	=> 'POST');
			
		return $result;
	}	

	/**
	 * Método responsável por verificar se o arquivo que ira ser armazenado já existe
	 * No caso de ser localizado, exclui o mesmo
	 * @author Rodrigo Conceicao de Araujo
	 * @access private 
	 * @return void
	 */
	private function existeImagemPrincipal($biblioteca_id){

				$criteria = new CDbCriteria;				
				$criteria->addCondition('t.biblioteca_id ='.$biblioteca_id);
				$model = BibliotecaImagem::Model()->findAll($criteria);


				if($model){
					return 1;
				}else{
					return 0;
				}
	}	
}