<?php

$this->breadcrumbs = array(
	ArquivisticoImagem::label(2),
	'Index',
);

$this->menu = array(
	array('label'=>'Create' . ' ' . ArquivisticoImagem::label(), 'url' => array('create')),
	array('label'=>'Manage' . ' ' . ArquivisticoImagem::label(2), 'url' => array('admin')),
);
?>

<h1><?php echo GxHtml::encode(ArquivisticoImagem::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 