<?php

class AcumuladoPorController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'AcumuladoPor'),
		));
	}

	public function actionCreate() {
		$model = new AcumuladoPor;


		if (isset($_POST['AcumuladoPor'])) {
			$model->setAttributes($_POST['AcumuladoPor']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'AcumuladoPor');


		if (isset($_POST['AcumuladoPor'])) {
			$model->setAttributes($_POST['AcumuladoPor']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'AcumuladoPor');
		$model->excluido = 1;

		if ($model->save()) {
			$this->redirect(array('admin'));
		}
	}	

	public function actionIndex() {
		$this->actionAdmin();
	}

	public function actionAdmin() {
		$model = new AcumuladoPor('search');
		$model->unsetAttributes();

		if (isset($_GET['AcumuladoPor']))
			$model->setAttributes($_GET['AcumuladoPor']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}