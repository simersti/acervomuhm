<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

	private $_id			= null;
	private $_username		= null;
	private $userData		= null;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate(){
		
		if(!isset($this->username)){
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}elseif(!isset($this->password)){
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}else{
			
			// executa método que ira autenticar o usuário no AD e captura o resultado
			$this->errorCode = Yii::app()->LdapUserIdentity->Authenticate($this->username, $this->password);

			// se não ocorreu erros no momento da autenticação
			if($this->errorCode == self::ERROR_NONE){
				$this->errorCode = $this->finalizerProcessAuthentication();
			}

		}

		return !$this->errorCode;
	}

	/**
	 * Método responsável por finalizar o processo de autenticação do usuário
	 * @access private
	 * @param integer $usuario_id - id do usuário que esta acessando a aplicação
	 * @return Retorna CUserIdentity::ERROR_USERNAME_INVALID ou null no caso de sucesso
	 */
	private function finalizerProcessAuthentication(){
		return $this->setValuesInSession();
	}

	/**
	 * Método responsável por setar informações do usuário na sessão
	 * @access private
	 * @return CUserIdentity ERROR_USERNAME_INVALID ou ERROR_NONE
	 */
	private function setValuesInSession(){

		// captura os dados do usuário autenticado no AD
		if(($this->userData = Yii::app()->LdapUserIdentity->getInfoUser()) !== self::ERROR_USERNAME_INVALID){

			@session_start(rand(0, 1000000));
		
			$this->setPersistentStates($_SESSION);
		
			$this->_id 			= $this->userData->username;
			$this->_username	= $this->userData->username;

			//atributos do ad
		 	$this->setState('username', 		$this->userData->username);		
		 	$this->setState('name', 			$this->userData->name);


			//atributos da tabela usuarios
		 	$aux = Usuario::model()->userDados($this->userData->username);

		 	$this->setState('id', 				$aux['id']);
		 	$this->setState('grupo_id', 		$aux['grupo_id']);
		 	$this->setState('nome', 			$aux['nome']);
		 	$this->setState('permissao', 		$aux['permissao']);
		 	$this->setState('login', 		$aux['login']);


		 	return self::ERROR_NONE;
	 	}

	 	return self::ERROR_USERNAME_INVALID;
	}

	/**
	 * @access public
	 * @Overwrite CUserIdentity::getId()
	 * @return integer
	 */	
	public function getId(){
        return $this->_id;
    }

    /**
	 * @access public
	 * @Overwrite CUserIdentity::getName()
	 * @return Strong
	 */
	public function getName(){
		return $this->username;
	}
	

}