<div class="form well">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'arquivistico-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Os campos com <span class="required">*</span> são obrigatórios.
	</p>

	<?php echo $form->errorSummary($model); ?>

		 
	
		    <div class="row">
			<?php if(!$model->isNewRecord): ?>		    	
				<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'codigo',array('widgetOptions' => array('htmlOptions' => array('disabled' => true)))); ?></div>
			<?php endif; ?>		
				<div class="col-md-4">			
					<?php echo $form->labelEx($model,'classificacao_arquivistico_id'); ?>
					<?php echo $form->dropDownList($model, 'classificacao_arquivistico_id', GxHtml::listDataEx(ClassificacaoArquivistico::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'classificacao_arquivistico_id'); ?>								
				</div>	
				<div class="col-md-4">			
					<?php echo $form->labelEx($model,'tipo_serie_id'); ?>
					<?php echo $form->dropDownList($model, 'tipo_serie_id', GxHtml::listDataEx(TipoSerie::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'tipo_serie_id'); ?>								
				</div>											
			</div><!-- row -->		
		

		<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'descricao', array('maxlength' => 255)); ?></div>			
		</div><!-- row -->	

		<div class="row">
			<div class="col-sm-4">

				<?php
					echo $form->datePickerGroup($model, 'data_entrada_museu',
		                array(
		                    'widgetOptions' => array(
		                        'options' => array(
		                            'language'                 => 'pt-BR',
		                            'format'                 => 'dd/mm/yyyy',
		                            'showDropdowns'         => false,
		                            'autoclose'                => false,
		                            'todayHighlight'         => false,
		                        ),
		                        // 'htmlOptions'=>array('placeholder'=>'Período',),
		                    ),
		                )
		            );
				?>

			</div>

			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'modo_aquisicao_id'); ?>
				<?php echo $form->dropDownList($model, 'modo_aquisicao_id', GxHtml::listDataEx(ModoAquisicao::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'modo_aquisicao_id'); ?>								
			</div>
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'acumulado_por_id'); ?>
				<?php echo $form->dropDownList($model, 'acumulado_por_id', GxHtml::listDataEx(AcumuladoPor::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'acumulado_por_id'); ?>								
			</div>				
		</div><!-- row -->	

		<div class="row">
			<div class="col-md-12">
			<?php echo $form->html5EditorGroup(
				$model,
				'historico',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>
		</div><!-- row -->	

		<div class="row">
			<!--<div class="col-md-12">
			<?php echo $form->html5EditorGroup(
				$model,
				'caracterizacao_sumaria',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>-->
		</div><!-- row -->	

		<!--<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'tipos_documentais_mais_frequentes', array('maxlength' => 255)); ?></div>-->
		</div><!-- row -->

		<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'condicoes_de_reprodutividade', array('maxlength' => 255)); ?></div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'condicoes_fisicas', array('maxlength' => 255)); ?></div>
		</div><!-- row -->	

		<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'instrumentos_de_pesquisa', array('maxlength' => 50)); ?></div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'localizacao', array('maxlength' => 100)); ?></div>
		</div><!-- row -->

		<div class="row">			
			<div class="col-md-2"><?php echo $form->textFieldGroup($model, 'data_limite_inicio', array('maxlength' => 4)); ?></div>
			<div class="col-md-2"><?php echo $form->textFieldGroup($model, 'data_limite_fim', array('maxlength' => 4)); ?></div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-6"><?php echo $form->textFieldGroup($model, 'acesso', array('maxlength' => 50)); ?></div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-6">			
				<?php echo $form->labelEx($model,'doador_id'); ?>
				<?php echo $form->dropDownList($model, 'doador_id', GxHtml::listDataEx(Doador::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'doador_id'); ?>								
			</div>			
		</div><!-- row -->

		<div class="row">

			<div class="col-sm-3">

				<?php
					echo $form->datePickerGroup($model, 'data_doacao',
		                array(
		                    'widgetOptions' => array(
		                        'options' => array(
		                            'language'                 => 'pt-BR',
		                            'format'                 => 'dd/mm/yyyy',
		                            'showDropdowns'         => false,
		                            'autoclose'                => false,
		                            'todayHighlight'         => false,
		                        ),
		                        // 'htmlOptions'=>array('placeholder'=>'Período',),
		                    ),
		                )
		            );
				?>

			</div>

		</div><!-- row -->

		<div class="row">
			<div class="col-md-12">
			<?php echo $form->html5EditorGroup(
				$model,
				'observacoes',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>
		</div>

		<div id="motivo_inativo" class="row" style="display:<?php echo ($model->ativo==1)? 'none':'block'; ?>;">
			<div class="col-md-6">			
				<?php echo $form->labelEx($model,'motivo_inativo_id'); ?>
				<?php echo $form->dropDownList($model, 'motivo_inativo_id', GxHtml::listDataEx(MotivoInativo::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'motivo_inativo_id'); ?>								
			</div>				
		</div>

		<div id="observacao_inativo" class="row" style="display:<?php echo ($model->ativo==1)? 'none':'block'; ?>;">
			<div class="col-md-12">
			<?php echo $form->html5EditorGroup(
				$model,
				'observacao_inativo',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>			
		</div>

		<div class="row">
			<div class="row">
			<div class="col-md-3">
			  <?php  echo $form->dropDownListGroup($model,'ativo',array(
						'widgetOptions' => array(
							'data' => array('1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('ativo'),
							),
						),
					)); ?>
			</div>
			<div class="col-md-3">
			  <?php  echo $form->dropDownListGroup($model,'tem_termo_doacao',array(
						'widgetOptions' => array(
							'data' => array('1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('tem_termo_doacao'),
							),
						),
					)); ?>
			</div>
			</div><!-- row -->					
				<div class="col-md-6">
				<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'submit',
						'size'=>'large',
						'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
						'label'=>$model->isNewRecord ? 'Cadastrar' : 'Salvar',
					)); ?>
				</div>
		</div><!-- row -->
<?php

$this->endWidget();
?>
</div><!-- form -->