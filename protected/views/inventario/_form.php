<div class="form well">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'inventario-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		Os campos com <span class="required">*</span> são obrigatórios.
	</p>

	<?php echo $form->errorSummary($model); ?>

		 <?php if(!$model->isNewRecord): ?>
			<div class="row">
				<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'numero_inventario',array('widgetOptions' => array('htmlOptions' => array('disabled' => true)))); ?></div>
			</div><!-- row -->		
		<?php endif; ?>		

		<div class="row">
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'nome', array('maxlength' => 255)); ?></div>			
			<div class="col-sm-3">

				<?php
					echo $form->datePickerGroup($model, 'data_aquisicao',
		                array(
		                    'widgetOptions' => array(
		                        'options' => array(
		                            'language'              => 'pt-BR',
		                            'format'                => 'dd/mm/yyyy',
		                            'showDropdowns'         => false,
		                            'autoclose'             => false,
		                            'todayHighlight'        => false,
		                        ),		                       
		                    ),
		                )
		            );
				?>

			</div>			
		</div><!-- row -->	

		<div class="row">
			<div class="col-sm-4">

				<?php
					echo $form->datePickerGroup($model, 'data_entrada_museu',
		                array(
		                    'widgetOptions' => array(
		                        'options' => array(
		                            'language'              => 'pt-BR',
		                            'format'                => 'dd/mm/yyyy',
		                            'showDropdowns'         => false,
		                            'autoclose'             => false,
		                            'todayHighlight'        => false,
		                        ),		                        
		                    ),
		                )
		            );
				?>

			</div>

			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'modo_aquisicao_id'); ?>
				<?php echo $form->dropDownList($model, 'modo_aquisicao_id', GxHtml::listDataEx(ModoAquisicao::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'modo_aquisicao_id'); ?>								
			</div>			
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'acumulado_por_id'); ?>
				<?php echo $form->dropDownList($model, 'acumulado_por_id', GxHtml::listDataEx(AcumuladoPor::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'acumulado_por_id'); ?>								
			</div>				
		</div><!-- row -->			

		<div class="row">
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'origem', array('maxlength' => 255)); ?></div>
			<div class="col-sm-3">

				<?php
					echo $form->datePickerGroup($model, 'data_origem',
		                array(
		                    'widgetOptions' => array(
		                        'options' => array(
		                            'language'              => 'pt-BR',
		                            'format'                => 'dd/mm/yyyy',
		                            'showDropdowns'         => false,
		                            'autoclose'             => false,
		                            'todayHighlight'        => false,
		                        ),
		                    ),
		                )
		            );
				?>

			</div>			
		</div><!-- row -->

		<div class="row">
			<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'procedencia', array('maxlength' => 255)); ?></div>
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'foto_autor', array('maxlength' => 255)); ?></div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'altura', array('maxlength' => 10)); ?></div>		
			<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'largura', array('maxlength' => 10)); ?></div>		    
		    <div class="col-md-3"><?php echo $form->textFieldGroup($model, 'comprimento', array('maxlength' => 10)); ?></div> 
 			<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'diametro', array('maxlength' => 10)); ?></div> 			
		</div><!-- row -->	

		<div class="row">
			<div class="col-md-3"><?php echo $form->textFieldGroup($model, 'quantidade', array('maxlength' => 10)); ?></div>
		</div><!-- row -->	

		<div class="row">
			<div class="col-md-12"><?php echo $form->textFieldGroup($model, 'localizacao', array('maxlength' => 100)); ?></div>
		</div><!-- row -->	

		<div class="row">			
			<div class="col-md-4"><?php echo $form->textFieldGroup($model, 'inventariado', array('maxlength' => 100)); ?></div>
			<div class="col-sm-3">

				<?php
					echo $form->datePickerGroup($model, 'inventariado_data',
		                array(
		                    'widgetOptions' => array(
		                        'options' => array(
		                            'language'              => 'pt-BR',
		                            'format'                => 'dd/mm/yyyy',
		                            'showDropdowns'         => false,
		                            'autoclose'             => false,
		                            'todayHighlight'        => false,
		                        ),
		                        // 'htmlOptions'=>array('placeholder'=>'Período',),
		                    ),
		                )
		            );
				?>

			</div>			
		</div><!-- row -->	

		<div class="row">
			<div class="col-md-6">
			<?php echo $form->html5EditorGroup(
				$model,
				'descricao_peca',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>

			<div class="col-md-6">
			<?php echo $form->html5EditorGroup(
				$model,
				'inscricoes_peca',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
			<?php echo $form->html5EditorGroup(
				$model,
				'historico_peca',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>

			<div class="col-md-6">
			<?php echo $form->html5EditorGroup(
				$model,
				'referencias_historico',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
			<?php echo $form->html5EditorGroup(
				$model,
				'referencias_bibliograficas',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>

			<div class="col-md-6">
			<?php echo $form->html5EditorGroup(
				$model,
				'observacoes',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'doador_id'); ?>
				<?php echo $form->dropDownList($model, 'doador_id', GxHtml::listDataEx(Doador::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'doador_id'); ?>								
			</div>
			<div class="col-md-3">			
				<?php echo $form->labelEx($model,'tipo_inventario_id'); ?>
				<?php echo $form->dropDownList($model, 'tipo_inventario_id', GxHtml::listDataEx(TipoInventario::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'tipo_inventario_id'); ?>								
			</div>			
		</div><!-- row -->					

		<div class="row">
			<div class="col-md-6">			
				<?php echo $form->labelEx($model,'classificacao_id'); ?>
				<?php echo $form->dropDownList($model, 'classificacao_id', GxHtml::listDataEx(Classificacao::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'classificacao_id'); ?>								
			</div>
		</div><!-- row -->							

		<div class="row">
			<div class="col-md-6">			
				<?php echo $form->labelEx($model,'colecao_id'); ?>
				<?php echo $form->dropDownList($model, 'colecao_id', GxHtml::listDataEx(Colecao::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'colecao_id'); ?>								
			</div>

			<div class="col-md-6">					
				<?php echo $form->labelEx($model,'sub_colecao_id'); ?>
				<?php echo CHtml::activeDropDownList($model,'sub_colecao_id', CHtml::listData(SubColecao::model()->findAll(), 'id', 'nome'), array('empty' => 'Sem Sub Coleção','class'=>'form-control')); ?>
				<?php echo $form->error($model,'sub_colecao_id'); ?>								
			</div>

		</div><!-- row -->					

		<div class="row">
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'conservacao_id'); ?>
				<?php echo $form->dropDownList($model, 'conservacao_id', GxHtml::listDataEx(Conservacao::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'conservacao_id'); ?>								
			</div>

			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'material_id'); ?>
				<?php echo $form->dropDownList($model, 'material_id', GxHtml::listDataEx(Material::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'material_id'); ?>								
			</div>
			<div class="col-md-4">			
				<?php echo $form->labelEx($model,'acondicionado_id'); ?>
				<?php echo $form->dropDownList($model, 'acondicionado_id', GxHtml::listDataEx(Acondicionado::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'acondicionado_id'); ?>								
			</div>			
		</div><!-- row -->					

		<div id="motivo_inativo" class="row" style="display:<?php echo ($model->ativo==1)? 'none':'block'; ?>;">
			<div class="col-md-6">			
				<?php echo $form->labelEx($model,'motivo_inativo_id'); ?>
				<?php echo $form->dropDownList($model, 'motivo_inativo_id', GxHtml::listDataEx(MotivoInativo::model()->findAllAttributes(null, true)),array('class'=>'form-control')); ?>
				<?php echo $form->error($model,'motivo_inativo_id'); ?>								
			</div>				
		</div>

		<div id="observacao_inativo" class="row" style="display:<?php echo ($model->ativo==1)? 'none':'block'; ?>;">
			<div class="col-md-12">
			<?php echo $form->html5EditorGroup(
				$model,
				'observacao_inativo',
				 array(
					'widgetOptions' => array(
						'editorOptions' => array(
							'class' => 'span4',
							'rows' => 5,
							'height' => '200',
							'options' => array('color' => true)
						),
					)
					)
				); ?>			
			</div>			
		</div>
		
		<div class="row">
			<div class="row">
			<div class="col-md-3">
			  <?php  echo $form->dropDownListGroup($model,'ativo',array(
						'widgetOptions' => array(
							'data' => array('1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('ativo'),
							),
						),
					)); ?>
			</div>
			<div class="col-md-3">
			  <?php  echo $form->dropDownListGroup($model,'catalogo',array(
						'widgetOptions' => array(
							'data' => array('1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('catalogo'),
							),
						),
					)); ?>
			</div>
			</div><!-- row -->					
				<div class="col-md-6">
				<?php $this->widget('booster.widgets.TbButton', array(
						'buttonType'=>'submit',
						'size'=>'large',
						'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
						'label'=>$model->isNewRecord ? 'Cadastrar' : 'Salvar',
					)); ?>
				</div>
		</div><!-- row -->
<?php

$this->endWidget();
?>
</div><!-- form -->

