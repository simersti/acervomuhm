<h1><?php echo 'Relatório ' . GxHtml::encode($model->label()); ?></h1>

<div class="form well">
<?php 
	$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'biblioteca-form',
	'htmlOptions'=>array('target'=>'_blank'),
	'enableAjaxValidation' => false,
));
?>
		<?php echo $form->errorSummary($model); ?>

		<div class="row">

		</div><!-- row -->
		<div class="row">
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'autor'); ?>
				<?php echo $form->dropDownList($model, 'autor', GxHtml::listDataEx($autor, 'autor', 'autor'),array('prompt'=>'Todos','class'=>'form-control')); ?>
			</div>
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'editora_id'); ?>
				<?php echo $form->dropDownList($model, 'editora_id', GxHtml::listDataEx(Editora::model()->findAll(),'id','id'),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'editora_id'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'titulo'); ?>
				<?php
					echo $form->dropDownList($model, 'titulo', GxHtml::listDataEx($titulo, 'titulo', 'titulo'),array('prompt'=>'Todos','class'=>'form-control'));
				?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'doador_id'); ?>
				<?php echo $form->dropDownList($model, 'doador_id', GxHtml::listDataEx(Doador::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'doador_id'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'modo_aquisicao_id'); ?>
				<?php echo $form->dropDownList($model, 'modo_aquisicao_id', GxHtml::listDataEx(ModoAquisicao::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'modo_aquisicao_id'); ?>
			</div>
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'tipologia_id'); ?>
				<?php echo $form->dropDownList($model, 'tipologia_id', GxHtml::listDataEx(Tipologia::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'tipologia_id'); ?>
			</div>
		</div><!-- row -->

		<div class="row">
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'acumulado_por_id'); ?>
				<?php echo $form->dropDownList($model, 'acumulado_por_id', GxHtml::listDataEx(AcumuladoPor::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'acumulado_por_id'); ?>
			</div>
			
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'especialidade_medica_id'); ?>
				<?php echo $form->dropDownList($model, 'especialidade_medica_id', GxHtml::listDataEx(EspecialidadeMedica::model()->findAllAttributes(null, true)),array('prompt'=>'Todos','class'=>'form-control')); ?>
				<?php echo $form->error($model,'especialidade_medica_id'); ?>
			</div>

			<div class="col-md-4">
			  <?php  echo $form->dropDownListGroup($model,'ativo',array(
						'widgetOptions' => array(
							'data' => array('2'=>'Ambos','1'=>'Sim','0'=>'Não'),
							'options' => array(

								'placeholder' => $model->getAttributeLabel('ativo'),
							),
						),
					)); 
				?>
			</div>
		</div>
		           <!-- row -->
		<div class="row">
			<div class="col-md-12">
				<?php echo $form->labelEx($model,'Tipo de Relatório'); ?>
				<?php echo 'Listagem Simples&nbsp;&nbsp;'. CHtml::radioButton('tipoRelatorio[]', 'S', array(
										    'value'=>'S',
										    'id'=>'btnname',
										    'uncheckValue'=>null
										)); 
						echo '&nbsp;&nbsp;&nbsp;Listagem Detalhada&nbsp;&nbsp;'.CHtml::radioButton('tipoRelatorio[]', false, array(
						    'value'=>'D',
						    'id'=>'btnname2',
						    'uncheckValue'=>null
						));
				?>
				<?php echo $form->error($model,'editora_id'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
			<?php 
				$this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'size'=>'large',
					'htmlOptions'=>array('style'=>'width:200px;','class'=>'btn btn-success'),
					'label'=> 'Gerar Relatório',
				)); 
			?>
			</div>
		</div><!-- row -->
		<?php
			$this->endWidget();
		?>
</div><!-- form -->