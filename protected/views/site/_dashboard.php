<style type="text/css">
.summary, .filters, .items thead{
	display: none;
}
.grid-view {
		padding: 0px;

}
</style>
<?php
	Yii::app()->clientScript->registerScript('search', "
	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('vinculo-empresa-grid', {
			data: $(this).serialize()
		});
		return false;
	});
	");
?>
<div class="row">
	<div class="col-md-6">
		<h4><span class="label label-default">Últimas empresas adicionas.</span> </h4>
		<div class="well" style="padding:2px;">
			<?php $this->widget('booster.widgets.TbExtendedGridView', array(
				'id' => 'vinculo-empresa-grid',
				'type' => 'striped bordered condensed hover',
				'enablePagination' => false,
				'dataProvider' => $modelEmpresa->search(),
				'filter' => $modelEmpresa,
				'columns' => array(
					'nome_empresa',
					'cod_empresa',
					'crm'
				),
			)); 
			?>
		</div>
	</div>
	<div class="col-md-6">

	</div>
</div>