<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	'Create',
);


$this->title_action = 'Cadastro de' . ' ' . GxHtml::encode($model->label(2));

?>

<?php 
	$this->widget(
	    'booster.widgets.TbButtonGroup',
	    array(
	        'buttons' => array(
	            array('buttonType'=>'link' ,'label' => 'Grade de '.$model->label(2), 'url'=>array('admin'), 'context'=>'primary'),
	        ),
	    )
	);

?>

<?php

$this->renderPartial('_form', array(
		'model' => $model,
		'buttons' => 'create'));
?>